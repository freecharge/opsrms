package com.snapdeal.payments.opsRms.services.impl.test;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.handlers.TokenUtils;
import com.snapdeal.payments.opsRms.services.impl.GetUserByTokenServiceImpl;
import com.snapdeal.payments.opsRmsModel.dto.TokenDTO;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.GetUserByTokenRequest;
import com.snapdeal.payments.opsRmsModel.request.User;

@RunWith(PowerMockRunner.class)
@PrepareForTest(TokenUtils.class)
public class GetUserByTokenServiceImplTest {
	@InjectMocks
	private GetUserByTokenServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;

	@Spy
	private RequestParamValidatorForRole<GetUserByTokenRequest> requestValidator = new RequestParamValidatorForRole<>();

	  @Before
	    public void setup() {
	        MockitoAnnotations.initMocks(this);
	    }

	  private TokenDTO getUserByTokenId(String token){
		  if(token.equals("1234"))
			return new TokenDTO("dummy", new Date(), true, "dummy");
		  else
			  return new TokenDTO("dummy", new Date(), false, "dummy");
		  }
	  private User getUserByUserName(String userName){
		  User user = new User();
		  user.setEmail("dummyEmail");
		  user.setId("dummyId");
		  user.setName("dummyName");
		  user.setUserName("dummyusername");
		  return user;
	  }
		@Test
		public void testGetUserByTokenSuccess() {
			GetUserByTokenRequest request = new GetUserByTokenRequest();
			request.setRequestToken("1234");
			PowerMockito.mockStatic(TokenUtils.class);
			Mockito.when(TokenUtils.getTokenIdFromToken(Mockito.any(String.class))).thenReturn("1001");
			Mockito.when(TokenUtils.getDEFAULT_TIMEOUT()).thenReturn(2);
			Mockito.when(persistanceManager.getUserNameFromId(Mockito.any(String.class))).thenReturn(getUserByTokenId(request.getRequestToken()));
			Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class))).thenReturn(getUserByUserName("ddd"));
			service.getUserByToken(request);
		}
		@Test(expected= ValidationExceptionOps.class)
		public void testGetUserByTokenFailure() {
			GetUserByTokenRequest request = new GetUserByTokenRequest();
			request.setRequestToken("12345");
			PowerMockito.mockStatic(TokenUtils.class);
			Mockito.when(TokenUtils.getTokenIdFromToken(Mockito.any(String.class))).thenReturn("1001");
			Mockito.when(persistanceManager.getUserNameFromId(Mockito.any(String.class))).thenReturn(getUserByTokenId(request.getRequestToken()));
			Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class))).thenReturn(getUserByUserName("ddd"));
			service.getUserByToken(request);
		}

}
