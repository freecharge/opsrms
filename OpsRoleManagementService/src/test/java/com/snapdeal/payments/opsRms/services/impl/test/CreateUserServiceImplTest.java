package com.snapdeal.payments.opsRms.services.impl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.dao.model.UserDaoModel;
import com.snapdeal.payments.opsRms.handlers.IDFactory;
import com.snapdeal.payments.opsRms.handlers.OTPUtils;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.services.impl.CreateUserServiceImpl;
import com.snapdeal.payments.opsRmsModel.dto.UserVerificationDTO;
import com.snapdeal.payments.opsRmsModel.exceptions.UserAlreadyExistExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.CreateUserRequest;
import com.snapdeal.payments.opsRmsModel.request.EmailTemplate;
import com.snapdeal.payments.opsRmsModel.request.GetUserByUserNameRequest;
import com.snapdeal.payments.opsRmsModel.request.User;

public class CreateUserServiceImplTest {

	@InjectMocks
	private CreateUserServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;
		
	@Mock
	private OTPUtils otpUtils;
	
	@Mock
	private IDFactory idfactory;
	
	@Spy
	private RequestParamValidatorForRole<CreateUserRequest> requestValidator = new RequestParamValidatorForRole<>();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		service.setExpiryDurationInHours(24);
	}

	private User getUserByUserName(String userName) {
		User user = new User();
		user.setUserName("random123@gmail.com");
		if (userName.equals(user.getUserName())) {
			return user;
		}
		return null;
	}

	@Test
	public void testCreateUserSuccess() {
		GetUserByUserNameRequest request = new GetUserByUserNameRequest();
		request.setUserName("random1234@gmail.com");
		CreateUserRequest user = new CreateUserRequest();
		user.setUserName("random1234@gmail.com");
		user.setName("Aniket");
		user.setPassword("password");
		user.setEmail("email");
		Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class)))
				.thenReturn(getUserByUserName(request.getUserName()));
		Mockito.doNothing().when(persistanceManager).saveUser(Mockito.any(UserDaoModel.class),
				Mockito.any(UserVerificationDTO.class));
		Mockito.when(otpUtils.getSendToTestEmail()).thenReturn("false");
		service.createUser(user);

	}
	
	@Test
	public void testCreateUserWithLinkSuccess() {
		GetUserByUserNameRequest request = new GetUserByUserNameRequest();
		request.setUserName("random1234@gmail.com");
		CreateUserRequest user = new CreateUserRequest();
		user.setUserName("random1234@gmail.com");
		user.setName("Aniket");
		user.setPassword("password");
		user.setEmail("email");
		user.setLinkForSettingPassword("dummyLink");
		user.setEmailTemplate(EmailTemplate.MERCHANT_PANEL_SETPASSWORD);
		Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class)))
				.thenReturn(getUserByUserName(request.getUserName()));
		Mockito.doNothing().when(persistanceManager).saveUser(Mockito.any(UserDaoModel.class),
				Mockito.any(UserVerificationDTO.class));
		Mockito.when(otpUtils.getSendToTestEmail()).thenReturn("false");
		service.createUser(user);

	}

	@Test
	public void testCreateUserWithPermissionsAndRolesSuccess() {
		GetUserByUserNameRequest request = new GetUserByUserNameRequest();
		request.setUserName("random1234@gmail.com");
		CreateUserRequest user = new CreateUserRequest();
		user.setUserName("random1234@gmail.com");
		user.setName("Aniket");
		user.setPassword("password");
		user.setEmail("email");
		List<Integer> permList = new ArrayList<Integer>();
		permList.add(1);
		List<Integer> roleList = new ArrayList<Integer>();
		roleList.add(1001);
		user.setPermissionIds(permList);
		user.setRoleIds(roleList);
		Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class)))
				.thenReturn(getUserByUserName(request.getUserName()));
		Mockito.doNothing().when(persistanceManager).saveUser(Mockito.any(UserDaoModel.class),
				Mockito.any(UserVerificationDTO.class));
		Mockito.when(otpUtils.getSendToTestEmail()).thenReturn("false");
		service.createUser(user);

	}

	@Test(expected = UserAlreadyExistExceptionOps.class)
	public void testCreateUserFailure() {
		GetUserByUserNameRequest request = new GetUserByUserNameRequest();
		request.setUserName("random123@gmail.com");
		CreateUserRequest user = new CreateUserRequest();
		user.setUserName("random123@gmail.com");
		user.setName("Aniket");
		user.setPassword("password");
		user.setEmail("email");
		Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class)))
				.thenReturn(getUserByUserName(request.getUserName()));
		service.createUser(user);

	}

}
