package com.snapdeal.payments.opsRms.services.impl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.services.impl.UpdateRoleServiceImpl;
import com.snapdeal.payments.opsRmsModel.request.UpdateRoleRequest;
import com.snapdeal.payments.opsRmsModel.request.UpdateUserRequest;

public class UpdateRoleServiceImplTest {

   @InjectMocks
   private UpdateRoleServiceImpl service;

   @Mock
   private PersistanceManager persistanceManager;

   @Spy
   private RequestParamValidatorForRole<UpdateUserRequest> requestValidator;

   @Before
   public void setup() {
      MockitoAnnotations.initMocks(this);
   }

   @Test
   public void testUpdateRoleSuccess() {
      UpdateRoleRequest request = new UpdateRoleRequest();
      request.setId(1001);
      List<Integer> permissionIds = new ArrayList<>();
      permissionIds.add(1);
      permissionIds.add(2);
      request.setPermissionIds(permissionIds);
      service.updateRole(request);
   }

}
