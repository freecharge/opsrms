package com.snapdeal.payments.opsRms.services.impl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.services.impl.CreateRoleServiceImpl;
import com.snapdeal.payments.opsRmsModel.request.CreateRoleRequest;
import com.snapdeal.payments.opsRmsModel.request.CreateUserRequest;

public class CreateRoleServiceImplTest {

   @InjectMocks
   private CreateRoleServiceImpl service;

   @Mock
   private PersistanceManager persistanceManager;

   @Spy
   private RequestParamValidatorForRole<CreateUserRequest> requestValidator;

   @Before
   public void setup() {
      MockitoAnnotations.initMocks(this);
   }

   @Test
   public void testCreateRoleSuccess() {
      CreateRoleRequest request = new CreateRoleRequest();
      request.setRoleName("ADMIN");
      List<Integer> permissionIds = new ArrayList<>();
      permissionIds.add(1);
      permissionIds.add(2);
      request.setPermissionIds(permissionIds);
      Mockito.when(persistanceManager.addRole(Mockito.any(String.class))).thenReturn(1001);
      service.createRole(request);

   }

}
