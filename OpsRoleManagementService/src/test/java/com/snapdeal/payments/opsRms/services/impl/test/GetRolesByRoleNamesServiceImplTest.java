package com.snapdeal.payments.opsRms.services.impl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.services.impl.GetRolesByRoleNamesServiceImpl;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.GetRolesByRoleNamesRequest;
import com.snapdeal.payments.opsRmsModel.request.Role;

public class GetRolesByRoleNamesServiceImplTest {
	@InjectMocks
	private GetRolesByRoleNamesServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;

	@Spy
	private RequestParamValidatorForRole<GetRolesByRoleNamesRequest> requestValidator;

	  @Before
	    public void setup() {
	        MockitoAnnotations.initMocks(this);
	    }

	  private List<Role> getRolesByRoleNames(List<String> userlist){
			List<Role> response = new ArrayList<Role>();
		  	Role role = new Role();
			role.setName("Aniket");
			response.add(role);
			return response;
		  }
		@Test
		public void testGetRolesByRoleNamesSuccess() {
			GetRolesByRoleNamesRequest request = new GetRolesByRoleNamesRequest();
			List<String> roleNameList = new ArrayList<String>();
			roleNameList.add("1");
			roleNameList.add("2");
			request.setListRoleNames(roleNameList);
			Mockito.when(persistanceManager.getRolesByRoleNames(Mockito.any(List.class)))
			.thenReturn(getRolesByRoleNames(request.getListRoleNames()));
			service.getRolesByRoleNames(request);
		}
		@Test(expected= ValidationExceptionOps.class)
		public void testGetUsersByRoleValidationFailure(){
			GetRolesByRoleNamesRequest request = new GetRolesByRoleNamesRequest();
			service.getRolesByRoleNames(request);
		}

}
