package com.snapdeal.payments.opsRms.services.impl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.services.impl.UIDropDownServiceImpl;
import com.snapdeal.payments.opsRmsModel.request.Permission;
import com.snapdeal.payments.opsRmsModel.request.Role;
import com.snapdeal.payments.opsRmsModel.request.User;

public class UIDropDownServiceImplTest {
	@InjectMocks
	private UIDropDownServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;

	  @Before
	    public void setup() {
	        MockitoAnnotations.initMocks(this);
	    }

	  private List<User> getUsers(){
		  	List<User> userList = new ArrayList<User>();
			User user = new User();
			user.setEmail("random123@gmail.com");
			user.setName("Aniket");
			userList.add(user);
			return userList;
		  }
		@Test
		public void testGetUsersSuccess() {
			Mockito.when(persistanceManager.getAllUsers())
			.thenReturn(getUsers());
			service.getAllUsers(null);
		}
		private List<Permission> getPermissions(){
		  	List<Permission> permissionList = new ArrayList<Permission>();
			Permission p = new Permission();
			p.setName("Aniket");
			permissionList.add(p);
			return permissionList;
		  }
		@Test
		public void testGetPermissionsSuccess() {
			Mockito.when(persistanceManager.getAllPermissions())
			.thenReturn(getPermissions());
			service.getAllPermissions(null);
		}
		private List<Role> getRoles(){
		  	List<Role> roleList = new ArrayList<Role>();
		  	Role role = new Role();
			role.setName("Aniket");
			roleList.add(role);
			return roleList;
		  }
		@Test
		public void testGetRolesSuccess() {
			Mockito.when(persistanceManager.getAllRoles())
			.thenReturn(getRoles());
			service.getAllRoles(null);
		}

}
