package com.snapdeal.payments.opsRms.services.impl.test;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.IDFactory;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.handlers.TokenUtils;
import com.snapdeal.payments.opsRms.services.impl.ChangePasswordServiceImpl;
import com.snapdeal.payments.opsRmsModel.dto.TokenDTO;
import com.snapdeal.payments.opsRmsModel.exceptions.UnAuthrizedExceptionOps;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.ChangePasswordRequest;

@RunWith(PowerMockRunner.class)
@PrepareForTest(TokenUtils.class)
public class ChangePasswordServiceImplTest {

   @InjectMocks
   private ChangePasswordServiceImpl service;

   @Mock
   private PersistanceManager persistanceManager;
   
   @Mock
   private IDFactory idFactory;

   @Spy
   private RequestParamValidatorForRole<ChangePasswordRequest> requestValidator = new RequestParamValidatorForRole<>();

   @Before
   public void setup() {
      MockitoAnnotations.initMocks(this);
   }
   
	private TokenDTO getUserByTokenId(String token) {
		if (token.equals("1234"))
			return new TokenDTO("dummy", new Date(), true, "dummy");
		else
			return new TokenDTO("dummy", new Date(), false, "dummy");
	}

   @Test
   public void testChangePasswordSuccess() {
      ChangePasswordRequest request = new ChangePasswordRequest();

      request.setRequestToken("1234");
      request.setToken("1234");
      request.setNewPassword("shubhambansal");
      request.setOldPassword("shubhambansal23");
      PowerMockito.mockStatic(TokenUtils.class);
      PowerMockito.when(TokenUtils.getTokenIdFromToken(Mockito.any(String.class))).thenReturn(
               "1234");
      Mockito.when(TokenUtils.getDEFAULT_TIMEOUT()).thenReturn(2);
		Mockito.when(persistanceManager.getUserNameFromId(Mockito.any(String.class))).thenReturn(getUserByTokenId(request.getRequestToken()));
      Mockito.when(
               persistanceManager.getPasswordExpiryDate(Mockito.any(String.class), Mockito.any(String.class)))
               .thenReturn(new Date());
      service.changePassword(request);
   }
   
   @Test(expected= ValidationExceptionOps.class)
   public void testChangePasswordInvalidTokenFailure() {
      ChangePasswordRequest request = new ChangePasswordRequest();

      request.setRequestToken("12345");
      request.setNewPassword("shubhambansal");
      request.setOldPassword("shubhambansal23");
      PowerMockito.mockStatic(TokenUtils.class);
      PowerMockito.when(TokenUtils.getTokenIdFromToken(Mockito.any(String.class))).thenReturn(
               "12345");
      Mockito.when(TokenUtils.getDEFAULT_TIMEOUT()).thenReturn(2);
		Mockito.when(persistanceManager.getUserNameFromId(Mockito.any(String.class))).thenReturn(getUserByTokenId(request.getRequestToken()));
      Mockito.when(
               persistanceManager.getPasswordExpiryDate(Mockito.any(String.class), Mockito.any(String.class)))
               .thenReturn(new Date());
      service.changePassword(request);
   }

   @Test(expected = UnAuthrizedExceptionOps.class)
   public void testChangePasswordFailure() {
      ChangePasswordRequest request = new ChangePasswordRequest();
      request.setRequestToken("1234");
      request.setToken("dummyToken");
      request.setNewPassword("shubhambansal");
      request.setOldPassword("shubhambansal23");
      PowerMockito.mockStatic(TokenUtils.class);
      Mockito.when(TokenUtils.getDEFAULT_TIMEOUT()).thenReturn(2);
      Mockito.when(persistanceManager.getUserNameFromId(Mockito.any(String.class))).thenReturn(getUserByTokenId(request.getRequestToken()));
      PowerMockito.when(TokenUtils.getTokenIdFromToken(Mockito.any(String.class))).thenReturn(
               "dummyUser");
      Mockito.when(
               persistanceManager.getPasswordExpiryDate(Mockito.any(String.class), Mockito.any(String.class)))
               .thenReturn(null);
      service.changePassword(request);
   }

   @Test(expected = ValidationExceptionOps.class)
   public void testChangePasswordValidationFailure() {
      ChangePasswordRequest request = new ChangePasswordRequest();
      service.changePassword(request);
   }
}
