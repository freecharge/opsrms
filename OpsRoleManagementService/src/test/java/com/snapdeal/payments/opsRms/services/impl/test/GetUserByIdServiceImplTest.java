package com.snapdeal.payments.opsRms.services.impl.test;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.services.impl.GetUserByIdServiceImpl;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.GetUserByIdRequest;
import com.snapdeal.payments.opsRmsModel.request.User;

public class GetUserByIdServiceImplTest {
	@InjectMocks
	private GetUserByIdServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;

	@Spy
	private RequestParamValidatorForRole<GetUserByIdRequest> requestValidator;

	  @Before
	    public void setup() {
	        MockitoAnnotations.initMocks(this);
	    }

	  private User getUserById(String roleId){
			User user = new User();
			user.setEmail("random123@gmail.com");
			user.setName("Aniket");
			return user;
		  }
		@Test
		public void testGetUsersByRoleSuccess() {
			GetUserByIdRequest request = new GetUserByIdRequest();
			request.setUserId("1234");
			Mockito.when(persistanceManager.getUserById(Mockito.any(String.class)))
			.thenReturn(getUserById(request.getUserId()));
			service.getUserById(request);
		}
		@Test(expected= ValidationExceptionOps.class)
		public void testGetUsersByRoleValidationFailure(){
			GetUserByIdRequest request = new GetUserByIdRequest();
			service.getUserById(request);
		}

}
