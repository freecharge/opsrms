package com.snapdeal.payments.opsRms.services.impl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.services.impl.GetUsersByIdsServiceImpl;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.GetUsersByIdsRequest;
import com.snapdeal.payments.opsRmsModel.request.User;

public class GetUsersByIdsServiceImplTest {
	@InjectMocks
	private GetUsersByIdsServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;

	@Spy
	private RequestParamValidatorForRole<GetUsersByIdsRequest> requestValidator;

	  @Before
	    public void setup() {
	        MockitoAnnotations.initMocks(this);
	    }

	  private List<User> getUsersByIds(List<String> userlist){
			List<User> response = new ArrayList<User>();
		  	User user = new User();
			user.setEmail("random123@gmail.com");
			user.setName("Aniket");
			response.add(user);
			return response;
		  }
		@Test
		public void testGetUsersByIdsSuccess() {
			GetUsersByIdsRequest request = new GetUsersByIdsRequest();
			List<String> userIdList = new ArrayList<String>();
			userIdList.add("1");
			userIdList.add("2");
			request.setListUserIds(userIdList);
			Mockito.when(persistanceManager.getUsersByIds(Mockito.any(List.class)))
			.thenReturn(getUsersByIds(request.getListUserIds()));
			service.getUsersByIds(request);
		}
		@Test(expected= ValidationExceptionOps.class)
		public void testGetUsersByRoleValidationFailure(){
			GetUsersByIdsRequest request = new GetUsersByIdsRequest();
			service.getUsersByIds(request);
		}

}
