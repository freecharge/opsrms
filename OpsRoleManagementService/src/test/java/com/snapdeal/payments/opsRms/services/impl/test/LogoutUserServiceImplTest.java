package com.snapdeal.payments.opsRms.services.impl.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.handlers.TokenUtils;
import com.snapdeal.payments.opsRms.services.impl.LogoutUserServiceImpl;
import com.snapdeal.payments.opsRmsModel.request.LogoutUserRequest;

@RunWith(PowerMockRunner.class)
@PrepareForTest(TokenUtils.class)
public class LogoutUserServiceImplTest {
	@InjectMocks
	   private LogoutUserServiceImpl service;

	   @Mock
	   private PersistanceManager persistanceManager;
	   
	   @Mock
	   private TokenUtils tokenUtils;

	   @Spy
	   private RequestParamValidatorForRole<LogoutUserRequest> requestValidator = new RequestParamValidatorForRole<>();

	   @Before
	   public void setup() {
	      MockitoAnnotations.initMocks(this);

	   }
   @Test
   public void testLogoutUserSuccess() {
     LogoutUserRequest request = new LogoutUserRequest();
     request.setRequestToken("dummyRequestToken");
     PowerMockito.mockStatic(TokenUtils.class);
     PowerMockito.when(TokenUtils.getTokenIdFromToken(Mockito.any(String.class))).thenReturn(
             "dummyId");
     service.logoutUser(request);
   }

}
