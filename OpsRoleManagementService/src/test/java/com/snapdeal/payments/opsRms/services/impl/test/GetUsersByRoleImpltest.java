package com.snapdeal.payments.opsRms.services.impl.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.services.impl.GetUsersByRoleServiceImpl;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.GetUsersByRoleRequest;
import com.snapdeal.payments.opsRmsModel.request.User;

public class GetUsersByRoleImpltest {
	@InjectMocks
	private GetUsersByRoleServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;

	@Spy
	private RequestParamValidatorForRole<GetUsersByRoleRequest> requestValidator;

	  @Before
	    public void setup() {
	        MockitoAnnotations.initMocks(this);
	    }

	private List<User> getUsersByRole(String roleName){
		User user1 = new User();
		user1.setEmail("random123@gmail.com");
		user1.setName("Aniket");
		User user2 = new User();
		user2.setEmail("email");
		user2.setName("name");
		List<User> userList=new ArrayList<User>();
		return userList;
	  }
	@Test
	public void testGetUsersByRoleSuccess() {
		GetUsersByRoleRequest request = new GetUsersByRoleRequest();
		request.setRoleName("random1234@gmail.com");
		Mockito.when(persistanceManager.getUsersByRole(Mockito.any(String.class)))
		.thenReturn(getUsersByRole(request.getRoleName()));
		service.getUsersByRole(request);
	}
	@Test(expected= ValidationExceptionOps.class)
	public void testGetUsersByRoleValidationFailure(){
		GetUsersByRoleRequest request = new GetUsersByRoleRequest();
		service.getUsersByRole(request);
	}

}
