package com.snapdeal.payments.opsRms.services.impl.test;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.services.impl.UpdateUserServiceImpl;
import com.snapdeal.payments.opsRmsModel.exceptions.NoSuchUserExceptionOps;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.UpdateUserRequest;
import com.snapdeal.payments.opsRmsModel.request.User;

public class UpdateUserServiceImplTest {
	
	@InjectMocks
	private UpdateUserServiceImpl service;

	@Mock
	private PersistanceManager persistanceManager;

	@Spy
	private RequestParamValidatorForRole<UpdateUserRequest> requestValidator;

	  @Before
	    public void setup() {
	        MockitoAnnotations.initMocks(this);
	    }

	private Boolean isUserExist(String userId){
		User user = new User();
		user.setId("1234");
		if(userId.equals(user.getId())){
			 return true; 
		 }
		return false;
	  }
	@Test
	public void testUpdateUserSuccess() {
		UpdateUserRequest user = new UpdateUserRequest();
		user.setUserId("1234");
		user.setName("Lohia");
		Mockito.when(persistanceManager.isUserExist(Mockito.any(String.class)))
		.thenReturn(isUserExist("1234"));
		service.updateUser(user);
	
	}
	@Test(expected= NoSuchUserExceptionOps.class)
	public void testUpdateUserFailure() {
		UpdateUserRequest user = new UpdateUserRequest();
		user.setUserId("12345");
		user.setName("Lohia");
		Mockito.when(persistanceManager.isUserExist(Mockito.any(String.class)))
		.thenReturn(isUserExist("12345"));
		service.updateUser(user);
	}
	
	@Test(expected= ValidationExceptionOps.class)
	public void testUpdateUserValidationFailure(){
		UpdateUserRequest user = new UpdateUserRequest();
		service.updateUser(user);
	}
}
