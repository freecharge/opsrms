package com.snapdeal.payments.opsRms.services.impl.test;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.snapdeal.onecheck.social.entity.SocialInfo;
import com.snapdeal.onecheck.social.factory.SocialProviderFactory;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.IDFactory;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.handlers.TokenUtils;
import com.snapdeal.payments.opsRms.services.impl.SocialLoginUserServiceImpl;
import com.snapdeal.payments.opsRmsModel.dto.GenerateTokenResponse;
import com.snapdeal.payments.opsRmsModel.dto.TokenDTO;
import com.snapdeal.payments.opsRmsModel.exceptions.InternalServerExceptionOps;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.CreateUserRequest;
import com.snapdeal.payments.opsRmsModel.request.SocialLoginUserRequest;
import com.snapdeal.payments.opsRmsModel.request.User;
import com.snapdeal.payments.opsRmsModel.response.CreateUserResponse;
import com.snapdeal.payments.opsRmsModel.services.OpsRoleMgmtService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SocialProviderFactory.class,TokenUtils.class})
public class SocialLoginUserServiceImplTest {

   @InjectMocks
   private SocialLoginUserServiceImpl service;
   
   @Mock
   private OpsRoleMgmtService opsRoleMgmtService;
   @Mock
   private PersistanceManager persistanceManager;
   
   @Mock
   private SocialProviderFactory socialProviderFactory;
   
   @Mock
   private TokenUtils tokenUtils;
   
   @Spy
   private IDFactory idfactory = new IDFactory();

   @Spy
   private RequestParamValidatorForRole<SocialLoginUserRequest> requestValidator = new RequestParamValidatorForRole<>();

   @Before
   public void setup() {
      MockitoAnnotations.initMocks(this);

   }

   private User getUserByUserName(String userName) {
      User user = new User();
      user.setEmail("dummyEmail");
      user.setUserName(userName);
      return user;
   }

   @Test
   public void testLoginNewUserSuccess() throws Exception {
	  SocialLoginUserRequest request = new SocialLoginUserRequest();
      request.setEmailId("right@right.com");
      request.setSocialToken("socialToken");
      request.setSource("google");
      PowerMockito.mockStatic(SocialProviderFactory.class);
      PowerMockito.mockStatic(TokenUtils.class);
      Mockito.when(tokenUtils.generateToken()).thenReturn(new GenerateTokenResponse("dumm", "dumm"));
      Mockito.when(SocialProviderFactory.getEmailFromSocialInfo(Mockito.any(SocialInfo.class))).thenReturn("right@right.com");
      Mockito.when(TokenUtils.getDEFAULT_TIMEOUT()).thenReturn(2);
      Mockito.when(persistanceManager.getTokenFromUsername(Mockito.any(String.class))).thenReturn(new TokenDTO("dumm", new Date(), true, "dummy"));
      Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class))).thenReturn(
               getUserByUserName(request.getEmailId()));
      service.socialLoginUser(request);
   }
   @Test
   public void testLoginOldUserSuccess() throws Exception {
	  SocialLoginUserRequest request = new SocialLoginUserRequest();
      request.setEmailId("right@right.com");
      request.setSocialToken("socialToken");
      request.setSource("google");
      CreateUserResponse response = new CreateUserResponse();
      response.setUserId("a");
      PowerMockito.mockStatic(SocialProviderFactory.class);
      PowerMockito.mockStatic(TokenUtils.class);
      Mockito.when(tokenUtils.generateToken()).thenReturn(new GenerateTokenResponse("dumm", "dumm"));
      Mockito.when(SocialProviderFactory.getEmailFromSocialInfo(Mockito.any(SocialInfo.class))).thenReturn("right@right.com");
      Mockito.when(TokenUtils.getDEFAULT_TIMEOUT()).thenReturn(2);
      Mockito.when(persistanceManager.getTokenFromUsername(Mockito.any(String.class))).thenReturn(new TokenDTO("dumm", new Date(), true, "dummy"));
      Mockito.when(persistanceManager.getUserByUserName(Mockito.any(String.class))).thenReturn(null);
      Mockito.when(opsRoleMgmtService.createUser(Mockito.any(CreateUserRequest.class))).thenReturn(response);
      Mockito.when(persistanceManager.getUserById(Mockito.any(String.class))).thenReturn(getUserByUserName(request.getEmailId()));
      service.socialLoginUser(request);
   }
   
   @Test(expected = ValidationExceptionOps.class)
   public void testLoginUserFailure() throws Exception {
	  SocialLoginUserRequest request = new SocialLoginUserRequest();
      request.setEmailId("right@right.com");
      request.setSocialToken("socialToken");
      request.setSource("google");
      PowerMockito.mockStatic(SocialProviderFactory.class);
      Mockito.when(SocialProviderFactory.getEmailFromSocialInfo(Mockito.any(SocialInfo.class))).thenReturn("wrong@right.com");
      service.socialLoginUser(request);
   }
   @Test(expected = ValidationExceptionOps.class)
   public void testSocialLoginUserValidationFailure() {
      SocialLoginUserRequest request = new SocialLoginUserRequest();
      service.socialLoginUser(request);
   }
   @Test(expected = InternalServerExceptionOps.class)
   public void testSocialLoginUserServerFailure() throws Exception {
	   SocialLoginUserRequest request = new SocialLoginUserRequest();
	      request.setEmailId("right@right.com");
	      request.setSocialToken("socialToken");
	      request.setSource("google");
	      PowerMockito.mockStatic(SocialProviderFactory.class);
	      Mockito.when(SocialProviderFactory.getEmailFromSocialInfo(Mockito.any(SocialInfo.class))).thenThrow(new InternalServerExceptionOps());
	      service.socialLoginUser(request);
   }
   
}
