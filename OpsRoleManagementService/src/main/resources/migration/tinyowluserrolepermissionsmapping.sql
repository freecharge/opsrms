use role_mgmt;
insert into role (id,name) values (1,'Account');
insert into role (id,name) values (1002,'Manage User');
insert into role (id,name) values (1003,'Payments');


insert into role_permission_map (role_id,permission_id) values (1,1001);
insert into role_permission_map (role_id,permission_id) values (1002,1001);
insert into role_permission_map (role_id,permission_id) values (1002,1002);
insert into role_permission_map (role_id,permission_id) values (1003,1003);
insert into role_permission_map (role_id,permission_id) values (1003,1012);


insert into user_permission_mapping (permission_id,user_id) values (1001,'14514642Q5K9q_uqSJOK6kN6NyBMqQ');
insert into user_permission_mapping (permission_id,user_id) values (1002,'14514642Q5K9q_uqSJOK6kN6NyBMqQ');