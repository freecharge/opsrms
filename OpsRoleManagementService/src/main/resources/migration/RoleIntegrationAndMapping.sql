use role_mgmt;
insert into role(name) values('Integration');
insert into role_permission_map values((select id from role where name='Integration'),(select id from permissions where permission_name='MERCHANT_SANDBOX_INT_KEY'));
insert into role_permission_map values((select id from role where name='Integration'),(select id from permissions where permission_name='MERCHANT_PROD_INT_KEY'));