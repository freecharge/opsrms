use role_mgmt;
alter table user add password_expires_on DATE();
update user set password_expires_on = DATE_ADD(now(),INTERVAL 45 DAY);