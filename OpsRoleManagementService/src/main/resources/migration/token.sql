use role_mgmt;
CREATE TABLE token (
  id varchar(127) NOT NULL,
  user_name varchar(127) NOT NULL,
  is_active TINYINT DEFAULT 0,
  login_time datetime NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY(user_name)	
);