package com.snapdeal.payments.vault;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariConfigMXBean;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jdbc.pool.DataSourceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Scheduled;

import javax.sql.DataSource;

@Configuration
@Slf4j
@ComponentScan(value = "com.snapdeal.payments.vault")
public class DBConfig {

    @Autowired
    AppProperties dbProperties;

    private DataSource dataSource = null;
    private DataSource poolDataSource = null;

    @Bean(name = {"dataSource"})
    @Primary
    DataSource dataSource() {
        log.info("System out Creating datasource object " + dataSource);
        if (dataSource == null) {
            HikariConfig config = new HikariConfig();
            log.info("System out Creating datasource object" + dbProperties);
            dbProperties.generateDbCredentials();
            try {
                config.setDriverClassName(dbProperties.getProperty(DBConstants.DRIVER_CLASSNAME).toString());
                config.setJdbcUrl(dbProperties.getProperty(DBConstants.JDBC_URL).toString());
                log.info("jdbcurl: " + dbProperties.getProperty(DBConstants.JDBC_URL).toString());
                config.setUsername(dbProperties.getProperty(DBConstants.USERNAME).toString());
                log.info("username: " + dbProperties.getProperty(DBConstants.USERNAME).toString());
                config.setPassword(dbProperties.getProperty(DBConstants.PASS_WORD).toString());
                //log.info("password: " + dbProperties.getProperty(DBConstants.PASSWORD).toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            poolDataSource = DataSourceBuilder.create()
                    .driverClassName(dbProperties.getProperty(DBConstants.DRIVER_CLASSNAME).toString())
                    .url(dbProperties.getProperty(DBConstants.JDBC_URL).toString())
                    .username(dbProperties.getProperty(DBConstants.USERNAME).toString())
                    .password(dbProperties.getProperty(DBConstants.PASS_WORD).toString())
                    .build();
            dataSource = new HikariDataSource(config);
        }
        return dataSource;
    }

    @Scheduled(fixedRateString = "${oprms.database.refreshRate}")
    private void updateDataSource() {
        log.info("Updating the datasource");
        System.out.println("Updating the datasource");
        dbProperties.generateDbCredentials();
        try {
            HikariDataSource hikariDataSource = (HikariDataSource) dataSource();
            HikariConfigMXBean hikariConfigMXBean = hikariDataSource.getHikariConfigMXBean();
            hikariConfigMXBean.setUsername(dbProperties.getProperty(DBConstants.USERNAME).toString());
            hikariConfigMXBean.setPassword(dbProperties.getProperty(DBConstants.PASS_WORD).toString());
            ((DataSourceProxy)poolDataSource).setUsername(dbProperties.getProperty(DBConstants.USERNAME).toString());
            ((DataSourceProxy)poolDataSource).setPassword(dbProperties.getProperty(DBConstants.PASS_WORD).toString());
            HikariPoolMXBean hikariPoolMXBean = hikariDataSource.getHikariPoolMXBean();
            // if (hikariPoolMXBean != null) {
            hikariPoolMXBean.softEvictConnections();
            // }
            log.info("Value" + dataSource.toString());
            log.info("Updated datasource value.....");
            System.out.println("Value" + dataSource.toString());
            System.out.println("Updated datasource value.....");
        } catch (Exception e) {
            log.error("Exception occurred trying updating the datasource with error: " + e);
        }
    }
}
