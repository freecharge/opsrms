package com.snapdeal.payments.vault;

/**
 * @author smruti.sahoo
 */
public class VaultConstants {

    private VaultConstants(){
        throw new IllegalStateException("VaultConstants");
    }

    //Vault URLs
    public static final String VAULT_BASE_URL = "https://vault.freecharge.in";
    public static final String TOKEN_HEADER_KEY = "X-Vault-Token";
    public static final String VAULT_ENVIRONMENT = "deployment_environment";
    public static final String DB_CREDS_URL = "https://vault.freecharge.in/v1/database/creds/%s_oprms_%s_write";


    public static final String VAULT_TOKEN_URL = "https://vault.freecharge.in/v1/auth/ldap/login/%s";
    public static final String URI = "https://vault.freecharge.in/v1/databags/data/%s/oprms";

    public static final String APP_PROPERTIES_PATH = "databags/data/%s/oprms/application.properties";
    public static final String LOG_PROPERTIES_PATH = "databags/data/%s/oprms/log.properties";
    public static final String DB_PROPERTIES_PATH = "databags/data/%s/oprms/db.properties";
    public static final String CONFIG_PROPERTIES_PATH = "databags/data/%s/oprms/config.properties";

    public static final String DB_USERNAME = "username";
    public static final String DB_PASS_WORD = "password";

    public static final String VAULT_USER_NAME = "vault_username";
    public static final String VAULT_PASS_WORD = "vault_password";

}
