package com.snapdeal.payments.vault;

public class DBConstants {
    private DBConstants(){
        throw new IllegalStateException("DBConstants");
    }
    public static final String JDBC_URL = "oprms.database.url";
    public static final String PASS_WORD = "oprms.database.password";
    public static final String USERNAME = "oprms.database.username";
    public static final String DRIVER_CLASSNAME = "oprms.database.driverClassName";
    public static final String MIN_Idle="oprms.database.minIdle";
    public static final String MAX_ACTIVE="oprms.database.maxactive";
}
