package com.snapdeal.payments.vault;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.vault.authentication.ClientAuthentication;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.config.AbstractVaultConfiguration;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


import java.net.URI;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author smruti.sahoo
 * */

@Slf4j
@Configuration("vaultConfig")
@Profile({"qa", "staging", "prod"})
public class VaultConfig extends AbstractVaultConfiguration {

    @Value("${spring.profiles.active:staging}")
    private String env;
    private String vaultToken;

    private static final String VAULT_USER_NAME = System.getenv(VaultConstants.VAULT_USER_NAME);

    private static final String VAULT_PASSWORD = System.getenv(VaultConstants.VAULT_PASS_WORD);

    static {
        log.info("OPRMS vault username "+ VAULT_USER_NAME );
        log.info("OPRMS vault password "+ VAULT_PASSWORD );
    }


    @Override
    public VaultEndpoint vaultEndpoint() {
        try {
            final String vaultUri = String.format(VaultConstants.URI, env);
            log.info("Creating vault endpoint for environment {} and URL is {}", System.getenv(VaultConstants.VAULT_ENVIRONMENT),vaultUri);
            VaultEndpoint endpoint = VaultEndpoint.from(new URI(vaultUri));
            System.out.println("system out log :: vault endpoint is " +endpoint);
            log.info("vault endpoint is {}",endpoint);
            return endpoint;
        } catch (Exception ex) {
            log.error("Exception occurred while connecting to the Vault endpoint : " + ex);
        }
        return null;
    }

    @SneakyThrows
    @Override
    public ClientAuthentication clientAuthentication() {
        return new TokenAuthentication(getToken());
    }

    @SneakyThrows
    public String getToken() {
        if (vaultToken == null) {
            vaultToken = getTokenFromVault();
        }
        log.info("vaultToken test: "+ vaultToken);
        return vaultToken;
    }

    private  String getTokenFromVault()  {
        final String vaultTokenURI = String.format(VaultConstants.VAULT_TOKEN_URL, VAULT_USER_NAME);
        log.info("username {} and password {} and Vault Token URL {} ", VAULT_USER_NAME,VAULT_PASSWORD,vaultTokenURI);
        String token ="";

        RestTemplate restTemplate = new RestTemplate();
        RetryTemplate retryTemplate = retryTemplate();

        Map<String, String> request = new HashMap<>();
        request.put("password", VAULT_PASSWORD);
        try {
           ResponseEntity<Object> response  = retryTemplate.execute(
                    (RetryCallback<ResponseEntity<Object>, Throwable>)
                            retryContext -> restTemplate.postForEntity(vaultTokenURI, request, Object.class));

            log.info("Vault Api call status code: " + response.getStatusCode());
            Map<String, Object> responseEntityBody = (Map<String, Object>) response.getBody();
            Map<String, Object> authData = (Map<String, Object>) responseEntityBody.get("auth");
            token = (String) authData.get("client_token");
            System.out.println("system out log :: Vault token:" +token);
            log.info("Vault token: " + token);
        } catch (RestClientException e) {
            e.printStackTrace();
            log.error("Exception occurred during Vault API call with error message" + e.toString());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return token;
    }


    public static String getApplicationEnv() {
        return System.getenv(VaultConstants.VAULT_ENVIRONMENT);
    }


    public RetryTemplate retryTemplate() {
        RetryTemplate retryTemplate = new RetryTemplate();
        FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
        fixedBackOffPolicy.setBackOffPeriod(2000l);
        retryTemplate.setBackOffPolicy(fixedBackOffPolicy);
        Map<Class<? extends Throwable>, Boolean> r = new HashMap<>();
        r.put(Exception.class, true);
        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy(3, r);
        retryTemplate.setRetryPolicy(retryPolicy);
        return retryTemplate;
    }

    @Bean
    @Primary
    public VaultTemplate vaultTemplate() {
        return new RetryableVaultTemplate(this.vaultEndpointProvider(), this.clientHttpRequestFactoryWrapper().getClientHttpRequestFactory(), this.sessionManager(), retryTemplate());
    }



}
