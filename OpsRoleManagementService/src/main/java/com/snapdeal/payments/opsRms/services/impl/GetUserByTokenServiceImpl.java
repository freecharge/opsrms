package com.snapdeal.payments.opsRms.services.impl;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.handlers.TokenUtils;
import com.snapdeal.payments.opsRmsModel.dto.TokenDTO;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.GetUserByTokenRequest;
import com.snapdeal.payments.opsRmsModel.request.User;
import com.snapdeal.payments.opsRmsModel.response.GetUserByTokenResponse;
import com.snapdeal.payments.opsRmsModel.services.GetUserByTokenService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("getUserByToken")
public class GetUserByTokenServiceImpl implements GetUserByTokenService {

	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private RequestParamValidatorForRole<GetUserByTokenRequest> requestValidator;


	@Value("${mob.sessionTimeForMobRevamp}")
	private int sessionTimeForMobRevamp;
	
	@Override
	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetUserByTokenResponse getUserByToken(GetUserByTokenRequest userrequest) {
		requestValidator.validate(userrequest);
		String token = userrequest.getRequestToken();
		String tokenId = TokenUtils.getTokenIdFromToken(token);
		TokenDTO tokenObj = persistanceManager.getUserNameFromId(tokenId);
		if (null == tokenObj || !tokenObj.getActive()
				|| tokenObj.getLoginTime().before(new DateTime().minusHours(TokenUtils.getDEFAULT_TIMEOUT()).toDate()))
			throw new ValidationExceptionOps(ExceptionMessages.SESSION_EXPIRED);
		User user = persistanceManager.getUserByUserName(tokenObj.getUserName());
		return getResponseFromUser(user);
	}
	
	//Created due to CS-211 (Different Session time needed for mob revamp)
	@Override
	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetUserByTokenResponse getUserByTokenForMerchant(GetUserByTokenRequest userrequest) {
	
		requestValidator.validate(userrequest);
		String token = userrequest.getRequestToken();
		String tokenId = TokenUtils.getTokenIdFromToken(token);
		TokenDTO tokenObj = persistanceManager.getUserNameFromId(tokenId);
		
		if (null == tokenObj || !tokenObj.getActive()
				|| tokenObj.getLoginTime().before(new DateTime().minusMinutes(sessionTimeForMobRevamp).toDate()))
			throw new ValidationExceptionOps(ExceptionMessages.SESSION_EXPIRED);
		log.info("getUserByTokenForMerchant() is called for a session timeout of {} minutes",sessionTimeForMobRevamp);
		User user = persistanceManager.getUserByUserName(tokenObj.getUserName());
		return getResponseFromUser(user);
	}
	

	private GetUserByTokenResponse getResponseFromUser(User data) {
		return new GetUserByTokenResponse(data);
	}
}
