package com.snapdeal.payments.opsRms.services.impl;

import java.text.MessageFormat;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Optional;
import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.IDFactory;
import com.snapdeal.payments.opsRms.handlers.OTPUtils;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRmsModel.commons.OTPConstants;
import com.snapdeal.payments.opsRmsModel.dto.FreezeAccountDTO;
import com.snapdeal.payments.opsRmsModel.dto.UserOTPDTO;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.opsRmsModel.exceptions.OTPServiceExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.User;
import com.snapdeal.payments.opsRmsModel.request.VerifyOTPRequest;
import com.snapdeal.payments.opsRmsModel.response.FrozenAccountResponse;
import com.snapdeal.payments.opsRmsModel.services.VerifyOTPService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aniket 
 * 5-Jan-2016
 */
@Service
@Slf4j
public class VerifyOTPServiceImpl implements VerifyOTPService {
	
	
	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private RequestParamValidatorForRole<VerifyOTPRequest> requestValidator;
	
	@Autowired
	private IDFactory idfactory;
	
	@Autowired
	private OTPUtils otpUtils;

	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	@Override
	public void verifyOTP(VerifyOTPRequest verifyOTPRequest) {
		requestValidator.validate(verifyOTPRequest);
		int status;
		Optional<UserOTPDTO> currentOtpInfo = persistanceManager
				.getOTPFromId(verifyOTPRequest.getOtpId());
		
		if (currentOtpInfo.isPresent()) {
			
			status = verify(currentOtpInfo, verifyOTPRequest);
		} else {
			throw new OTPServiceExceptionOps(ExceptionMessages.INVALID_OTP_ID);
		}
		User user = persistanceManager.getUserById(currentOtpInfo.get().getUserId());
		if (status == OTPConstants.STATUS) {
			String hashedPassword = IDFactory.hashString(verifyOTPRequest.getNewPassword());
			persistanceManager.resetPassword(user.getUserName(), hashedPassword,
					new DateTime().plusDays(idfactory.getPasswordExpiryInDays()).toDate());
		} else {
			throw new OTPServiceExceptionOps(ExceptionMessages.INVALID_OTP_ENTERED);
		}
		
	}
	
	public int verify(Optional<UserOTPDTO> currentOtpInfo,
			VerifyOTPRequest request) {
		int status;
		FrozenAccountResponse frozenAccountResponse = isUserFrozen(currentOtpInfo
				.get());
		if (activeOrNot(currentOtpInfo)) {
			if (currentOtpInfo.get().getOtp()
					.equalsIgnoreCase(request.getOtp())) {
				otpUtils.updateOTPState(currentOtpInfo.get());
				if (frozenAccountResponse.isStatus() == true
						&& frozenAccountResponse.getRequestType().equals(
								OTPConstants.FROZEN_REASON_RESEND_ATTEMPTS)) {
					persistanceManager.dropFreezedUser(currentOtpInfo.get().getUserId());
				}
				status = 200; // OTP matched
				return status;
			} else {
				// increase invalid attempts
				otpUtils.updateInvalidAttempts(currentOtpInfo);
				// if invalid attempts exceeds maximum
				if (currentOtpInfo.get().getInvalidAttempts() >= otpUtils
						.getInvalidAttemptsLimit()) {
					// if user is in already frozen table the change attempts to
					// invalid attempts.
					if (frozenAccountResponse.isStatus() == true
							&& frozenAccountResponse.getRequestType().equals(
									OTPConstants.FROZEN_REASON_RESEND_ATTEMPTS)) {
						otpUtils.updateBlockUser(currentOtpInfo.get());
					} else {
						// if not then block him with reason invalid_attempts.
						otpUtils.blockUser(currentOtpInfo.get(),OTPConstants.FROZEN_REASON_INVALID_ATTEMPTS);
					}
				}
				throw new OTPServiceExceptionOps(ExceptionMessages.INVALID_OTP_ENTERED);
			}
		} else {
			// throwing exception in case of OTP is not in ACTIVE state
		   throw new OTPServiceExceptionOps(ExceptionMessages.EXPIRED_OTP_ERROR);
		}
	}

	private boolean activeOrNot(Optional<UserOTPDTO> currentOtpInfo) {
		
		String state = OTPUtils.getOtpState(currentOtpInfo).toString();
		if (state.equalsIgnoreCase("ACTIVE")
				|| state.equalsIgnoreCase("IN_THRESHOLD")
				|| state.equalsIgnoreCase("IN_EXPIRY")) {
			return true;
		}

		return false;
	}
	
	private FrozenAccountResponse isUserFrozen(UserOTPDTO otp) {

		Optional<FreezeAccountDTO> frozenAccountEntity = persistanceManager.getFreezedAccount(otp.getUserId());
		FrozenAccountResponse frozenAccountResponse = OTPUtils.calculateFrozenAccountResponse(frozenAccountEntity);
		if (frozenAccountResponse.isStatus() == true
				&& frozenAccountResponse.getRequestType().equalsIgnoreCase(
						OTPConstants.FROZEN_REASON_INVALID_ATTEMPTS)) {
			throw new OTPServiceExceptionOps(
                  MessageFormat.format(ExceptionMessages.VERIFY_OTP_LIMIT_BREACHED,
                           frozenAccountResponse.getRemainingMinutes() / 1));
		}
		
		return frozenAccountResponse;
	}

}
