package com.snapdeal.payments.opsRms.dao;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Optional;
import com.snapdeal.payments.opsRmsModel.commons.OTPState;
import com.snapdeal.payments.opsRmsModel.dto.UpdateInvalidAttemptsDTO;
import com.snapdeal.payments.opsRmsModel.dto.UpdateOTPStateDTO;
import com.snapdeal.payments.opsRmsModel.dto.UpdateResendAttemptsDTO;
import com.snapdeal.payments.opsRmsModel.dto.UserOTPDTO;

@Component
public class OTPDaoImpl implements OTPDao {

	@Autowired
	private SqlSessionTemplate sqlSession;

	@Transactional
	@Override
	public Optional<UserOTPDTO> getLatestOTP(String userId) {
		// tested
		List<UserOTPDTO> otpDaos = new ArrayList<UserOTPDTO>();

		otpDaos = sqlSession.selectList("otp.getLatestOTP", userId);

		if (otpDaos == null || otpDaos.isEmpty()) {
			return Optional.<UserOTPDTO> absent();
		} else {
			UserOTPDTO otp = otpDaos.get(0);
			if (otp.getOtpState() != OTPState.ACTIVE) {
				return Optional.<UserOTPDTO> absent();
			} else {
				return Optional.<UserOTPDTO> of(otpDaos.get(0));
			}
		}
	}

	@Override
	public void updateCurrentOTPStatus(UpdateOTPStateDTO otpStatusInfo) {
		sqlSession.update("otp.updateCurrentOtpState", otpStatusInfo);

	}

	@Override
	public void saveOTP(UserOTPDTO otpDaoInfo) {
		sqlSession.insert("otp.saveOTP", otpDaoInfo);
	}

	@Override
	public void incrementResendAttempts(UpdateResendAttemptsDTO resendAttemptsInfo) {
		sqlSession.update("otp.incrementResendAttempts", resendAttemptsInfo);

	}

	@Override
	public Optional<UserOTPDTO> getOTPFromId(String otpId) {
		List<UserOTPDTO> otpDaos = new ArrayList<UserOTPDTO>();

		otpDaos = sqlSession.selectList("otp.isOtpPresent", otpId);
		if (otpDaos == null || otpDaos.isEmpty()) {
			return Optional.<UserOTPDTO> absent();
		} else {
			UserOTPDTO otp = otpDaos.get(0);
			if (otp.getOtpState() != OTPState.ACTIVE) {
				return Optional.<UserOTPDTO> absent();
			} else {
				return Optional.<UserOTPDTO> of(otpDaos.get(0));
			}
		}

	}

	@Override
	public void incrementInvalidAttempts(UpdateInvalidAttemptsDTO invalidAttemptsInfo) {
		sqlSession.update("otp.incrementInvalidAttempts", invalidAttemptsInfo);
	}

}
