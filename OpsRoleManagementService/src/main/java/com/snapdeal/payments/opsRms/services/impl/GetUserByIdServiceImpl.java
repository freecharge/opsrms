/**
 * 26-Oct-2015
 *shubham
 */
package com.snapdeal.payments.opsRms.services.impl;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRmsModel.request.GetUserByIdRequest;
import com.snapdeal.payments.opsRmsModel.request.User;
import com.snapdeal.payments.opsRmsModel.response.GetUserByIdResponse;
import com.snapdeal.payments.opsRmsModel.services.GetUserByIdService;


@Service
@Slf4j
@Qualifier("getUserById")
public class GetUserByIdServiceImpl implements GetUserByIdService {

   @Autowired
   private PersistanceManager persistanceManager;

   @Autowired
   private RequestParamValidatorForRole<GetUserByIdRequest> requestValidator;

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetUserByIdResponse getUserById(GetUserByIdRequest userrequest) {
		requestValidator.validate(userrequest);
		return getResponseFromUser(persistanceManager.getUserById(userrequest.getUserId()));
	}

   private GetUserByIdResponse getResponseFromUser(User data) {
      return new GetUserByIdResponse(data);
   }
}
