package com.snapdeal.payments.opsRms.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.payments.opsRms.constant.OprmsUtilConstant;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.communicator.client.ISendCommunicationIntentClient;
import com.snapdeal.payments.communicator.constants.JSONConstants;
import com.snapdeal.payments.communicator.dto.CommunicationIntent;
import com.snapdeal.payments.communicator.dto.CommunicationType;
import com.snapdeal.payments.communicator.dto.Party;
import com.snapdeal.payments.communicator.dto.PartyIdentity;
import com.snapdeal.payments.communicator.dto.PartyIdentityType;
import com.snapdeal.payments.communicator.dto.PartyType;
import com.snapdeal.payments.communicator.dto.Source;
import com.snapdeal.payments.communicator.exception.HttpTransportException;
import com.snapdeal.payments.communicator.exception.ServiceException;
import com.snapdeal.payments.communicator.request.SendCommunicationIntentRequest;
import com.snapdeal.payments.communicator.response.SendCommunicationIntentResponse;
import com.snapdeal.payments.opsRmsModel.commons.CommunicationConstants;
import com.snapdeal.payments.opsRmsModel.dto.UserOTPDTO;
import com.snapdeal.payments.opsRmsModel.request.CreateUserRequest;
import com.snapdeal.payments.opsRmsModel.request.EmailTemplate;
import java.text.SimpleDateFormat;
import java.util.Date;
@Component
@Slf4j
public class CommunicatorUtils {
	
	@Autowired
	private ISendCommunicationIntentClient sendCommunicationIntentClient;
	
	public CommunicationIntent generateCommunicationIntent(String email, UserOTPDTO otp) {
		log.info("Generating communication intent for userEmail: {}", email);
		CommunicationIntent intent = new CommunicationIntent();
		getCommonParamsForEmail(intent, email);

		CommunicationType comType = new CommunicationType();
		Map<String, Object> metaData = new HashMap<>();
		metaData.put(JSONConstants.EVENT_TYPE, CommunicationConstants.RMS_GENERATE_OTP);
		metaData.put(JSONConstants.EVENT_STATE, CommunicationConstants.SUCCESS);
		comType.setMetadata(metaData);
		comType.setSource(Source.TRIGGER);
		intent.setCommunicationType(comType);
		
		Map<String,String> requestParams =  new HashMap<>();

		requestParams.put("otp", otp.getOtp());
		
		intent.setRequestParams(requestParams);

		return intent;
	}

	public CommunicationIntent generateCommunicationIntent(
			CreateUserRequest user, String url) {

		log.info("Generating communication intent for userEmail: {}", user.getEmail());
		CommunicationIntent intent = new CommunicationIntent();
		getCommonParamsForEmail(intent, user.getEmail());

		CommunicationType comType = new CommunicationType();
		Map<String, Object> metaData = new HashMap<>();
		if (user.getEmailTemplate()
				.getKey()
				.equalsIgnoreCase(
						EmailTemplate.DEBUGGER_DASHBOARD_SETPASSWORD.toString())) {
			metaData.put(JSONConstants.EVENT_TYPE,
					OprmsUtilConstant.RMS_DEBUGGER_DASHBOARD_SETPSWD);
			metaData.put(JSONConstants.EVENT_STATE,
					CommunicationConstants.SUCCESS);
		} else if (user
				.getEmailTemplate()
				.getKey()
				.equalsIgnoreCase(EmailTemplate.OP_PANEL_SETPASSWORD.toString())) {
			metaData.put(JSONConstants.EVENT_TYPE,
					OprmsUtilConstant.RMS_OP_PANEL_SETPSWD);
			metaData.put(JSONConstants.EVENT_STATE,
					CommunicationConstants.SUCCESS);
		} else if (user
				.getEmailTemplate()
				.getKey()
				.equalsIgnoreCase(
						EmailTemplate.MERCHANT_PANEL_SETPASSWORD.toString())) {
			metaData.put(JSONConstants.EVENT_TYPE,
					OprmsUtilConstant.RMS_MERCHANT_PANEL_SETPSWD);
			metaData.put(JSONConstants.EVENT_STATE,
					CommunicationConstants.SUCCESS);
		}

		comType.setMetadata(metaData);
		comType.setSource(Source.TRIGGER);
		intent.setCommunicationType(comType);

		Map<String, String> requestParams = new HashMap<>();
		requestParams.put("link", url);

		intent.setRequestParams(requestParams);
		return intent;
	}
	

	
	public CommunicationIntent generateCommunicationIntentForMerchant(
			 String url,String email,String mobile,String userName) {

		log.info("Generating communication intent for Merchant userEmail: {}", email);
		CommunicationIntent intent = new CommunicationIntent();
		getCommonParams(intent, email,mobile);

		CommunicationType comType = new CommunicationType();
		Map<String, Object> metaData = new HashMap<>();
	
			metaData.put(JSONConstants.EVENT_TYPE,
					OprmsUtilConstant.RMS_MERCHANT_PANEL_SETPSWD);
			metaData.put(JSONConstants.EVENT_STATE,
					CommunicationConstants.SUCCESS);
		

		comType.setMetadata(metaData);
		comType.setSource(Source.TRIGGER);
		intent.setCommunicationType(comType);

		Map<String, String> requestParams = new HashMap<>();
		requestParams.put("link", url);
		requestParams.put("userName", userName);

		intent.setRequestParams(requestParams);
		return intent;
	}
		
	public CommunicationIntent generateSMSCommunicationIntent(String email, String mobile, String otp, String name) {

		CommunicationIntent intent = new CommunicationIntent();
		getCommonParams(intent, email, mobile);
		
		CommunicationType comType = new CommunicationType();
		Map<String, Object> metaData = new HashMap<>();
	
		metaData.put(JSONConstants.EVENT_TYPE, CommunicationConstants.MERCHANT_CREATION_OTP);
		metaData.put(JSONConstants.EVENT_STATE, CommunicationConstants.SUCCESS);

		comType.setMetadata(metaData);
		comType.setSource(Source.TRIGGER);
		intent.setCommunicationType(comType);
		
		Date date=new Date();  
		SimpleDateFormat formatter = new SimpleDateFormat("hh.mm,dd MMM yyyy, EEEE");
		String strDate= formatter.format(date);
		
		String mobno = null;
		if(!StringUtils.isBlank(mobile))
		mobno = mobile.replace(mobile.substring(0,6),"XXXXXX");
		Map<String, String> requestParams = new HashMap<>();
		requestParams.put("otp", otp);
		requestParams.put("name", name);
		requestParams.put("current_time", strDate);
		requestParams.put("mobile", mobno);
		intent.setRequestParams(requestParams);
		return intent;
	}
	
	
	private void getCommonParamsForEmail(CommunicationIntent intent, String email) {
		intent.setVersion(1);
		intent.setIdempotencyId(getIdempotencyId());
		intent.setIdempotencyTimeStamp(System.currentTimeMillis());
		
		List<Party> participants = new ArrayList<Party>();

		Party receiver = new Party();
		//receiver.setSender(false);
		receiver.setType(PartyType.RECEIVER);
		
		List<PartyIdentity> partyIdentitiesR = new ArrayList<PartyIdentity>();
		log.info("Creating intent for email {}",email);
		PartyIdentity emailIdentityR = new PartyIdentity(PartyIdentityType.EMAIL, email);
		partyIdentitiesR.add(emailIdentityR);
		
		receiver.setPartyIdentities(partyIdentitiesR);
		participants.add(receiver);
		intent.setParticipants(participants);
	}
	
	
	private void getCommonParams(CommunicationIntent intent, String email, String mobile) {
		intent.setVersion(1);
		intent.setIdempotencyId(getIdempotencyId());
		intent.setIdempotencyTimeStamp(System.currentTimeMillis());
		
		List<Party> participants = new ArrayList<Party>();

		Party receiver = new Party();
		//receiver.setSender(false);
		receiver.setType(PartyType.RECEIVER);
		
		List<PartyIdentity> partyIndentityList = new ArrayList<PartyIdentity>();
        log.info("Creating intent for mobile {} and email {}",mobile,email);
		PartyIdentity partyIdentityMobile = new PartyIdentity(PartyIdentityType.MOBILE, mobile);
		PartyIdentity partyIdentityEmail = new PartyIdentity(PartyIdentityType.EMAIL, email);
		
		partyIndentityList.add(partyIdentityMobile);
		partyIndentityList.add(partyIdentityEmail);
		
		receiver.setPartyIdentities(partyIndentityList);
		participants.add(receiver);
		intent.setParticipants(participants);
	}
	
	
	
	
	private String getIdempotencyId(){
		String idempotencyId = "RMS_"+RandomStringUtils.randomNumeric(10);
        return idempotencyId;
	}
	
	public void sendCommunicationIntent(CommunicationIntent intent) throws HttpTransportException, ServiceException {
		SendCommunicationIntentRequest request = new SendCommunicationIntentRequest();
		request.setCommunicationIntent(intent);
		SendCommunicationIntentResponse response = sendCommunicationIntentClient.sendCommunicationIntentResponse(request);
	}

}
