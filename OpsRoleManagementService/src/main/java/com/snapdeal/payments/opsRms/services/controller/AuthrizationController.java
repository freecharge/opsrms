package com.snapdeal.payments.opsRms.services.controller;

import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.handlers.AuthorizeHandlerImpl;
import com.snapdeal.payments.opsRmsModel.commons.OPRMSPreAuthorize;
import com.snapdeal.payments.opsRmsModel.commons.RestURIConstants;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionErrorCode;
import com.snapdeal.payments.opsRmsModel.exceptions.OpsRoleMgmtException;
import com.snapdeal.payments.opsRmsModel.exceptions.UnAuthrizedExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.AuthrizeUserRequest;
import com.snapdeal.payments.opsRmsModel.response.ServiceResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@Slf4j
public class AuthrizationController extends AbstractController {

    @Autowired
    private AuthorizeHandlerImpl authHandler;

    @OPRMSPreAuthorize
    @RequestAware
    @Marked
    @Logged(printStackTraceEnabled = false)
    @Timed
    @RequestMapping(value = RestURIConstants.PREAUTHRIZE_TO_USER, method = RequestMethod.GET)
    public ServiceResponse<Void> getUsersByRole(@ModelAttribute AuthrizeUserRequest permissionRequest)
            throws UnAuthrizedExceptionOps, ParseException {

        ServiceResponse<Void> response = new ServiceResponse<Void>();
        authHandler.handleAuthrization(permissionRequest.getPreAuthrizeString());
        return response;
    }

    @ExceptionHandler(OpsRoleMgmtException.class)
    public <T> ServiceResponse<T> handleException(OpsRoleMgmtException exception) {
        ServiceResponse<T> response = new ServiceResponse<T>();
        OpsRoleMgmtException roleException = new OpsRoleMgmtException(exception.getMessage());
        roleException.setErrorCode(exception.getErrorCode());
        response.setException(roleException);
        return response;
    }

    @ExceptionHandler(Exception.class)
    public <T> ServiceResponse<T> handleException(Exception exception) {
        log.info("Exception {} " , exception.getMessage());
        ServiceResponse<T> response = new ServiceResponse<T>();
        OpsRoleMgmtException roleException = new OpsRoleMgmtException("Internal Server Error. Please try after some time");
        roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
        response.setException(roleException);
        return response;
    }

    @ExceptionHandler(RuntimeException.class)
    public <T> ServiceResponse<T> handleException(RuntimeException exception) {
        log.info("Runtime Exception {} " , exception.getMessage());
        ServiceResponse<T> response = new ServiceResponse<T>();
        OpsRoleMgmtException roleException = new OpsRoleMgmtException("Internal Server Error. Please try after some time");
        roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
        response.setException(roleException);
        return response;
    }
}
