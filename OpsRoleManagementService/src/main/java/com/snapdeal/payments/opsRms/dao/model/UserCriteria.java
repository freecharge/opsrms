package com.snapdeal.payments.opsRms.dao.model;

import java.util.List;

import com.snapdeal.payments.opsRmsModel.dto.FieldFilter;
import com.snapdeal.payments.opsRmsModel.dto.RangeFilter;
import com.snapdeal.payments.opsRmsModel.dto.SortField;

import lombok.Data;

@Data
public class UserCriteria {
   private List<FieldFilter> filters;
   private List<RangeFilter> rangeFilter;
   private List<SortField> sortColumns;
   private List<String> searchColumns;
   private String searchString;
   private Integer recordsPerPage;
   private Integer offset;
}
