package com.snapdeal.payments.opsRms.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.snapdeal.payments.opsRms.dao.model.PermissionModel;
import com.snapdeal.payments.opsRmsModel.request.AddPermissionRequest;
import com.snapdeal.payments.opsRmsModel.request.UserPermission;
import com.snapdeal.payments.opsRmsModel.request.UserPermissionRequest;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.opsRms.dao.model.PermissionUserMapperModel;
import com.snapdeal.payments.opsRmsModel.request.Permission;

@Component
@Slf4j
public class UserPermissionDaoImpl implements UserPermissionDao {

   @Autowired
   private SqlSessionTemplate sqlSession;

   @Override
   public List<Permission> getAllPermissions() {

      List<Permission> permissions = sqlSession.selectList("permission.getAllPermissions");
      return permissions;
   }

   @Override
   public List<Permission> getAllPermissionsByUser(String userName) {
      List<Permission> permissions = sqlSession.selectList("permission.getAllPermissionsByUser",
               userName);
      return permissions;
   }

   @Override
   public List<Permission> getAllPermissionsByRoleId(Integer roleId) {
      List<Permission> permissions = sqlSession.selectList("permission.getAllPermissionsByRole",
               roleId);
      return permissions;
   }

   @Override
   public Map<String, Integer> getAllUserPermissions(String userName) {
      Map<String, String> inputMap = new HashMap<String, String>();
      inputMap.put("userName", userName);

      Map<String, Integer> userPermissionMap = sqlSession.selectMap(
               "permission.getAllPermissionsByUserMap", inputMap, "name");
      return userPermissionMap;
   }

   @Override
   public void mapAllPermissionsWithUser(PermissionUserMapperModel userPermissionMapper) {
      sqlSession.insert("permission.saveRolePermissionMapping", userPermissionMapper);

   }

   @Override
   public void deleteUserPermissionMappingByUser(String userId) {
      sqlSession.delete("permission.deleteUserPermissionMappingByUser", userId);

   }
   
   @Override
   public List<String> getUsersByPermissionCriteria(Set<String> userIds,String columnName,String searchString){
	   Map<String,String> requestObjectForPermissions = new HashMap<String,String>();
	   //requestObject.put("userIds", userIds);
	   requestObjectForPermissions.put("columnName", columnName);
	   requestObjectForPermissions.put("searchString", searchString);
	   List<String> validPermissionsList = new ArrayList<String>();
	   validPermissionsList = sqlSession.selectList("permission.getPermissionsByCriteria",requestObjectForPermissions);
	   Map<String,Object> requestObjectForUserPermissionMapping = new HashMap<String,Object>();
	   requestObjectForUserPermissionMapping.put("userIds", userIds);
	   requestObjectForUserPermissionMapping.put("permissionIds", validPermissionsList);
	   List<String> userByPermissionsList = sqlSession.selectList("permission.getUsersByPermissionCriteria",requestObjectForUserPermissionMapping);
	   return userByPermissionsList;
   }

	@Override
	public List<Permission> getAllPermissionsByUserId(String userId) {
		List<Permission> permissions = sqlSession.selectList("permission.getAllPermissionsByUserId", userId);
		return permissions;
	}

    @Override
    public List<String> getAllAppNamesFromPermissions() {
        List<String> appNameList = sqlSession.selectList("permission.getAllAppNamesFromPermissions");
        log.info("appNameList: "+ appNameList);
        return appNameList;
    }

    @Override
    public List<Permission> getAllPermissionsBasedOnCriteria(UserPermissionRequest request) {
        log.info("request: " + request);
        if (request == null)
            return null;

        String permissionName = request.getPermissionName();
        String appName = request.getAppName();
        String displayText = request.getDisplayText();
        List<Permission> permissionList=null;
        if (request.getPermissionName() != null)
             permissionList = sqlSession.selectList("permission.getAllPermissionsByPermissionName", permissionName);
        else if (request.getAppName() != null)
            permissionList = sqlSession.selectList("permission.getAllPermissionsByAppName", appName);
        else if (request.getDisplayText() != null)
            permissionList = sqlSession.selectList("permission.getAllPermissionsByDisplayText", displayText);

        log.info("permissionList after DB: "+ permissionList);
        return permissionList;
    }

    @Override
    public Integer addNewPermissions(AddPermissionRequest request) throws RuntimeException {
        log.info("addNewPermission request: "+ request);
        PermissionModel permissionModel = new PermissionModel();
        permissionModel.setAppName(request.getAppName());
        permissionModel.setPermissionName(request.getPermissionName());
        permissionModel.setDisplayName(request.getDisplayName());
        try {
            sqlSession.insert("permission.addNewPermission", permissionModel);
        }catch (RuntimeException rx){
            throw new RuntimeException("Exception occurred while inserting data..."+rx.toString());
        }
        //fetching id for the added permission
        String permissionName = request.getPermissionName();
        Integer permissionId = sqlSession.selectOne("permission.getPermissionByName", permissionName);
        log.info("Newly added permissionId: "+permissionId);
        return permissionId;
    }


}
