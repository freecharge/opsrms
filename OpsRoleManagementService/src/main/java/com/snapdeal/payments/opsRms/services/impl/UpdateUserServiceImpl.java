package com.snapdeal.payments.opsRms.services.impl;
import lombok.extern.slf4j.Slf4j;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.dao.model.ColumnsConstants;
import com.snapdeal.payments.opsRms.dao.model.PermissionUserMapperModel;
import com.snapdeal.payments.opsRms.dao.model.RoleUserMapperModel;
import com.snapdeal.payments.opsRms.dao.model.UserDaoModel;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.opsRmsModel.exceptions.NoSuchUserExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.GetUserByIdRequest;
import com.snapdeal.payments.opsRmsModel.request.GetUserByTokenRequest;
import com.snapdeal.payments.opsRmsModel.request.UpdateUserRequest;
import com.snapdeal.payments.opsRmsModel.request.User;
import com.snapdeal.payments.opsRmsModel.response.GetUserByIdResponse;
import com.snapdeal.payments.opsRmsModel.response.GetUserByTokenResponse;
import com.snapdeal.payments.opsRmsModel.services.GetUserByIdService;
import com.snapdeal.payments.opsRmsModel.services.GetUserByTokenService;
import com.snapdeal.payments.opsRmsModel.services.UpdateUserService;
import com.snapdeal.payments.opsRmsModel.request.Permission;

/**
 * @author shubham
 *         26-Oct-2015
 */
@Service
@Slf4j
public class UpdateUserServiceImpl implements UpdateUserService {
   @Autowired
   private PersistanceManager persistanceManager;

   @Autowired
   private RequestParamValidatorForRole<UpdateUserRequest> requestValidator;
   
    @Autowired
	@Qualifier("getUserByToken")
	private GetUserByTokenService getUserByTokenService;
	
	@Autowired
	@Qualifier("getUserById")
	private GetUserByIdService getUserByIdService;

	private static final Logger auditLog = LoggerFactory.getLogger(ColumnsConstants.AUDIT_LOG);
	
	@Override
	@Transactional
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public void updateUser(UpdateUserRequest user) {
		requestValidator.validate(user);
		verifyIfUserExist(user.getUserId());
		List<Permission> allPermissions = persistanceManager.getAllPermissionsByUserId(user.getUserId());
		Map<Integer, String> allPermissionMap = mapPermissionToIdName(allPermissions);
		Map<Integer, String> revokedPermissionMap = new HashMap<>(allPermissionMap);
		revokedPermissionMap.keySet().removeAll(user.getPermissionIds());
		persistanceManager.updateUser(getUserDaoFromRequest(user));
		if (null != user.getPermissionIds() && !user.getPermissionIds().isEmpty()) {
			persistanceManager.deleteUserPermissionMappingByUser(user.getUserId());
			persistanceManager.mapAllPermissionsWithUser(getPermissionMapperFromRequest(user));
		}

		if (null != user.getRoleIds() && !user.getRoleIds().isEmpty()) {
			persistanceManager.deleteUserRoleMappingByUser(user.getUserId());
			persistanceManager.addUserRoleMapping(getRoleMapperFromRequest(user));
		}
		
		
		try {
			String appName = user.getAppName();
			String token = user.getRequestToken();
			String userIdCallingtheAPI = null;
			String userNameCallingtheAPI = null;
			String NameCallingtheAPI = null;
			if(!StringUtils.isBlank(token)) {
			GetUserByTokenRequest req = new GetUserByTokenRequest();
			req.setRequestToken(token);
			GetUserByTokenResponse res = getUserByTokenService.getUserByToken(req);
			User user_calling_the_API = res.getUser();
			userIdCallingtheAPI = user_calling_the_API.getId();
			userNameCallingtheAPI = user_calling_the_API.getUserName();
			NameCallingtheAPI = user_calling_the_API.getName();
			}
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			String time = dateFormat.format(date);
			GetUserByIdRequest getUserByIdRequest = new GetUserByIdRequest();
			getUserByIdRequest.setUserId(user.getUserId());
			GetUserByIdResponse getUserByIdResponse = getUserByIdService.getUserById(getUserByIdRequest);
			User userInfoAfterUpdating = getUserByIdResponse.getUser();
			List<Permission> allPermissionsUpdated = persistanceManager.getAllPermissionsByUserId(user.getUserId());
			Map<Integer, String> addedPermissionMap = mapPermissionToIdName(allPermissionsUpdated);
			addedPermissionMap.keySet().removeAll(allPermissionMap.keySet());
			User userUpdated = persistanceManager.getUserById(user.getUserId());
			auditLog.info("user_update for under JIRA ID: {} for user: {} with userId: {} by user: {} with userId: {}. Permissions given: {}. Permissions revoked: {}.",user.getJiraId(), userUpdated.getEmail(), user.getUserId(), userNameCallingtheAPI, userIdCallingtheAPI, addedPermissionMap.values(), revokedPermissionMap.values());
			log.info("USER_UPDATED : UpdateUserServiceImpl is called,application name calling the API is : {} ,Information of the user calling the API is --> userId : {} ,userNameCallingtheAPI : {} ,NameCallingtheAPI : {} ,updatedOn : {} ,Information of the user getting updated is --> userID : {} ,"
					+ "Information of user getting updated : {} ",appName,userIdCallingtheAPI,userNameCallingtheAPI,NameCallingtheAPI,time,user.getUserId(),userInfoAfterUpdating);
		}
		catch (Exception e) {
			log.error("Error in logging data on user updation! ",e);
		}
	}

	private Map<Integer, String> mapPermissionToIdName(List<Permission> allPermissions) {
		Map<Integer,String> permissionIdName = new HashMap<>();
		for (Permission permission : allPermissions)
			permissionIdName.put(permission.getId(), permission.getName());
		return permissionIdName;
	}

   private void verifyIfUserExist(String userId) {
      if (!persistanceManager.isUserExist(userId))
         throw new NoSuchUserExceptionOps(ExceptionMessages.NO_SUCH_USER_EXCEPTION);
   }

   private UserDaoModel getUserDaoFromRequest(UpdateUserRequest request) {
      UserDaoModel user = new UserDaoModel();
      user.setId(request.getUserId());
      user.setMobile(request.getMobile());
      user.setName(request.getName());
      user.setActive(request.isActive());
      return user;
   }

   private PermissionUserMapperModel getPermissionMapperFromRequest(UpdateUserRequest request) {
      return new PermissionUserMapperModel(request.getPermissionIds(), request.getUserId());
   }

   private RoleUserMapperModel getRoleMapperFromRequest(UpdateUserRequest request) {
      return new RoleUserMapperModel(request.getRoleIds(), request.getUserId());
   }

}
