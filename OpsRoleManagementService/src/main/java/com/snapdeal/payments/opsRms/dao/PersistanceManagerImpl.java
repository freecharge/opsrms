package com.snapdeal.payments.opsRms.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.experimental.Delegate;

@Component
public class PersistanceManagerImpl implements PersistanceManager {

   @Autowired
   @Delegate
   private UserDao userDao;

   @Autowired
   @Delegate
   private UserPermissionDao userPermissionDao;

   @Autowired
   @Delegate
   private UserRoleDao userRoleDao;

   @Autowired
   @Delegate
   private OTPDao otpDao;
   
   @Autowired
   @Delegate
   private TokenDao tokenDao;
}
