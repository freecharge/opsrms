package com.snapdeal.payments.opsRms.services.controller;

import com.snapdeal.payments.opsRmsModel.request.CreateRoleRequest;
import com.snapdeal.payments.opsRmsModel.request.GetAllPermissionsRequest;
import com.snapdeal.payments.opsRmsModel.request.GetAllRolesRequest;
import com.snapdeal.payments.opsRmsModel.request.GetRolesByRoleNamesRequest;
import com.snapdeal.payments.opsRmsModel.request.UpdateRoleRequest;
import com.snapdeal.payments.opsRmsModel.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRmsModel.commons.OPRMSPreAuthorize;
import com.snapdeal.payments.opsRmsModel.commons.RestURIConstants;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionErrorCode;
import com.snapdeal.payments.opsRmsModel.exceptions.OpsRoleMgmtException;
import com.snapdeal.payments.opsRmsModel.services.OpsRoleMgmtService;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

@RestController
@Slf4j
public class RoleController extends AbstractController {

   @Autowired
   private OpsRoleMgmtService opsRoleMgmtService;
   
   @RequestAware
   @Marked
   @Logged(printStackTraceEnabled=false)
   @Timed
   @OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
   @RequestMapping(value = RestURIConstants.CREATE_ROLE, method = RequestMethod.POST)
   public ServiceResponse<Void> createRole(@RequestBody CreateRoleRequest request) {

      ServiceResponse<Void> response = new ServiceResponse<Void>();
      opsRoleMgmtService.createRole(request);
      return response;
   }
   
   @RequestAware
   @Marked
   @Logged(printStackTraceEnabled=false)
   @Timed
   @OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
   @RequestMapping(value = RestURIConstants.UPDATE_ROLE, method = RequestMethod.POST)
   public ServiceResponse<Void> updateRole(@RequestBody UpdateRoleRequest request) {

      ServiceResponse<Void> response = new ServiceResponse<Void>();
      opsRoleMgmtService.updateRole(request);
      return response;
   }
   
   @RequestAware
   @Marked
   @Logged(printStackTraceEnabled=false)
   @Timed
   @OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
   @RequestMapping(value = RestURIConstants.GET_ALL_ROLES, method = RequestMethod.GET)
   public ServiceResponse<GetAllRolesResponse> getAllRoles(
            @ModelAttribute GetAllRolesRequest request) {

      ServiceResponse<GetAllRolesResponse> response = new ServiceResponse<GetAllRolesResponse>();
      GetAllRolesResponse allRoles = opsRoleMgmtService.getAllRoles(request);
      response.setResponse(allRoles);
      return response;
   }
   
   @RequestAware
   @Marked
   @Logged(printStackTraceEnabled=false)
   @Timed
   @OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
   @RequestMapping(value = RestURIConstants.GET_ALL_PERMISSIONS, method = RequestMethod.GET)
   public ServiceResponse<GetAllPermissionsResponse> getAllPermissions(
            @ModelAttribute GetAllPermissionsRequest request) {

      ServiceResponse<GetAllPermissionsResponse> response = new ServiceResponse<GetAllPermissionsResponse>();
      GetAllPermissionsResponse allPermissions = opsRoleMgmtService.getAllPermissions(request);
      response.setResponse(allPermissions);
      return response;
   }
   
   @RequestAware
   @Marked
   @Logged(printStackTraceEnabled=false)
   @Timed
   @OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
   @RequestMapping(value = RestURIConstants.GET_ALL_ROLES_BY_ROLENAMES, method = RequestMethod.POST)
   public ServiceResponse<GetRolesByRoleNamesResponse> getRolesByRoleNames(
            @RequestBody GetRolesByRoleNamesRequest request) {
      ServiceResponse<GetRolesByRoleNamesResponse> response = new ServiceResponse<GetRolesByRoleNamesResponse>();
      GetRolesByRoleNamesResponse roles = opsRoleMgmtService.getRolesByRoleNames(request);
      response.setResponse(roles);
      return response;
   }

   @ExceptionHandler(OpsRoleMgmtException.class)
   public <T> ServiceResponse<T> handleException(OpsRoleMgmtException exception) {
      ServiceResponse<T> response = new ServiceResponse<T>();
      OpsRoleMgmtException roleException = new OpsRoleMgmtException(exception.getMessage());
      roleException.setErrorCode(exception.getErrorCode());
      response.setException(roleException);
      return response;
   }
   
   @ExceptionHandler(Exception.class)
   public <T> ServiceResponse<T> handleException(Exception exception) {
	      log.info("Exception {} " ,  exception.getMessage());
	      ServiceResponse<T> response = new ServiceResponse<T>();
	      OpsRoleMgmtException roleException= new OpsRoleMgmtException("Internal Server Error. Please try after some time");
	      roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
	      response.setException(roleException);
	      return response;
	}
   
   @ExceptionHandler(RuntimeException.class)
   public <T> ServiceResponse<T> handleException(RuntimeException exception) {
	      log.info("Runtime Exception {} " , exception.getMessage());
	      ServiceResponse<T> response = new ServiceResponse<T>();
	      OpsRoleMgmtException roleException= new OpsRoleMgmtException("Internal Server Error. Please try after some time");
	      roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
	      response.setException(roleException);
	      return response;
	}


}
