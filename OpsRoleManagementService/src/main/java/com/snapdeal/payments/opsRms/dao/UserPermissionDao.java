package com.snapdeal.payments.opsRms.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.snapdeal.payments.opsRms.dao.model.PermissionUserMapperModel;
import com.snapdeal.payments.opsRmsModel.request.AddPermissionRequest;
import com.snapdeal.payments.opsRmsModel.request.Permission;
import com.snapdeal.payments.opsRmsModel.request.UserPermissionRequest;

/**
 * @author shubham
 *         19-Oct-2015
 */
public interface UserPermissionDao {

   public List<Permission> getAllPermissions();

   public List<Permission> getAllPermissionsByUser(String userName);

   public List<Permission> getAllPermissionsByRoleId(Integer roleId);

   public Map<String, Integer> getAllUserPermissions(String userName);

   public void mapAllPermissionsWithUser(PermissionUserMapperModel userPermissionMapper);

   public void deleteUserPermissionMappingByUser(String userId);
   
   public List<String> getUsersByPermissionCriteria(Set<String> userIds,String columnName,String searchString);

   public List<Permission> getAllPermissionsByUserId(String userId);

   List<String> getAllAppNamesFromPermissions();

   List<Permission> getAllPermissionsBasedOnCriteria(UserPermissionRequest request);

   Integer addNewPermissions(AddPermissionRequest request) throws RuntimeException;

}
