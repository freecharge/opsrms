package com.snapdeal.payments.opsRms.dao;

import java.util.List;
import java.util.Set;

import com.snapdeal.payments.opsRms.dao.model.RolePermissionMapperModel;
import com.snapdeal.payments.opsRms.dao.model.RoleUserMapperModel;
import com.snapdeal.payments.opsRmsModel.request.Role;

/**
 * @author shubham
 *         19-Oct-2015
 */
public interface UserRoleDao {

   public Integer addRole(String role);

   public void addUserRoleMapping(RoleUserMapperModel userRoleMap);

   public void savePermissionRoleMapping(RolePermissionMapperModel rolePermissionMap);

   public List<Role> getAllRoles();

   public List<Role> getAllRolesByUser(String userName);

   public void deleteRole(String role);

   public void deleteUserRoleMappingByUser(String userId);

   public void deletePermissionRoleMappingByRoleId(Integer roleId);
   
   public List<Role> getRolesByRoleNames(List<String> listRoleNames);
   
   public List<String> getUsersByRoleCriteria(Set<String> userIds,String columnName,String searchString);

}
