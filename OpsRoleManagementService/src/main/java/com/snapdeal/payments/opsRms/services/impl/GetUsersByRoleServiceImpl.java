/**
 * 26-Oct-2015
 *shubham
 */
package com.snapdeal.payments.opsRms.services.impl;

import java.util.List;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRmsModel.request.GetUsersByRoleRequest;
import com.snapdeal.payments.opsRmsModel.request.User;
import com.snapdeal.payments.opsRmsModel.response.GetUsersByRoleResponse;
import com.snapdeal.payments.opsRmsModel.services.GetUsersByRoleService;

/**
 * @author shubham
 *         26-Oct-2015
 */
@Service
@Slf4j
public class GetUsersByRoleServiceImpl implements GetUsersByRoleService {
   @Autowired
   private PersistanceManager persistanceManager;

   @Autowired
   private RequestParamValidatorForRole<GetUsersByRoleRequest> requestValidator;

   /*
    * (non-Javadoc)
    * 
    * @see com.snapdeal.payments.roleManagement.services.GetUsersByRoleService#
    * getUserByRole
    * (com.snapdeal.payments.roleManagementModel.request.GetUsersByRoleRequest)
    */

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetUsersByRoleResponse getUsersByRole(GetUsersByRoleRequest userrequest) {
		requestValidator.validate(userrequest);
		return getResponseFromData(persistanceManager.getUsersByRole(userrequest.getRoleName()));
	}

   private GetUsersByRoleResponse getResponseFromData(List<User> data) {
      return new GetUsersByRoleResponse(data);
   }
}
