package com.snapdeal.payments.opsRms.services.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.snapdeal.payments.opsRms.constant.InputUtilCheck;
import com.snapdeal.payments.opsRms.constant.OprmsUtilConstant;
import com.snapdeal.payments.opsRmsModel.commons.RestURIConstants;
import com.snapdeal.payments.opsRmsModel.request.*;
import com.snapdeal.payments.opsRmsModel.response.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.model.ColumnsConstants;
import com.snapdeal.payments.opsRmsModel.commons.OPRMSPreAuthorize;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionErrorCode;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.opsRmsModel.exceptions.InternalServerExceptionOps;
import com.snapdeal.payments.opsRmsModel.exceptions.OpsRoleMgmtException;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.interceptor.RequestHeaders;
import com.snapdeal.payments.opsRmsModel.interceptor.OpsRoleMgmtRequestContext;
import com.snapdeal.payments.opsRmsModel.request.ChangePasswordRequest;
import com.snapdeal.payments.opsRmsModel.request.CreateUserRequest;
import com.snapdeal.payments.opsRmsModel.request.DeleteUserRequest;
import com.snapdeal.payments.opsRmsModel.request.ForgotPasswordNotifyRequest;
import com.snapdeal.payments.opsRmsModel.request.GenerateOTPRequest;
import com.snapdeal.payments.opsRmsModel.request.GetAllRolesRequest;
import com.snapdeal.payments.opsRmsModel.request.GetAllUsersRequest;
import com.snapdeal.payments.opsRmsModel.request.GetUserByIdRequest;
import com.snapdeal.payments.opsRmsModel.request.GetUserByTokenRequest;
import com.snapdeal.payments.opsRmsModel.request.GetUserByUserNameRequest;
import com.snapdeal.payments.opsRmsModel.request.GetUsersByCriteriaRequest;
import com.snapdeal.payments.opsRmsModel.request.GetUsersByIdsRequest;
import com.snapdeal.payments.opsRmsModel.request.GetUsersByRoleRequest;
import com.snapdeal.payments.opsRmsModel.request.LoginUserRequest;
import com.snapdeal.payments.opsRmsModel.request.LogoutUserRequest;
import com.snapdeal.payments.opsRmsModel.request.ResendOTPRequest;
import com.snapdeal.payments.opsRmsModel.request.SendEmailAndSmsOnMerchantCreationRequest;
import com.snapdeal.payments.opsRmsModel.request.SetPasswordToDummyPasswordRequest;
import com.snapdeal.payments.opsRmsModel.request.SocialLoginUserRequest;
import com.snapdeal.payments.opsRmsModel.request.UpdateUserRequest;
import com.snapdeal.payments.opsRmsModel.request.VerifyCodeRequest;
import com.snapdeal.payments.opsRmsModel.request.VerifyOtpForMobileVerificationRequest;
import com.snapdeal.payments.opsRmsModel.services.MerchantRoleMgmtService;
import com.snapdeal.payments.opsRmsModel.services.OpsRoleMgmtService;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

@RestController
@Slf4j
public class UserController extends AbstractController {

	@Autowired
	private OpsRoleMgmtService opsRoleMgmtService;

	@Autowired
	private MerchantRoleMgmtService merchantRoleMgmtService;
	
	@Autowired
	private HttpServletRequest servletRequest;

	private static final Logger auditLog = LoggerFactory.getLogger(ColumnsConstants.AUDIT_LOG);

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)

	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.CREATE_USER, method = RequestMethod.POST)
	public ServiceResponse<CreateUserResponse> createUser(@RequestBody @Valid CreateUserRequest request,
			BindingResult results, HttpServletRequest httpRequest) {
		if ((results.hasErrors() && null != results.getAllErrors())) {
			throw new ValidationExceptionOps(ExceptionMessages.PARAM_VALIDATION_EXCEPTION);
		}
		if (StringUtils.isBlank(request.getAppName())) {
			String app_Name = OpsRoleMgmtRequestContext.get(RequestHeaders.APP_NAME).orNull();
			request.setAppName(app_Name);
		}
		if (StringUtils.isBlank(request.getRequestToken())) {
			String token_value = OpsRoleMgmtRequestContext.get(RequestHeaders.TOKEN).orNull();
			request.setRequestToken(token_value);
		}
		ServiceResponse<CreateUserResponse> response = new ServiceResponse<CreateUserResponse>();
		CreateUserResponse createUserResponse = opsRoleMgmtService.createUser(request);
		response.setResponse(createUserResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.CREATE_USER_WITH_LINK, method = RequestMethod.POST)
	public ServiceResponse<CreateUserResponse> createUserWithLink(@RequestBody @Valid CreateUserRequest request,
			BindingResult results, HttpServletRequest httpRequest) {
		if (((results.hasErrors() && null != results.getAllErrors()))) {
			throw new ValidationExceptionOps(ExceptionMessages.PARAM_VALIDATION_EXCEPTION);
		}
		if (StringUtils.isBlank(request.getAppName())) {
			String app_Name = OpsRoleMgmtRequestContext.get(RequestHeaders.APP_NAME).orNull();
			request.setAppName(app_Name);
		}
		if (StringUtils.isBlank(request.getRequestToken())) {
			String token_value = OpsRoleMgmtRequestContext.get(RequestHeaders.TOKEN).orNull();
			request.setRequestToken(token_value);
		}
		if (request.isSendEmail()) {
			if (StringUtils.isEmpty(request.getLinkForSettingPassword()) || null == request.getEmailTemplate()
					|| StringUtils.isEmpty(request.getEmailTemplate().getKey())) {
				throw new ValidationExceptionOps(ExceptionMessages.PARAM_VALIDATION_EXCEPTION);
			}
		}
		ServiceResponse<CreateUserResponse> response = new ServiceResponse<CreateUserResponse>();
		auditLog.info("user_create request made with jira id: {}", request.getJiraId());
		CreateUserResponse createUserResponse = opsRoleMgmtService.createUser(request);
		response.setResponse(createUserResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.UPDATE_USER, method = RequestMethod.POST)
	public ServiceResponse<Void> updateUser(@RequestBody UpdateUserRequest request) {
		if (StringUtils.isBlank(request.getAppName())) {
			String app_Name = OpsRoleMgmtRequestContext.get(RequestHeaders.APP_NAME).orNull();
			request.setAppName(app_Name);
		}
		if (StringUtils.isBlank(request.getRequestToken())) {
			String token_value = OpsRoleMgmtRequestContext.get(RequestHeaders.TOKEN).orNull();
			request.setRequestToken(token_value);
		}

		ServiceResponse<Void> response = new ServiceResponse<Void>();
		auditLog.info("user_update request made with jira id: {}", request.getJiraId());
		opsRoleMgmtService.updateUser(request);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.GET_USER_BY_ID, method = RequestMethod.GET)
	public ServiceResponse<GetUserByIdResponse> getUserById(@ModelAttribute GetUserByIdRequest request) {

		ServiceResponse<GetUserByIdResponse> response = new ServiceResponse<GetUserByIdResponse>();
		GetUserByIdResponse getUserByIdResponse = opsRoleMgmtService.getUserById(request);
		response.setResponse(getUserByIdResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.GET_USER_BY_USER_NAME, method = RequestMethod.GET)
	public ServiceResponse<GetUserByUserNameResponse> getUserByUserName(
			@ModelAttribute GetUserByUserNameRequest request) {

		ServiceResponse<GetUserByUserNameResponse> response = new ServiceResponse<GetUserByUserNameResponse>();
		GetUserByUserNameResponse getUserByUserNameResponse = opsRoleMgmtService.getUserByUserName(request);
		response.setResponse(getUserByUserNameResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.GET_USERS_BY_ROLE, method = RequestMethod.GET)
	public ServiceResponse<GetUsersByRoleResponse> getUsersByRole(@ModelAttribute GetUsersByRoleRequest request) {

		ServiceResponse<GetUsersByRoleResponse> response = new ServiceResponse<GetUsersByRoleResponse>();
		GetUsersByRoleResponse getUsersByRoleResponse = opsRoleMgmtService.getUsersByRole(request);
		response.setResponse(getUsersByRoleResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.CHANGE_PSWD, method = RequestMethod.POST)
	public ServiceResponse<Void> changePassword(@RequestBody ChangePasswordRequest request) {

		ServiceResponse<Void> response = new ServiceResponse<Void>();
		opsRoleMgmtService.changePassword(request);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.FORGOT_PSWD, method = RequestMethod.POST)
	public ServiceResponse<Void> forgotPasswordNotify(@RequestBody ForgotPasswordNotifyRequest request) {

		throw new InternalServerExceptionOps(OprmsUtilConstant.FORGOT_PSWD_INVALID);
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.LOGIN_USER, method = RequestMethod.POST)
	public ServiceResponse<LoginUserResponse> loginUser(@RequestBody LoginUserRequest request) {
		auditLog.info("user_login request made by user: {} from ip: {}", request.getUserName(), servletRequest.getRemoteAddr());
		ServiceResponse<LoginUserResponse> response = new ServiceResponse<LoginUserResponse>();
		LoginUserResponse loginResponse = opsRoleMgmtService.loginUser(request);
		GetAllRolesRequest getRolesRequest = new GetAllRolesRequest();
		GetAllRolesResponse getRolesResponse;
		getRolesResponse = opsRoleMgmtService.getAllRoles(getRolesRequest);
		loginResponse.setAllRoles(getRolesResponse.getRoles());
		response.setResponse(loginResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.LOGOUT_USER, method = RequestMethod.POST)
	public ServiceResponse<Void> logoutUser(@RequestBody @Valid LogoutUserRequest request, BindingResult results,
			HttpServletRequest httpRequest) {
		if (((results.hasErrors() && null != results.getAllErrors()))) {
			throw new ValidationExceptionOps(ExceptionMessages.PARAM_VALIDATION_EXCEPTION);
		}
		ServiceResponse<Void> response = new ServiceResponse<Void>();
		opsRoleMgmtService.logoutUser(request);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.SOCIAL_LOGIN_USER, method = RequestMethod.POST)
	public ServiceResponse<LoginUserResponse> socialLoginUser(@RequestBody SocialLoginUserRequest request) {

		ServiceResponse<LoginUserResponse> response = new ServiceResponse<LoginUserResponse>();
		response.setResponse(opsRoleMgmtService.socialLoginUser(request));
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.DELETE_USER, method = RequestMethod.POST)
	public ServiceResponse<Void> deleteUser(@RequestBody DeleteUserRequest request) {

		ServiceResponse<Void> response = new ServiceResponse<Void>();
		opsRoleMgmtService.deleteUser(request);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.VALIDATE_CODE, method = RequestMethod.POST)
	public ServiceResponse<Void> verifyCodeAndSetPassword(@RequestBody @Valid VerifyCodeRequest request,
			BindingResult results, HttpServletRequest httpRequest) {
		if ((results.hasErrors() && null != results.getAllErrors())) {
			throw new ValidationExceptionOps(ExceptionMessages.PARAM_VALIDATION_EXCEPTION);
		}
		ServiceResponse<Void> response = new ServiceResponse<Void>();
		opsRoleMgmtService.verifyCodeAndSetPassword(request);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.GET_ALL_USERS_BY_IDS, method = RequestMethod.POST)
	public ServiceResponse<GetUsersByIdsResponse> getUsersByIds(@RequestBody GetUsersByIdsRequest request) {

		ServiceResponse<GetUsersByIdsResponse> response = new ServiceResponse<GetUsersByIdsResponse>();
		GetUsersByIdsResponse getUsersByIdsResponse = opsRoleMgmtService.getUsersByIds(request);
		response.setResponse(getUsersByIdsResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.GET_ALL_USER, method = RequestMethod.GET)
	public ServiceResponse<GetAllUsersResponse> getAllUsers(@ModelAttribute GetAllUsersRequest request) {
		ServiceResponse<GetAllUsersResponse> response = new ServiceResponse<GetAllUsersResponse>();
		GetAllUsersResponse allUsers = opsRoleMgmtService.getAllUsers(request);
		response.setResponse(allUsers);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.GET_USER_DETAILS_FROM_TOKEN, method = RequestMethod.POST)
	public ServiceResponse<GetUserByTokenResponse> getUserByToken(@RequestBody GetUserByTokenRequest request) {
		ServiceResponse<GetUserByTokenResponse> response = new ServiceResponse<GetUserByTokenResponse>();
		GetUserByTokenResponse getUserByTokenResponse = opsRoleMgmtService.getUserByToken(request);
		response.setResponse(getUserByTokenResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.GET_USER_BY_CRITERIA, produces = OprmsUtilConstant.APPLICATION_JSON, method = RequestMethod.POST)
	public ServiceResponse<GetUsersByCriteriaResponse> getMerchantsByCriteria(
			@RequestBody GetUsersByCriteriaRequest getSelectedUsersRequest) {
		ServiceResponse<GetUsersByCriteriaResponse> response = new ServiceResponse<GetUsersByCriteriaResponse>();
		GetUsersByCriteriaResponse getSelectedUsersResponse = opsRoleMgmtService
				.getUsersByCriteria(getSelectedUsersRequest);
		response.setResponse(getSelectedUsersResponse);
		return response;
	}

	@ExceptionHandler(OpsRoleMgmtException.class)
	public <T> ServiceResponse<T> handleException(OpsRoleMgmtException exception) {
		ServiceResponse<T> response = new ServiceResponse<T>();
		OpsRoleMgmtException roleException = new OpsRoleMgmtException(exception.getMessage());
		roleException.setErrorCode(exception.getErrorCode());
		response.setException(roleException);
		return response;
	}

	@ExceptionHandler(Exception.class)
	public <T> ServiceResponse<T> handleException(Exception exception) {
		log.info("Exception {} " , exception.getMessage());
		ServiceResponse<T> response = new ServiceResponse<T>();
		OpsRoleMgmtException roleException = new OpsRoleMgmtException("Internal Server Error. Please try after some time");
		roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
		response.setException(roleException);
		return response;
	}

	@ExceptionHandler(RuntimeException.class)
	public <T> ServiceResponse<T> handleException(RuntimeException exception) {
		log.info("Runtime Exception {} " , exception.getMessage());
		ServiceResponse<T> response = new ServiceResponse<T>();
		OpsRoleMgmtException roleException = new OpsRoleMgmtException("Internal Server Error. Please try after some time");
		roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
		response.setException(roleException);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.SEND_EMAIL_AND_SMS_ON_MERCHANT_CREATION, method = RequestMethod.POST)
	public ServiceResponse<SendEmailAndSmsOnMerchantCreationResponse> sendEmailAndSmsOnMerchantCreation(
			@RequestBody SendEmailAndSmsOnMerchantCreationRequest request) {
		ServiceResponse<SendEmailAndSmsOnMerchantCreationResponse> response = new ServiceResponse<SendEmailAndSmsOnMerchantCreationResponse>();
		SendEmailAndSmsOnMerchantCreationResponse sendEmailAndSmsOnMerchantCreationResponse = merchantRoleMgmtService
				.sendEmailAndSmsOnMerchantCreation(request);
		response.setResponse(sendEmailAndSmsOnMerchantCreationResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.SEND_OTP_FOR_MOBILE_VERIFICATION, method = RequestMethod.POST)
	public ServiceResponse<GenerateOTPResponse> sendOtpForMobileVerification(@RequestBody GenerateOTPRequest request) {
		ServiceResponse<GenerateOTPResponse> response = new ServiceResponse<GenerateOTPResponse>();
		GenerateOTPResponse generateOTPResponse = merchantRoleMgmtService.sendOtpForMobileVerification(request);
		response.setResponse(generateOTPResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.SEND_DUMMY_OTP_FOR_MOBILE_VERIFICATION, method = RequestMethod.POST)
	public ServiceResponse<GenerateOTPResponse> sendDummyOtpForMobileVerification(
			@RequestBody GenerateOTPRequest request) {
		ServiceResponse<GenerateOTPResponse> response = new ServiceResponse<GenerateOTPResponse>();
		GenerateOTPResponse generateOTPResponse = merchantRoleMgmtService.sendDummyOtpForMobileVerification(request);
		response.setResponse(generateOTPResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.RESEND_OTP_FOR_MOBILE_VERIFICATION, method = RequestMethod.POST)
	public ServiceResponse<GenerateOTPResponse> resendOtpForMobileVerification(@RequestBody ResendOTPRequest request) {
		ServiceResponse<GenerateOTPResponse> response = new ServiceResponse<GenerateOTPResponse>();
		GenerateOTPResponse generateOTPResponse = merchantRoleMgmtService.resendOtpForMobileVerification(request);
		response.setResponse(generateOTPResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.VERIFY_OTP_FOR_MOBILE_VERIFICATION, method = RequestMethod.POST)
	public ServiceResponse<VerifyOtpForMobileVerificationResponse> verifyOtpForMobileVerification(
			@RequestBody VerifyOtpForMobileVerificationRequest request) {
		ServiceResponse<VerifyOtpForMobileVerificationResponse> response = new ServiceResponse<VerifyOtpForMobileVerificationResponse>();
		VerifyOtpForMobileVerificationResponse verifyOtpForMobileVerificationResponse = merchantRoleMgmtService
				.verifyOtpForMobileVerification(request);
		response.setResponse(verifyOtpForMobileVerificationResponse);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.RESET_PSWD_TO_DUMMY_PSWD, method = RequestMethod.POST)
	public ServiceResponse<Boolean> resetPasswordToDummyPasswordForStaging(
			@RequestBody SetPasswordToDummyPasswordRequest request) {
		ServiceResponse<Boolean> response = new ServiceResponse<Boolean>();
		Boolean responseForResetPassword = merchantRoleMgmtService.resetPasswordToDummyPasswordForStaging(request);
		response.setResponse(responseForResetPassword);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@RequestMapping(value = OprmsUtilConstant.GET_USER_DETAILS_FROM_TOKEN_FOR_MERCHANT, method = RequestMethod.POST)
	public ServiceResponse<GetUserByTokenResponse> getUserByTokenForMerchant(
			@RequestBody GetUserByTokenRequest request) {
		ServiceResponse<GetUserByTokenResponse> response = new ServiceResponse<GetUserByTokenResponse>();
		GetUserByTokenResponse getUserByTokenResponse = opsRoleMgmtService.getUserByTokenForMerchant(request);
		response.setResponse(getUserByTokenResponse);
		return response;
	}
	
	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.DELETE_AUDIT_USER, method = RequestMethod.POST)
	public ServiceResponse<Void> deleteAuditUser(@RequestBody DeleteUserRequest request) {
		if (StringUtils.isBlank(request.getAppName())) {
			String app_Name = OpsRoleMgmtRequestContext.get(RequestHeaders.APP_NAME).orNull();
			request.setAppName(app_Name);
		}
		if (StringUtils.isBlank(request.getRequestToken())) {
			String token_value = OpsRoleMgmtRequestContext.get(RequestHeaders.TOKEN).orNull();
			request.setRequestToken(token_value);
		}
		ServiceResponse<Void> response = new ServiceResponse<Void>();
		auditLog.info("user_delete request made with jira id: {}", request.getJiraId());
		opsRoleMgmtService.deleteAuditUser(request);
		return response;
	}

	@RequestAware
	@Marked
	@Logged(printStackTraceEnabled = false)
	@Timed
	@OPRMSPreAuthorize("(hasPermission('MANAGE_USER'))")
	@RequestMapping(value = OprmsUtilConstant.DOWNLOAD_USERS, method = RequestMethod.GET)
	public ServiceResponse<DownloadUsersResponse> downloadAllUsers(@ModelAttribute GetAllUsersRequest request) {
		ServiceResponse<DownloadUsersResponse> response = new ServiceResponse<DownloadUsersResponse>();
		DownloadUsersResponse usersResponse = opsRoleMgmtService.downloadAllUsers(request);
		response.setResponse(usersResponse);
		return response;
	}

	@Logged(printStackTraceEnabled=true)
	@Timed
	@RequestMapping(value = RestURIConstants.GET_ALL_APP_NAMES, method = RequestMethod.POST)
	public ServiceResponse<GetAllAppResponse> getAllApps(@RequestBody GetAllPermissionsRequest request) {
		ServiceResponse<GetAllAppResponse> response = new ServiceResponse<GetAllAppResponse>();
		GetAllAppResponse getAllAppResponse = opsRoleMgmtService.getAllAppNames(request);

		response.setResponse(getAllAppResponse);
		return response;
	}

	@Logged(printStackTraceEnabled=true)
	@Timed
	@RequestMapping(value = RestURIConstants.GET_ALL_PERMISSIONS_BY_CRITERIA, method = RequestMethod.POST)
	public ServiceResponse<GetAllPermissionsResponse> getAllPermissions(@RequestBody UserPermissionRequest request) {
		ServiceResponse<GetAllPermissionsResponse> response = new ServiceResponse<GetAllPermissionsResponse>();
		GetAllPermissionsResponse allPermissionResponse = opsRoleMgmtService.getAllPermissionsBasedOnCriteria(request);
		response.setResponse(allPermissionResponse);
		return response;
	}

	@Logged(printStackTraceEnabled=true)
	@Timed
	@RequestMapping(value = RestURIConstants.ADD_NEW_PERMISSION, method = RequestMethod.POST)
	public ServiceResponse<Void> addPermission(@RequestBody AddPermissionRequest request) throws RuntimeException{

		ServiceResponse<Void> response = new ServiceResponse<Void>();
		if (request == null || StringUtils.isEmpty(request.getPermissionName())
				|| StringUtils.isEmpty(request.getAppName()) || StringUtils.isEmpty(request.getDisplayName())){

			ValidationExceptionOps exception = new ValidationExceptionOps("request is NULL or PermissionName/AppName/DisplayName is blank!!");
			response.setException(exception);
			response.setServerTimeStamp(new Date());
			return response;
		}
		boolean isValidPermission = InputUtilCheck.validateInput(request.getPermissionName());
		boolean isValidAppName = InputUtilCheck.validateInput(request.getAppName());
		log.info("isValidPermission: "+ isValidPermission + " isValidAppName: "+ isValidAppName);
		if (!isValidPermission || !isValidAppName ){
			ValidationExceptionOps exception = new ValidationExceptionOps("Only CAPITAL & UnderScore(_) is allowed for PermissionName & AppName!!");
			response.setException(exception);
			response.setServerTimeStamp(new Date());
			return response;
		}
		log.info("Incoming request: {}",request);
		try {
			opsRoleMgmtService.addNewPermission(request);
		}catch (RuntimeException rx){
			throw rx;
		}
		return response;
	}




}