package com.snapdeal.payments.opsRms.dao.model;

public interface ColumnsConstants {

   String USER_ID = "id";
   String NAME = "name";
   String USER_NAME = "user_name";
   String EMAILID = "email";
   String MOBILE_NUMBER = "mobile_no";
   String CREATED_ON = "created_on";
   String UPDATED_ON = "updated_on";
   String PERMISSION_NAME = "display_name";
   String ROLE_NAME = "name";
   String ACTIVE = "active";
   String AUDIT_LOG = "auditLog";
}


