package com.snapdeal.payments.opsRms.services.impl;

import com.snapdeal.payments.opsRms.dao.model.ColumnsConstants;
import com.snapdeal.payments.opsRmsModel.exceptions.OpsRoleMgmtException;
import com.snapdeal.payments.opsRmsModel.request.*;
import com.snapdeal.payments.opsRmsModel.response.GetUserByTokenResponse;
import com.snapdeal.payments.opsRmsModel.services.GetUserByTokenService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.opsRmsModel.exceptions.NoSuchUserExceptionOps;
import com.snapdeal.payments.opsRmsModel.services.DeleteUserService;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author aniket 
 * 8-Dec-2015
 */
@Service
@Slf4j
public class DeleteUserServiceImpl implements DeleteUserService {
	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private RequestParamValidatorForRole<DeleteUserRequest> requestValidator;

	@Autowired
	@Qualifier("getUserByToken")
	private GetUserByTokenService getUserByTokenService;

	private static final Logger auditLog = LoggerFactory.getLogger(ColumnsConstants.AUDIT_LOG);

	@Override
	@Transactional
	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public void deleteUser(DeleteUserRequest user) {
		requestValidator.validate(user);
		if (!persistanceManager.isUserExist(user.getUserId()))
			throw new NoSuchUserExceptionOps(ExceptionMessages.NO_SUCH_USER_EXCEPTION);
		persistanceManager.deleteUser(user.getUserId());
	}

	@Override
	@Transactional
	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public void deleteAuditUser(DeleteUserRequest user) {
		requestValidator.validate(user);
		if (!persistanceManager.isUserExist(user.getUserId()))
			throw new NoSuchUserExceptionOps(ExceptionMessages.NO_SUCH_USER_EXCEPTION);
		try {
			String token = user.getRequestToken();
			String userIdCallingtheAPI = null;
			String userNameCallingtheAPI = null;
			if(!StringUtils.isBlank(token)) {
				GetUserByTokenRequest req = new GetUserByTokenRequest();
				req.setRequestToken(token);
				GetUserByTokenResponse res = getUserByTokenService.getUserByToken(req);
				User user_calling_the_API = res.getUser();
				userIdCallingtheAPI = user_calling_the_API.getId();
				userNameCallingtheAPI = user_calling_the_API.getUserName();
			} else{
				log.error("token is NULL");
				throw new Exception("Please use a valid Token.");
			}
			GetUserByIdRequest getUserByIdRequest = new GetUserByIdRequest();
			getUserByIdRequest.setUserId(user.getUserId());
			User userDeleted = persistanceManager.getUserById(user.getUserId());
			List<Permission> permissions = persistanceManager.getAllPermissionsByUserId(user.getUserId());
			List<String> permission_names = new ArrayList<>();
			if(permissions!=null){
				for (Permission permission:permissions)
					permission_names.add(permission.getName());
			}
			auditLog.info("user_delete request in process under JIRA number: {} for user: {} with userId: {} and permissions: {} raised by user: {} with userId: {}", user.getJiraId(), userDeleted.getEmail(), user.getUserId(), permission_names, userNameCallingtheAPI, userIdCallingtheAPI);
			log.info("user_delete request in process for user: {} with userId: {} and permissions: {} raised by user: {} with userId: {}", userDeleted.getEmail(), user.getUserId(), permission_names, userNameCallingtheAPI, userIdCallingtheAPI);
			persistanceManager.deleteAuditUser(user.getUserId());
			auditLog.info("user_delete success");
			log.info("user_delete success");
		}
		catch (Exception e) {
			auditLog.info("user_delete failure");
			log.error("Error in deleting user data on user deletion! ",e);
			throw new OpsRoleMgmtException("Error in user deletion.", e);
		}
	}
}
