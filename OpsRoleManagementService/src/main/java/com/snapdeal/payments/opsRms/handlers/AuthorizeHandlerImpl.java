package com.snapdeal.payments.opsRms.handlers;

import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRmsModel.dto.TokenDTO;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.opsRmsModel.exceptions.InternalServerExceptionOps;
import com.snapdeal.payments.opsRmsModel.exceptions.UnAuthrizedExceptionOps;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.interceptor.OpsRoleMgmtRequestContext;
import com.snapdeal.payments.opsRmsModel.interceptor.RequestHeaders;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.text.ParseException;

/**
 * @author shubham
 * 17-Oct-2015
 * <p>
 * Handle Authorization for apis
 */
@Component
public class AuthorizeHandlerImpl {

    public static final ExpressionParser parser = new SpelExpressionParser();

    private StandardEvaluationContext context;

    @Autowired
    private PermissionHandler permissionHandler;

    @Autowired
    private PersistanceManager persistanceManager;

    public AuthorizeHandlerImpl() throws InternalServerExceptionOps {
        context = new StandardEvaluationContext(new PermissionHandler());
        Method method = null;
        try {
            method = PermissionHandler.class.getMethod("hasPermission", String.class);
        } catch (NoSuchMethodException | SecurityException e) {
            throw new InternalServerExceptionOps(
                    ExceptionMessages.PERMISSION_INITIALISATION_EXCEPTION, e);
        }
        context.registerFunction("hasPermission", method);
    }

    public void handleAuthrization(String permissionString) throws UnAuthrizedExceptionOps,
            ParseException {
      boolean isAuthrized = false;
      String token = OpsRoleMgmtRequestContext.get(RequestHeaders.TOKEN).orNull();
      String tokenId = TokenUtils.getTokenIdFromToken(token);
      TokenDTO tokenObj = persistanceManager.getUserNameFromId(tokenId);
      if (null == tokenObj || !tokenObj.getActive()
				|| tokenObj.getLoginTime().before(new DateTime().minusHours(TokenUtils.getDEFAULT_TIMEOUT()).toDate()) )
         throw new ValidationExceptionOps(ExceptionMessages.SESSION_EXPIRED);
      String permittedExpression = permissionString;
      if (null != permittedExpression && !permittedExpression.isEmpty()) {
         try {
            permissionHandler.populateUserPermissions(tokenObj.getUserName());
            isAuthrized = parse(permittedExpression);
            if (!isAuthrized)
               throw new UnAuthrizedExceptionOps(ExceptionMessages.UNAUTHORIZED_USER_EXCEPTION);
         } finally {
            PermissionHandler.resetUserPermissions();
         }
      }

    }

    public Boolean parse(String permittedExpression) {
        Boolean isAuthrized = null;
        try {
            isAuthrized = parser.parseExpression(permittedExpression).getValue(context, Boolean.class); //NOSONAR
        } catch (Throwable th) {
            throw new ValidationExceptionOps(ExceptionMessages.SYSTEM_ERROR_EXCEPTION,th);
        }
        return isAuthrized;
    }
}
