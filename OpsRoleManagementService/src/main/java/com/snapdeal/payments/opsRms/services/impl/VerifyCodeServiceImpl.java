package com.snapdeal.payments.opsRms.services.impl;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.CipherServiceUtil;
import com.snapdeal.payments.opsRms.handlers.IDFactory;
import com.snapdeal.payments.opsRms.handlers.OTPUtils;
import com.snapdeal.payments.opsRmsModel.exceptions.CipherExceptionOps;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.opsRmsModel.exceptions.InvalidCodeExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.VerifyCodeRequest;
import com.snapdeal.payments.opsRmsModel.services.VerifyCodeService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aniket 21-Jan-2016
 */
@Service
@Slf4j
public class VerifyCodeServiceImpl implements VerifyCodeService {

	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private IDFactory idfactory;

	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	@Override
	public void verifyCodeAndSetPassword(VerifyCodeRequest verifyCodeRequest) {
		String verificationCode = "";
		try {
			verificationCode = CipherServiceUtil.decrypt(verifyCodeRequest.getVerificationCode());
		} catch (CipherExceptionOps e) {
			throw new InvalidCodeExceptionOps(ExceptionMessages.INCORRECT_CODE_ERROR,e);
		}
		String userId = persistanceManager.getUserFromCode(verificationCode);
		if (StringUtils.isEmpty(userId)) {
			throw new InvalidCodeExceptionOps(ExceptionMessages.INCORRECT_CODE_ERROR);
		}
		String hashedPassword = IDFactory.hashString(verifyCodeRequest.getPassword());
		persistanceManager.changePasswordUsingId(userId, hashedPassword,new DateTime().plusDays(idfactory.getPasswordExpiryInDays()).toDate());
	}

}
