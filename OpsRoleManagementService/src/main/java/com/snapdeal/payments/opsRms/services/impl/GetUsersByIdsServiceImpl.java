package com.snapdeal.payments.opsRms.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRmsModel.request.GetUsersByIdsRequest;
import com.snapdeal.payments.opsRmsModel.request.User;
import com.snapdeal.payments.opsRmsModel.response.GetUsersByIdsResponse;
import com.snapdeal.payments.opsRmsModel.services.GetUsersByIdsService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aniket 
 * 8-Dec-2015
 */
@Service
@Slf4j
public class GetUsersByIdsServiceImpl implements GetUsersByIdsService {
	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private RequestParamValidatorForRole<GetUsersByIdsRequest> requestValidator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.snapdeal.payments.roleManagement.services.GetUsersByIdsService#
	 * getUsersByIds
	 * (com.snapdeal.payments.roleManagementModel.request.GetUsersByIdsRequest)
	 */

	@Override
	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetUsersByIdsResponse getUsersByIds(GetUsersByIdsRequest getUsersByIds) {
		requestValidator.validate(getUsersByIds);
		return getResponseFromData(persistanceManager.getUsersByIds(getUsersByIds.getListUserIds()));
	}

	private GetUsersByIdsResponse getResponseFromData(List<User> data) {
		return new GetUsersByIdsResponse(data);
	}

}
