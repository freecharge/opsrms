package com.snapdeal.payments.opsRms.services.impl;

import java.util.List;

import com.snapdeal.payments.opsRms.constant.OprmsUtilConstant;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.IDFactory;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.handlers.TokenUtils;
import com.snapdeal.payments.opsRmsModel.dto.TokenDTO;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.opsRmsModel.exceptions.UnAuthrizedExceptionOps;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.ChangePasswordRequest;
import com.snapdeal.payments.opsRmsModel.services.ChangePasswordService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aniket 
 * 21-Nov-2015
 */

@Service
@Slf4j
public class ChangePasswordServiceImpl implements ChangePasswordService {
	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private RequestParamValidatorForRole<ChangePasswordRequest> requestValidator;

	@Autowired
	private IDFactory idFactory;

	@Override
	@Transactional
	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public void changePassword(ChangePasswordRequest changePasswordRequest) {
		requestValidator.validate(changePasswordRequest);

		String t = changePasswordRequest.getToken();
		String tokenId = TokenUtils.getTokenIdFromToken(t);
		TokenDTO tokenObj = persistanceManager.getUserNameFromId(tokenId);
		if (null == tokenObj || !tokenObj.getActive()
				|| tokenObj.getLoginTime().before(new DateTime().minusHours(TokenUtils.getDEFAULT_TIMEOUT()).toDate()))
			throw new ValidationExceptionOps(ExceptionMessages.SESSION_EXPIRED);
		String userName = tokenObj.getUserName();
		String newHashedPassword = IDFactory.hashString(changePasswordRequest.getNewPassword());
		String oldHashedPassword = IDFactory.hashString(changePasswordRequest.getOldPassword());
		if (null != persistanceManager.getPasswordExpiryDate(userName, oldHashedPassword)) {
			persistanceManager.resetPassword(userName, newHashedPassword,
					new DateTime().plusDays(idFactory.getPasswordExpiryInDays()).toDate());
			List<TokenDTO> tokens=persistanceManager.getTokensFromUsername(userName);
			for( TokenDTO token:tokens){			    
			    persistanceManager.invalidateToken(token.getTokenId());
			}
			
		} else {
			throw new UnAuthrizedExceptionOps(OprmsUtilConstant.PSWD_MISMATCH_EXCEPTION);
		}
	}
}