package com.snapdeal.payments.opsRms.dao;

/**
 * @author shubham
 *         19-Oct-2015
 */
public interface PersistanceManager extends UserDao, UserPermissionDao, UserRoleDao,OTPDao, TokenDao {
}
