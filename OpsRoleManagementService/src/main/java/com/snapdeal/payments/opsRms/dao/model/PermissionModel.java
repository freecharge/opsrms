package com.snapdeal.payments.opsRms.dao.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PermissionModel {
    private String permissionName;
    private String appName;
    private String displayName;
}
