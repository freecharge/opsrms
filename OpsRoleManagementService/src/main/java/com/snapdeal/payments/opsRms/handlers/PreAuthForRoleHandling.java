package com.snapdeal.payments.opsRms.handlers;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.opsRmsModel.commons.OPRMSPreAuthorize;

@Aspect
@Component
public class PreAuthForRoleHandling {

   @Autowired
   private AuthorizeHandlerImpl authhandler;

   @Before("within(com.snapdeal..*) && @annotation(authorize)")
   public void handleAuthrization(OPRMSPreAuthorize authorize) throws Exception {

      authhandler.handleAuthrization(authorize.value());
   }
}
