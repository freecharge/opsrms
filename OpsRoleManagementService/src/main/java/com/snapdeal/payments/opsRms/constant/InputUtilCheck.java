package com.snapdeal.payments.opsRms.constant;

public class InputUtilCheck {
    /**
     * validation for  APPNAME
     * checks for CAPITAL followed by Underscore(_)
     * eg: MOB_AdMIN
     * @param str
     * @return
     */
    public static boolean validateInput(String str) {
        String regex = "^[A-Z]+(?:_[A-Z]+)*$";
        return ( str.matches(regex));
    }
}
