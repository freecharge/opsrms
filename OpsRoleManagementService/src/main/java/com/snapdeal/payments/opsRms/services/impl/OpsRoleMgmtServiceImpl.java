package com.snapdeal.payments.opsRms.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.snapdeal.payments.opsRmsModel.services.ChangePasswordService;
import com.snapdeal.payments.opsRmsModel.services.CreateRoleService;
import com.snapdeal.payments.opsRmsModel.services.CreateUserService;
import com.snapdeal.payments.opsRmsModel.services.DeleteUserService;
import com.snapdeal.payments.opsRmsModel.services.ForgotPasswordNotifyService;
import com.snapdeal.payments.opsRmsModel.services.GenerateOTPService;
import com.snapdeal.payments.opsRmsModel.services.GetRolesByRoleNamesService;
import com.snapdeal.payments.opsRmsModel.services.GetUserByIdService;
import com.snapdeal.payments.opsRmsModel.services.GetUserByTokenService;
import com.snapdeal.payments.opsRmsModel.services.GetUserByUserNameService;
import com.snapdeal.payments.opsRmsModel.services.GetUsersByCriteriaService;
import com.snapdeal.payments.opsRmsModel.services.GetUsersByIdsService;
import com.snapdeal.payments.opsRmsModel.services.GetUsersByRoleService;
import com.snapdeal.payments.opsRmsModel.services.LoginUserService;
import com.snapdeal.payments.opsRmsModel.services.LogoutUserService;
import com.snapdeal.payments.opsRmsModel.services.ResendOTPService;
import com.snapdeal.payments.opsRmsModel.services.OpsRoleMgmtService;
import com.snapdeal.payments.opsRmsModel.services.SocialLoginUserService;
import com.snapdeal.payments.opsRmsModel.services.UiDropDownsService;
import com.snapdeal.payments.opsRmsModel.services.UpdateRoleService;
import com.snapdeal.payments.opsRmsModel.services.UpdateUserService;
import com.snapdeal.payments.opsRmsModel.services.VerifyCodeService;
import com.snapdeal.payments.opsRmsModel.services.VerifyOTPService;

import lombok.experimental.Delegate;

@Component
public class OpsRoleMgmtServiceImpl implements OpsRoleMgmtService {
   @Autowired
   @Delegate
   private CreateRoleService createRoleService;

   @Autowired
   @Delegate
   private UpdateRoleService updateRoleService;

   @Autowired
   @Delegate
   private CreateUserService createUserService;

   @Autowired
   @Delegate
   private UpdateUserService updateUserService;

   @Autowired
   @Delegate
   private GetUserByIdService getUserByIdService;

   @Autowired
   @Delegate
   private GetUserByUserNameService getUserByUserNameService;

   @Autowired
   @Delegate
   private GetUsersByRoleService getUsersByRoleService;

   @Autowired
   @Delegate
   private UiDropDownsService uiDropDownsService;

   @Autowired
   @Delegate
   private ForgotPasswordNotifyService forgotPasswordNotifyService;

   @Autowired
   @Delegate
   private ChangePasswordService changePasswordService;

   @Autowired
   @Delegate
   private LoginUserService loginUserService;

   @Autowired
   @Delegate
   private LogoutUserService logoutUserService;
   
   @Autowired
   @Delegate
   private SocialLoginUserService socialLoginUserService;

   @Autowired
   @Delegate
   private DeleteUserService deleteUserService;
   
   @Autowired
   @Delegate
   private GetUsersByIdsService getUsersByIdsService;
   
   @Autowired
   @Delegate
   private GetRolesByRoleNamesService getRolesByRoleNamesService;
   
   @Autowired
   @Delegate
   private GetUserByTokenService getUserByTokenService;

   @Autowired
   @Delegate
   private GenerateOTPService generateOTPService;
   
   @Autowired
   @Delegate
   private ResendOTPService resendOTPService;
   
   @Autowired
   @Delegate
   private VerifyOTPService verifyOTPService;
   
   @Autowired
   @Delegate
   private VerifyCodeService verifyCodeAndSetPassword;
   
   @Autowired
   @Delegate
   private GetUsersByCriteriaService getUsersByCriteria;
}
