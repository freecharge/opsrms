package com.snapdeal.payments.opsRms.services.impl;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.dao.model.RolePermissionMapperModel;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRmsModel.request.CreateRoleRequest;
import com.snapdeal.payments.opsRmsModel.services.CreateRoleService;

/**
 * @author shubham
 *         26-Oct-2015
 */
@Service
@Slf4j
public class CreateRoleServiceImpl implements CreateRoleService {
   @Autowired
   private PersistanceManager persistanceManager;

   @Autowired
   private RequestParamValidatorForRole<CreateRoleRequest> requestValidator;

   @Override
   @Transactional
   @Logged(printStackTraceEnabled=false)
   @RequestAware
   @Marked
   @ExceptionMetered
   @Timed
   public void createRole(CreateRoleRequest createRole) {
      requestValidator.validate(createRole);
      Integer roleid = persistanceManager.addRole(createRole.getRoleName());
      persistanceManager.savePermissionRoleMapping(getMapperFromRequest(createRole, roleid));

   }

   private RolePermissionMapperModel getMapperFromRequest(CreateRoleRequest request, Integer roleId) {
      return new RolePermissionMapperModel(request.getPermissionIds(), roleId);
   }

}
