package com.snapdeal.payments.opsRms.services.impl;

import java.util.Date;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.IDFactory;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRmsModel.request.ForgotPasswordNotifyRequest;
import com.snapdeal.payments.opsRmsModel.request.GetUserByUserNameRequest;
import com.snapdeal.payments.opsRmsModel.response.GetUserByUserNameResponse;
import com.snapdeal.payments.opsRmsModel.services.ForgotPasswordNotifyService;
import com.snapdeal.payments.opsRmsModel.services.OpsRoleMgmtService;

/**
 * @author aniket
 *         21-Nov-2015
 */
@Service
@Slf4j
public class ForgotPasswordNotifyServiceImpl implements ForgotPasswordNotifyService {

   private final static String templateName = "resetpassword";
   @Autowired
   private PersistanceManager persistanceManager;

   @Autowired
   private RequestParamValidatorForRole<ForgotPasswordNotifyRequest> requestValidator;

   @Autowired
   private OpsRoleMgmtService getUserByUserNameService;

   @Override
   @Transactional
   @Logged
   @Marked
   @Timed
   @ExceptionMetered
   public void forgotPasswordNotify(ForgotPasswordNotifyRequest request) {
      requestValidator.validate(request);
      String newPassword = IDFactory.forgotPasswordNotify();
      String hashedPassword = IDFactory.hashString(newPassword);
      GetUserByUserNameResponse user = getUserByUserNameService
               .getUserByUserName(getUserRequest(request.getUserName()));
      persistanceManager.resetPassword(request.getUserName(), hashedPassword, new Date());

      //notify.send(ChannelType.EMAIL, templateName, newPassword, user.getUser().getEmail());

   }

   private GetUserByUserNameRequest getUserRequest(String userName) {

      return new GetUserByUserNameRequest(userName);
   }

}