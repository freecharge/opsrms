package com.snapdeal.payments.opsRms.services.impl;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Optional;
import com.snapdeal.payments.communicator.dto.CommunicationIntent;
import com.snapdeal.payments.communicator.exception.HttpTransportException;
import com.snapdeal.payments.communicator.exception.ServiceException;
import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.handlers.CommunicatorUtils;
import com.snapdeal.payments.opsRms.handlers.IDFactory;
import com.snapdeal.payments.opsRms.handlers.OTPUtils;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRmsModel.commons.OTPState;
import com.snapdeal.payments.opsRmsModel.dto.GenerateOTPServiceDTO;
import com.snapdeal.payments.opsRmsModel.dto.UserOTPDTO;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.opsRmsModel.exceptions.NoSuchUserExceptionOps;
import com.snapdeal.payments.opsRmsModel.exceptions.OTPServiceExceptionOps;
import com.snapdeal.payments.opsRmsModel.request.GenerateOTPRequest;
import com.snapdeal.payments.opsRmsModel.request.User;
import com.snapdeal.payments.opsRmsModel.response.GenerateOTPResponse;
import com.snapdeal.payments.opsRmsModel.services.GenerateOTPService;

/**
 * @author aniket 
 * 5-Jan-2016
 */
@Service
@Slf4j
public class GenerateOTPServiceImpl implements GenerateOTPService {
	
	private final static String templateName = "resetpassword";
	
	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private RequestParamValidatorForRole<GenerateOTPRequest> requestValidator;

	@Autowired
	private OTPUtils otpUtils;
	
	@Autowired
	private CommunicatorUtils communicatorUtils;

	@Override
	@Logged(printStackTraceEnabled=false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GenerateOTPResponse generateOTP(GenerateOTPRequest generateOtpRequest) {
		requestValidator.validate(generateOtpRequest);
		User user = persistanceManager.getUserByUserName(generateOtpRequest.getUserName());
		if (null == user)
			throw new NoSuchUserExceptionOps(ExceptionMessages.NO_SUCH_USER_EXCEPTION);
		String userId = user.getId();
		otpUtils.verifyFrozenAccount(userId);
		UserOTPDTO otp = generate(user);
		
		CommunicationIntent intent = communicatorUtils.generateCommunicationIntent(user.getEmail(), otp);
		try {
			log.info("Sending communication intent for userEmail: {} " , user.getEmail());
			communicatorUtils.sendCommunicationIntent(intent);
		} catch (HttpTransportException ex){
			log.info("HttpTransportException while sending communication intent for userEmail: {}   Exception: {} ", user.getEmail(), ex.getMessage());
		} catch (ServiceException ex) {
			log.info("ServiceException while sending communication intent for userEmail: {}  Exception: {} ", user.getEmail(), ex.getMessage());
		} catch (Exception ex){
			log.info("Exception while sending communication intent for userEmail: {} Exception: {} ", user.getEmail(), ex.getMessage());
		}
		
		GenerateOTPResponse response = new GenerateOTPResponse(otp.getOtpId());
		log.info("GenerateOTPResponse for userId: {} is {} ", userId, response);
		return response;
	}
	
	//TODO:
	//Check if this can be integrated with the function in resendOTP
	public UserOTPDTO generate(User user) {
		Optional<UserOTPDTO> otpOption = persistanceManager.getLatestOTP(user.getId());
		//TODO:Apply null check here(verify if correct)
		String mobile= user.getMobile() != null?user.getMobile():"";
		if (otpOption.isPresent()) {
			UserOTPDTO otpinfo = otpOption.get();
			OTPState currentOtpState = OTPUtils.getOtpState(otpOption);
			switch (currentOtpState) {
			case VERIFIED:
			case NOT_ACTIVE:
				otpUtils.updateOTPState(otpinfo);
				return otpUtils.generateNewOTPInfo(new GenerateOTPServiceDTO(user.getId(),mobile,user.getEmail()));
			case IN_EXPIRY:
				UserOTPDTO otp = otpUtils.updateResendAttempts(otpOption);
				otpUtils.incrementSendCount(otp);
				return otp;
			default:
				throw new OTPServiceExceptionOps(ExceptionMessages.OTP_GENERIC_EXCEPTION);
			}
		} else {
			return otpUtils.generateNewOTPInfo(new GenerateOTPServiceDTO(user.getId(),mobile,user.getEmail()));
		}

	}
	
	

}
