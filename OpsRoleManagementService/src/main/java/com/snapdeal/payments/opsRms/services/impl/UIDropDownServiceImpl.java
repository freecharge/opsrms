package com.snapdeal.payments.opsRms.services.impl;


import com.snapdeal.payments.opsRms.dao.model.PermissionUserMapperModel;
import com.snapdeal.payments.opsRmsModel.request.*;
import com.snapdeal.payments.opsRmsModel.response.GetAllAppResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRmsModel.response.GetAllPermissionsResponse;
import com.snapdeal.payments.opsRmsModel.response.GetAllRolesResponse;
import com.snapdeal.payments.opsRmsModel.response.GetAllUsersResponse;
import com.snapdeal.payments.opsRmsModel.services.UiDropDownsService;

import java.util.ArrayList;
import java.util.List;

@Service
public class UIDropDownServiceImpl implements UiDropDownsService {

   @Autowired
   private PersistanceManager persistanceManager;

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetAllRolesResponse getAllRoles(GetAllRolesRequest request) {
		return new GetAllRolesResponse(persistanceManager.getAllRoles());
	}

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetAllPermissionsResponse getAllPermissions(GetAllPermissionsRequest request) {
		return new GetAllPermissionsResponse(persistanceManager.getAllPermissions());
	}

	@Override
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public GetAllUsersResponse getAllUsers(GetAllUsersRequest request) {

		return new GetAllUsersResponse(persistanceManager.getAllUsers());
	}

	@Override
	@Logged
	public GetAllAppResponse getAllAppNames(GetAllPermissionsRequest request) {
		List<String> applist = persistanceManager.getAllAppNamesFromPermissions();
		GetAllAppResponse response =  new GetAllAppResponse();
		response.setAppNameList(applist);
		return response;
	}


	@Override
	public GetAllPermissionsResponse getAllPermissionsBasedOnCriteria(UserPermissionRequest request) {
		List<Permission> permissions =  persistanceManager.getAllPermissionsBasedOnCriteria(request);
		GetAllPermissionsResponse permissionsResponse = new GetAllPermissionsResponse();
		permissionsResponse.setPermissions(permissions);
		return permissionsResponse;
	}

	@Override
	public void addNewPermission(AddPermissionRequest request) throws RuntimeException{
		Integer permissionId = persistanceManager.addNewPermissions(request);
		// now add into user permission mapping table;
		PermissionUserMapperModel userPermissionMapper = new PermissionUserMapperModel();
		List<Integer> permissionIds = new ArrayList<>();
		permissionIds.add(permissionId);
		userPermissionMapper.setPermissionIds(permissionIds);
		userPermissionMapper.setUserId(request.getUserId());
		persistanceManager.mapAllPermissionsWithUser(userPermissionMapper);

	}

}
