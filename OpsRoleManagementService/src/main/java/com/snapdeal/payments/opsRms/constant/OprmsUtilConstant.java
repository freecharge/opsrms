package com.snapdeal.payments.opsRms.constant;

public class OprmsUtilConstant {
        public static final String BASE_URI = "/v1";
        public static final String APPLICATION_JSON = "application/json";
        public static final String CREATE_ROLE = "/createRole";
        public static final String UPDATE_ROLE = "/updateRole";
        public static final String CREATE_USER = "/createUser";
        public static final String UPDATE_USER = "/updateUser";
        public static final String DELETE_USER = "/deleteUser";
        public static final String GET_USER_BY_ID = "/getUserByID";
        public static final String GET_USER_BY_USER_NAME = "/getUserByUserName";
        public static final String GET_USERS_BY_ROLE = "/getUsersByRole";
        public static final String CHANGE_PSWD = "/changePassword";
        public static final String FORGOT_PSWD = "/forgotPassword";
        public static final String LOGIN_USER = "/loginUser";
        public static final String TEST_LOGIN_USER = "/testloginUser";
        public static final String LOGOUT_USER = BASE_URI + "/logoutUser";
        public static final String SOCIAL_LOGIN_USER = "/socialLoginUser";
        public static final String PREAUTHRIZE_TO_USER = "/preAuthrizeToUser";
        public static final String GET_ALL_ROLES = "/getAllRoles";
        public static final String GET_ALL_PERMISSIONS = "/getAllPermissions";
        public static final String GET_ALL_USER = "/getAllUsers";
        public static final String GET_ALL_USERS_BY_IDS = "/getAllUsersByIds";
        public static final String GET_ALL_ROLES_BY_ROLENAMES = "/getAllRolesByRoleNames";
        public static final String GET_USER_DETAILS_FROM_TOKEN = "/getUserDetailsFromToken";
        public static final String GENERATE_OTP = BASE_URI + "/forgotPassword/generateOTP";
        public static final String RESEND_OTP = BASE_URI + "/resendOTP";
        public static final String VALIDATE_OTP = BASE_URI + "/forgotPassword/verifyOTP";
        public static final String VALIDATE_CODE = BASE_URI + "/password/verifyCode";
        public static final String CREATE_USER_WITH_LINK = BASE_URI + "/createUser";
        public static final String GET_USER_BY_CRITERIA = BASE_URI + "/users/criteria";

        public static final String SEND_EMAIL_AND_SMS_ON_MERCHANT_CREATION = "/sendEmailAndSmsOnMerchantCreation";
        public static final String SEND_OTP_FOR_MOBILE_VERIFICATION = "/sendOtpForMobileVerification";
        public static final String RESEND_OTP_FOR_MOBILE_VERIFICATION = "/resendOtpForMobileVerification";
        public static final String VERIFY_OTP_FOR_MOBILE_VERIFICATION = "/verifyOtpForMobileVerification";
        public static final String SEND_DUMMY_OTP_FOR_MOBILE_VERIFICATION = "/sendDummyOtpForMobileVerification";
        public static final String RESET_PSWD_TO_DUMMY_PSWD = "/resetPasswordToDummyPasswordForStaging";
        public static final String GET_USER_DETAILS_FROM_TOKEN_FOR_MERCHANT = "/getUserDetailsFromTokenForMerchant";
        public static final String DELETE_AUDIT_USER = "/deleteAuditUser";
        public static final String DOWNLOAD_USERS = "/users/download";

        public static final String PSWD_HASHING_EXCEPTION = "Problem in password hasing";
        public static final String RMS_DEBUGGER_DASHBOARD_SETPSWD = "RMS_DEBUGGER_DASHBOARD_SETPASSWORD";
        public static final String RMS_OP_PANEL_SETPSWD = "RMS_OP_PANEL_SETPASSWORD";
        public static final String RMS_MERCHANT_PANEL_SETPSWD = "RMS_MERCHANT_PANEL_SETPASSWORD";
        public static final String FORGOT_PSWD_INVALID = "Please use OTP generation for resetting password";
        public static final String PSWD_MISMATCH_EXCEPTION = "Incorrect \"current password\"";



}
