package com.snapdeal.payments.opsRms.services.impl;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.PersistanceManager;
import com.snapdeal.payments.opsRms.dao.model.ColumnsConstants;
import com.snapdeal.payments.opsRms.handlers.IDFactory;
import com.snapdeal.payments.opsRms.handlers.RequestParamValidatorForRole;
import com.snapdeal.payments.opsRms.handlers.TokenUtils;
import com.snapdeal.payments.opsRmsModel.dto.GenerateTokenResponse;
import com.snapdeal.payments.opsRmsModel.dto.Token;
import com.snapdeal.payments.opsRmsModel.dto.TokenDTO;
import com.snapdeal.payments.opsRmsModel.request.LoginUserRequest;
import com.snapdeal.payments.opsRmsModel.request.User;
import com.snapdeal.payments.opsRmsModel.response.LoginUserResponse;
import com.snapdeal.payments.opsRmsModel.services.LoginUserService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author aniket 23-Nov-2015
 */
@Service
@Slf4j
public class LoginUserServiceImpl implements LoginUserService {
	@Autowired
	private PersistanceManager persistanceManager;

	@Autowired
	private IDFactory idfactory;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private RequestParamValidatorForRole<LoginUserRequest> requestValidator;

	@Value("${expiry.token}")
	private int expiryDuration;
	
	private static final Logger auditLog = LoggerFactory.getLogger(ColumnsConstants.AUDIT_LOG);

	@Override
	@Transactional
	@Logged(printStackTraceEnabled = false)
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public LoginUserResponse loginUser(LoginUserRequest loginUserRequest) {
		requestValidator.validate(loginUserRequest);
		String userName = loginUserRequest.getUserName();
		User user = persistanceManager.getUserByUserName(userName);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR_OF_DAY, expiryDuration);
		TokenDTO tokenObj;
		Token token = new Token();
		GenerateTokenResponse response = tokenUtils.generateToken();
		token.setToken(response.getToken());
		tokenObj = new TokenDTO(response.getTokenId(),new Date(),Boolean.TRUE,userName);
		persistanceManager.insertToken(tokenObj);
		return getResponseFromData(user, token);
	}

	private LoginUserResponse getResponseFromData(User user, Token token) {
		LoginUserResponse response = new LoginUserResponse();
		response.setToken(token);
		response.setUser(user);
		return response;
	}
}