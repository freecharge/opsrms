package com.snapdeal.payments.opsRms.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snapdeal.payments.opsRmsModel.dto.DownloadUser;
import com.snapdeal.payments.opsRmsModel.response.DownloadUsersResponse;
import org.apache.commons.lang.StringUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Optional;
import com.snapdeal.payments.metrics.annotations.ExceptionMetered;
import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRms.dao.model.UserCriteria;
import com.snapdeal.payments.opsRms.dao.model.UserDaoModel;
import com.snapdeal.payments.opsRmsModel.dto.FreezeAccountDTO;
import com.snapdeal.payments.opsRmsModel.dto.UserVerificationDTO;
import com.snapdeal.payments.opsRmsModel.request.User;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UserDaoImpl implements UserDao {

	@Autowired
	private SqlSessionTemplate sqlSession;

	@Override
	@Transactional
	public void saveUser(UserDaoModel user,UserVerificationDTO userVerificationDTO) {
		sqlSession.insert("user.createUser", user);
		sqlSession.insert("verification_user.saveCode", userVerificationDTO);
	}
	

	@Override
	public void saveUser(UserDaoModel user) {
		sqlSession.insert("user.createUser", user);		
	}

	@Override
	public void updateUser(UserDaoModel user) {
		sqlSession.update("user.updateUser", user);
	}

	@Override
	public User getUserByUserName(String userName) {
		User user = sqlSession.selectOne("user.getUserByUserName", userName);
		return user;
	}

	@Override
	public User getUserById(String userId) {
		User user = sqlSession.selectOne("user.getUserById", userId);
		return user;
	}

	@Override
	public List<User> getUsersByRole(String roleName) {
		List<User> users = sqlSession.selectList("user.getUsersByRole", roleName);
		return users;
	}

	@Override
	public boolean isUserExist(String userId) {
		return (sqlSession.selectOne("user.userExist", userId) != null) ? true : false;
	}

	@Override
	public Date getPasswordExpiryDate(String userName, String oldPassword) {
		Map<String, String> requestObject = new HashMap<String, String>();
		requestObject.put("userName", userName);
		requestObject.put("password", oldPassword);
		Date passwordExpiry = sqlSession.selectOne("user.validateUser", requestObject);
		return passwordExpiry;
	}
	
	@Override
	public void resetPassword(String userName, String newPassword, Date passwordExpiresOn) {
		Map<String, Object> requestObject = new HashMap<String, Object>();
		requestObject.put("userName", userName);
		requestObject.put("password", newPassword);
		requestObject.put("passwordExpiresOn", passwordExpiresOn);
		sqlSession.update("user.changePassword", requestObject);
	}

	@Override
	public List<User> getAllUsers() {
		List<User> users = sqlSession.selectList("user.getAllUsers");
		return users;
	}

	@Override
	public List<User> getUsersByIds(List<String> userIds) {
		List<User> users = sqlSession.selectList("user.getAllUsersByIds", userIds);
		return users;
	}

	@Override
	public void deleteUser(String userId) {
		sqlSession.update("user.deleteUser", userId);
	}

	@Override
	public Optional<FreezeAccountDTO> getFreezedAccount(String userId) {
		List<FreezeAccountDTO> frozenAccountInfo = sqlSession.selectList("frozen_account.getFreezedUser", userId);
		if (frozenAccountInfo == null || frozenAccountInfo.isEmpty()) {
			return Optional.<FreezeAccountDTO> absent();
		}
		return Optional.<FreezeAccountDTO> of(frozenAccountInfo.get(0));
	}

	@Override
	public void freezeUser(FreezeAccountDTO freezeAccount) {
		sqlSession.insert("frozen_account.freezeUser", freezeAccount);
	}

	@Override
	public boolean dropFreezedUser(String userId) {
		int row = sqlSession.update("frozen_account.dropfreezedUser", userId);
		if (row == 0) {
			return false;
		}
		return true;
	}

	@Override
	public void updateFreezeUser(FreezeAccountDTO freezeAccount) {
		sqlSession.update("frozen_account.updateFreezedUser", freezeAccount);
	}

	@Override
	@Transactional
	public String getUserFromCode(String code) {
		String userId = sqlSession.selectOne("verification_user.getUserIdByCode", code);
		if(StringUtils.isNotEmpty(userId) )
			sqlSession.update("verification_user.expireCode",code);
		return userId;
	}
	
	@Override
	public void changePasswordUsingId(String userId,String newPassword,Date passwordExpiresOn) {
		Map<String, Object> requestObject = new HashMap<String, Object>();
		requestObject.put("userId", userId);
		requestObject.put("password", newPassword);
		requestObject.put("passwordExpiresOn", passwordExpiresOn);
		sqlSession.update("user.changePasswordById", requestObject);
	}
	
	@Override
	@Logged
	@RequestAware
	@Marked
	@ExceptionMetered
	@Timed
	public Map<String,UserDaoModel> getSelectedUsers(UserCriteria userCriteria){
		Map<String,UserDaoModel> selectedUsersMap = new HashMap<String,UserDaoModel>();
 		//List<UserDaoModel> selectedUserList = new ArrayList<UserDaoModel>();
		selectedUsersMap = sqlSession.selectMap("user.getUsersByCriteria", userCriteria, "id");
		//selectedUserList = sqlSession.selectList("user.getUsersByCriteria",userCriteria);
		return selectedUsersMap;
	}
	
	@Override
	@Transactional
	public void saveVerificationCode(UserVerificationDTO userVerificationDTO){
		sqlSession.insert("verification_user.saveCode", userVerificationDTO);
	}


	@Override
	@Transactional
	public void deleteAuditUser(String userId) {
		sqlSession.delete("user.deleteAuditUserRole", userId);
		sqlSession.delete("user.deleteAuditUserPermission", userId);
		sqlSession.delete("user.deleteAuditUser", userId);
	}

	@Override
	public DownloadUsersResponse downloadAllUsers() {
		List<DownloadUser> users = sqlSession.selectList("user.downloadAllUsers");
		DownloadUsersResponse downloadUsersResponse = new DownloadUsersResponse();
		downloadUsersResponse.setUsers(users);
		return downloadUsersResponse;
	}

}
