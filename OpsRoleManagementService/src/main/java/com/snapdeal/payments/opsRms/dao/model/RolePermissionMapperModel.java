package com.snapdeal.payments.opsRms.dao.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RolePermissionMapperModel {
   private List<Integer> permissionIds;
   private Integer roleId;
}
