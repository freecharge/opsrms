package com.snapdeal.payments.opsRms.dao.model;

import com.snapdeal.payments.opsRmsModel.commons.EntityConstants;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;

import lombok.Getter;

@Getter
public enum UserFieldColumnName {

   USERID(EntityConstants.USERID,ColumnsConstants.USER_ID),
   USERNAME(EntityConstants.USERNAME,ColumnsConstants.USER_NAME),
   CREATED_ON(EntityConstants.CREATED_ON,ColumnsConstants.CREATED_ON),
   UPDATED_ON(EntityConstants.UPDATED_ON,ColumnsConstants.UPDATED_ON),
   EMAIL(EntityConstants.EMAILID,ColumnsConstants.EMAILID),
   MOBILE_NUMBER(EntityConstants.MOBILENUMBER,ColumnsConstants.MOBILE_NUMBER),
   NAME(EntityConstants.NAME,ColumnsConstants.NAME),
   ACTIVE(EntityConstants.ACTIVE,ColumnsConstants.ACTIVE);
	/**
	 * Commented out these two fields as they are used only for search,
	 * their values are handled separately in service impl
	 */
   //PERMISSION_NAME(EntityConstants.PERMISSION_NAME,ColumnsConstants.PERMISSION_NAME),
   //ROLE_NAME(EntityConstants.ROLE_NAME,ColumnsConstants.ROLE_NAME);
	
      private String fieldName;
      private String columnName;

      private UserFieldColumnName(String field, String column) {
         this.fieldName = field;
         this.columnName=column;
      }

      public static UserFieldColumnName fromValue(String value) {
         UserFieldColumnName type = null;
         if (null != value) {
            for (UserFieldColumnName constant : values()) {
               if (constant.getFieldName().equals(value)) {
                  type = constant;
                  break;
               }
            }
         }
         if (type != null)
              return type;
         else
            throw new ValidationExceptionOps("Invalid user field " + value);

      }
}
