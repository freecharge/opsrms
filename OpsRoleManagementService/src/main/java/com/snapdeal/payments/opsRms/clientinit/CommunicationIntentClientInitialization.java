package com.snapdeal.payments.opsRms.clientinit;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.snapdeal.payments.communicator.client.ISendCommunicationIntentClient;
import com.snapdeal.payments.communicator.client.impl.SendCommunicationIntentClientImpl;
import com.snapdeal.payments.communicator.utils.ClientDetails;

@Configuration
@Slf4j
public class CommunicationIntentClientInitialization<Context extends Enum<Context>> {

	@Value("${communication.intent.clientId}")
	private String clientId;
	@Value("${communication.intent.clientKey}")
	private String clientKey;
	@Value("${communication.intent.IP}")
	private String IP;
	@Value("${communication.intent.port}")
	private String port;
	   
	@Bean
	@Qualifier( "SendCommunicationIntentClient" )
	@Scope
	public ISendCommunicationIntentClient initCommunicationIntent() throws Exception {
		log.info("inside initCommunicationIntent");
	   ClientDetails.init(IP, port, clientKey, clientId);
	   log.info("leaving initCommunicationIntent");
	   return new SendCommunicationIntentClientImpl();
	}
}
