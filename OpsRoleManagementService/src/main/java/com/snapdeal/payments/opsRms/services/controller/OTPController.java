package com.snapdeal.payments.opsRms.services.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.snapdeal.payments.metrics.annotations.Logged;
import com.snapdeal.payments.metrics.annotations.Marked;
import com.snapdeal.payments.metrics.annotations.RequestAware;
import com.snapdeal.payments.metrics.annotations.Timed;
import com.snapdeal.payments.opsRmsModel.commons.RestURIConstants;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionErrorCode;
import com.snapdeal.payments.opsRmsModel.exceptions.OpsRoleMgmtException;
import com.snapdeal.payments.opsRmsModel.request.GenerateOTPRequest;
import com.snapdeal.payments.opsRmsModel.request.ResendOTPRequest;
import com.snapdeal.payments.opsRmsModel.request.VerifyOTPRequest;
import com.snapdeal.payments.opsRmsModel.response.GenerateOTPResponse;
import com.snapdeal.payments.opsRmsModel.response.ServiceResponse;
import com.snapdeal.payments.opsRmsModel.services.OpsRoleMgmtService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class OTPController extends AbstractController {

	@Autowired
	private OpsRoleMgmtService opsRoleMgmtService;

	@RequestAware
    @Marked
    @Logged(printStackTraceEnabled=false)
    @Timed
	@RequestMapping(value = RestURIConstants.GENERATE_OTP, method = RequestMethod.POST)
	public ServiceResponse<GenerateOTPResponse> generateOtp(@RequestBody GenerateOTPRequest request) {

		ServiceResponse<GenerateOTPResponse> response = new ServiceResponse<GenerateOTPResponse>();
		GenerateOTPResponse generateOTPResponse = opsRoleMgmtService.generateOTP(request);
		response.setResponse(generateOTPResponse);
		return response;
	}

	@RequestAware
    @Marked
    @Logged(printStackTraceEnabled=false)
    @Timed
	@RequestMapping(value = RestURIConstants.RESEND_OTP, method = RequestMethod.POST)
	public ServiceResponse<GenerateOTPResponse> reSendOTP(@RequestBody ResendOTPRequest request) {
		ServiceResponse<GenerateOTPResponse> response = new ServiceResponse<GenerateOTPResponse>();
		GenerateOTPResponse generateOTPResponse = opsRoleMgmtService.resendOTP(request);
		response.setResponse(generateOTPResponse);
		return response;
	}

	@RequestAware
    @Marked
    @Logged(printStackTraceEnabled=false)
    @Timed
	@RequestMapping(value = RestURIConstants.VALIDATE_OTP, method = RequestMethod.POST)
	public ServiceResponse<Void> verifyOTP(@RequestBody VerifyOTPRequest request) {

		ServiceResponse<Void> response = new ServiceResponse<Void>();
		opsRoleMgmtService.verifyOTP(request);
		return response;
	}

	@ExceptionHandler(OpsRoleMgmtException.class)
	@Logged
	public <T> ServiceResponse<T> handleException(OpsRoleMgmtException exception) {
		ServiceResponse<T> response = new ServiceResponse<T>();
		OpsRoleMgmtException roleException = new OpsRoleMgmtException(exception.getMessage());
		roleException.setErrorCode(exception.getErrorCode());
		response.setException(roleException);
		return response;
	}

	@ExceptionHandler(Exception.class)
	@Logged
	public <T> ServiceResponse<T> handleException(Exception exception) {
		log.info("Exception {} " , exception.getMessage());
		ServiceResponse<T> response = new ServiceResponse<T>();
		OpsRoleMgmtException roleException = new OpsRoleMgmtException("Internal Server Error. Please try after some time");
		roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
		response.setException(roleException);
		return response;
	}

	@ExceptionHandler(RuntimeException.class)
	@Logged
	public <T> ServiceResponse<T> handleException(RuntimeException exception) {
		log.info("Runtime Exception {} " , exception.getMessage());
		ServiceResponse<T> response = new ServiceResponse<T>();
		OpsRoleMgmtException roleException = new OpsRoleMgmtException("Internal Server Error. Please try after some time");
		roleException.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
		response.setException(roleException);
		return response;
	}
}
