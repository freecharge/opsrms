package com.snapdeal.payments.opsRms.handlers;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.stereotype.Component;

import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionMessages;
import com.snapdeal.payments.opsRmsModel.exceptions.ValidationExceptionOps;

@Component
public class RequestParamValidatorForRole<T> {

   private Validator paramValidator;

   public RequestParamValidatorForRole() {
      initializer();
   }

   private void initializer() {
      ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
      this.paramValidator = validatorFactory.getValidator();
   }

   public void validate(T t) throws ValidationExceptionOps {

      Set<ConstraintViolation<T>> constraintViolations = paramValidator.validate(t);
      if (constraintViolations.size() != 0) {
         StringBuffer errorMessage = new StringBuffer();
         for (ConstraintViolation<T> constraintViolation : constraintViolations) {
            errorMessage.append(constraintViolation.getMessage());
         }

         throw new ValidationExceptionOps(ExceptionMessages.PARAM_VALIDATION_EXCEPTION + errorMessage);
      }
   }
}
