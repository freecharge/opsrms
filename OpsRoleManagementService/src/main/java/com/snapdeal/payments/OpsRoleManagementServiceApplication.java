package com.snapdeal.payments;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan({ "com.snapdeal.payments.vault.*","com.snapdeal.*" })
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@ImportResource("classpath*:spring/application-context.xml")
@EnableConfigurationProperties
@EnableScheduling
@EnableRetry
@EnableAspectJAutoProxy
public class OpsRoleManagementServiceApplication {

   public static void main(String[] args) {
      SpringApplication.run(OpsRoleManagementServiceApplication.class, args);
   }
}
