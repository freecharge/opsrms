pipeline {
    agent any
    environment {
        //**** This RPM_NAME should be always equal to the component name
        //**** and will also be unsed as service name
        //**** We only need to change this variable per microservice
        RPM_NAME = "oprms"

        //**** Product to which this microservice belongs
        PRODUCT = "oprms"

        // service folder which will contain the target folder in case of root directory use "./"
        TARGET_FOLDER = "OpsRoleManagementService"

        // Jar name should not contain any version string ask developer to use <finalName> tag in pom.xml
        JAR_NAME = "OpsRoleManagementService.jar"


        HEALTH_CHECK_RESPONSE = "OK 200"

        // Jira credentials to verify JiraID during deployment stage.
        // Please don't change this variable
        JIRA_CREDENTIALS = credentials('jira-plugin')

        // system variables please don't change these variables at any cost //

        appname = "$RPM_NAME"
        SERVICE_NAME = "$RPM_NAME"
        COMP_NAME = "$TARGET_FOLDER"

        QA_HEALTH_CHECK_ENDPOINT = "https://${SERVICE_NAME}-qa.fcinternal.in/health"
        STAGING_HEALTH_CHECK_ENDPOINT = "https://${SERVICE_NAME}-staging.fcinternal.in/health"
        PRODUCTION_HEALTH_CHECK_ENDPOINT = "https://${SERVICE_NAME}-prod.freecharge.in/health"

        DATABAG_JOB= "$RPM_NAME"+"_databag_build"
        selected_deployment_version = ""
        selected_traffic_weight = ""
        DEPLOYMENT_VERSION = "blue green red"
        TRAFFIC_WEIGHT = "10% 30% 50% 100% NA"
    }
    options {
        ansiColor('vga')
    }
    stages {
        stage('Compilation and code analysis') {
        //    environment {
          //      scannerHome = tool 'Sonar'
            //}
            tools {
               // maven 'maven-latest'
                jdk "java-1.8.0-amazon-corretto"
            }
            steps {
                script {
                    env.ORIGIN_BUILD_NUMBER = env.BUILD_NUMBER
                }
                sh "echo 'This stage will compile the code and perform the code analysis along with unit test'"
                //git branch: '$BranchName', credentialsId: 'id_rsa', url: 'git@bitbucket.org:freecharge/infra_control_centre.git'
                // some compilation step here
                sh 'mvn clean install -U'
               //  withSonarQubeEnv('sonarqube') {
               //     sh "${scannerHome}/bin/sonar-scanner"
               // }
            }
        }

        stage('Building Artifact') {
            steps {
                sh "echo 'Origin build is : $ORIGIN_BUILD_NUMBER'"
                sh "echo 'This stage will create a deployable build'"
                sh "sh -x $JENKINS_HOME/workspace/devops_common/scripts/docker/kube_build.sh"
            }
        }

        /*stage('Dev Deployment') {
            when {
             	expression {
             		GIT_BRANCH == 'origin/development' || GIT_BRANCH == 'origin/master'
        		}
            }
            steps {
                sh "echo 'Origin build is : $ORIGIN_BUILD_NUMBER'"
                script {
                    env.deployment_environment = "dev"
                    env.deployment_type = "complete_deployment"
                    List DEPLOYMENT_VERSION_LIST  = DEPLOYMENT_VERSION.split()
                    List TRAFFIC_WEIGHT_LIST  = TRAFFIC_WEIGHT.split()
                    //TODO: add new version for multiple instance of one service
                    //selected_deployment_version = input(
                                message: "Select the deployment version for ${deployment_environment} environment",
                    //    message: "Select the deployment version ",
                    //    parameters: [choice(choices: DEPLOYMENT_VERSION_LIST, description: '', name: 'DEPLOYMENT_VERSION')])
                    //echo 'deployment_version: ' + selected_deployment_version
                    selected_deployment_version = "blue"
                    selected_traffic_weight = "NA"
                }
                sh "echo 'This stage will promote and deploy the code to ${deployment_environment}'"
                sh "sh -x $JENKINS_HOME/workspace/devops_common/scripts/promotion/dev/promote_docker_pipeline.sh ${SERVICE_NAME} latest-${selected_deployment_version}"
                //sh "kubectl apply -f $JENKINS_HOME/workspace/fc-kube-config/env-${deployment_environment}/components/${SERVICE_NAME}/${selected_deployment_version}  --context ${deployment_environment}"
            }
        }*/

         stage('QA Deployment') {
//          when {
//              	expression {
//              		GIT_BRANCH == 'origin/eks_change' || GIT_BRANCH == 'origin/master'
//         		}
//             }
            steps {
                sh "echo 'Origin build is : $ORIGIN_BUILD_NUMBER'"
                script {
                    env.deployment_environment = "qa"
                    env.deployment_type = "complete_deployment"
                    List DEPLOYMENT_VERSION_LIST  = DEPLOYMENT_VERSION.split()
                    List TRAFFIC_WEIGHT_LIST  = TRAFFIC_WEIGHT.split()

                    try {
                        timeout(time: 5, unit: 'MINUTES') {
                            selected_deployment_version = input(
                                message: "Select the deployment version for ${deployment_environment} environment",
                                parameters: [choice(choices: DEPLOYMENT_VERSION_LIST, description: '', name: 'DEPLOYMENT_VERSION')])
                            selected_traffic_weight = "NA"
                            }
                    } catch (err) {
                        currentBuild.result = 'SUCCESS'
                        throw err
                    }
                    sh "echo 'deployment_version: ' + ${selected_deployment_version}"
                    sh "python /var/lib/jenkins/workspace/devops_common/scripts/validate_jira_issue.py $JIRA_CREDENTIALS_USR $JIRA_CREDENTIALS_PSW ${deployment_environment} ${JiraID} '${Component}'"
                    sh "echo 'This stage will promote and deploy the code to ${deployment_environment}'"
                   // sh "sh -x $JENKINS_HOME/workspace/devops_common/scripts/promotion/${deployment_environment}/promote_docker_pipeline.sh ${SERVICE_NAME} latest-${selected_deployment_version}"
                    sh "sh -x $JENKINS_HOME/workspace/devops_common/scripts/promotion/qa/promote_docker_pipeline_skipsonar.sh ${SERVICE_NAME} latest-${selected_deployment_version}"
                    sh "kubectl apply -f $JENKINS_HOME/workspace/fc-kube-config/env-${deployment_environment}/components/${PRODUCT}/  --context ${deployment_environment}"
                    sh "kubectl apply -f $JENKINS_HOME/workspace/fc-kube-config/env-${deployment_environment}/components/${PRODUCT}/${SERVICE_NAME}/  --context ${deployment_environment}"
                    sh "kubectl apply -f $JENKINS_HOME/workspace/fc-kube-config/env-${deployment_environment}/components/${PRODUCT}/${SERVICE_NAME}/${selected_deployment_version}  --context ${deployment_environment}"
                    sh "kubectl rollout restart deployment ${SERVICE_NAME}-${selected_deployment_version} -n ${PRODUCT}  --context ${deployment_environment}"
              		sh "kubectl rollout status deployment ${SERVICE_NAME}-${selected_deployment_version} -n ${PRODUCT}  --context ${deployment_environment}"
                }
            }
        }

        /*stage('QA Sanity') {
            when {
             	expression {
             		GIT_BRANCH == 'origin/development' || GIT_BRANCH == 'origin/master'
        		}
            }
            steps {
                sh "echo 'This stage will will perform a sanity suite on qa deployment'"
                build job: 'recharge_QE',
                    parameters: [
                        gitParameter(name: 'BranchName', value: 'master'),
                        string(name: 'Regression_Suite', value: 'rechargeSanity.xml'),
                        string(name: 'deployment_environment', value: '${deployment_environment}')
                    ],
                    propagate: false
                sh "curl -v --silent  -H 'deployment-version: $selected_deployment_version'  $QA_HEALTH_CHECK_ENDPOINT 2>&1 | grep '$HEALTH_CHECK_RESPONSE'"
            }
        }*/

        stage('Staging Deployment') {
//            when {
//             	expression {
//             		GIT_BRANCH == 'origin/development' || GIT_BRANCH == 'origin/master'
//         		}
//             }
            steps {
                script {
                    env.deployment_environment = "staging"
                    env.deployment_type = "complete_deployment"
                    List DEPLOYMENT_VERSION_LIST = DEPLOYMENT_VERSION.split()
                    List TRAFFIC_WEIGHT_LIST  = TRAFFIC_WEIGHT.split()

                    try {
                        timeout(time: 5, unit: 'MINUTES') {
                            selected_deployment_version = input(
                                message: "Select the deployment version for ${deployment_environment} environment",
                                parameters: [choice(choices: DEPLOYMENT_VERSION_LIST, description: '', name: 'DEPLOYMENT_VERSION')])
                            echo 'deployment_version: ' + selected_deployment_version
                            // selected_traffic_weight = input(
                            //     message: "Select the traffic weight",
                            //     parameters: [choice(choices: TRAFFIC_WEIGHT_LIST, description: '', name: 'TRAFFIC_WEIGHT')])
                            // echo 'traffic_weight: ' + selected_deployment_version
                        }
                    } catch (err) {
                        currentBuild.result = 'SUCCESS'
                        throw err
                    }
                    sh "echo 'This stage will promote and deploy the code to ${deployment_environment}'"
                    //sh "sh -x $JENKINS_HOME/workspace/devops_common/scripts/promotion/${deployment_environment}/promote_docker_pipeline.sh ${SERVICE_NAME} latest-${selected_deployment_version}"
                    sh "sh -x $JENKINS_HOME/workspace/devops_common/scripts/promotion/staging/promote_docker_pipeline_skipsonar.sh ${SERVICE_NAME} latest-${selected_deployment_version}"
                    sh "kubectl apply -f $JENKINS_HOME/workspace/fc-kube-config/env-${deployment_environment}/components/${PRODUCT}/  --context ${deployment_environment}"
                    sh "kubectl apply -f $JENKINS_HOME/workspace/fc-kube-config/env-${deployment_environment}/components/${PRODUCT}/${SERVICE_NAME}/  --context ${deployment_environment}"
                    sh "kubectl apply -f $JENKINS_HOME/workspace/fc-kube-config/env-${deployment_environment}/components/${PRODUCT}/${SERVICE_NAME}/${selected_deployment_version}  --context ${deployment_environment}"
                    sh "kubectl rollout restart deployment ${SERVICE_NAME}-${selected_deployment_version} -n ${PRODUCT}  --context ${deployment_environment}"
              		sh "kubectl rollout status deployment ${SERVICE_NAME}-${selected_deployment_version} -n ${PRODUCT}  --context ${deployment_environment}"
                }
            }
        }

        /*stage('Staging Sanity') {
            when {
             	expression {
             		GIT_BRANCH == 'origin/development' || GIT_BRANCH == 'origin/master'
        		}
            }
            steps {
                sh "echo 'This stage will will perform a sanity suite on staging deployment'"
                build job: 'recharge_QE',
                    parameters: [
                        gitParameter(name: 'BranchName', value: 'master'),
                        string(name: 'Regression_Suite', value: 'rechargeSanity.xml'),
                        string(name: 'deployment_environment', value: '${deployment_environment}')
                    ],
                    propagate: false
                sh "curl -v --silent -H 'deployment-version: $selected_deployment_version' $STAGING_HEALTH_CHECK_ENDPOINT 2>&1 | grep '$HEALTH_CHECK_RESPONSE'"
            }
        }*/

        stage('Production Deployment') {
            /* when {
             	expression {
             		GIT_BRANCH == 'origin/CS-3070-V2.0-EKS' || GIT_BRANCH == 'origin/master'
        		}
            } */

            steps {
                script {
                    env.deployment_environment = "prod"
                    env.deployment_type = "complete_deployment"
                    List DEPLOYMENT_VERSION_LIST  = DEPLOYMENT_VERSION.split()
                    List TRAFFIC_WEIGHT_LIST  = TRAFFIC_WEIGHT.split()

                    try {
                        timeout(time: 5, unit: 'MINUTES') {
                            selected_deployment_version = input(
                                message: "Select the deployment version for ${deployment_environment} environment",
                                parameters: [choice(choices: DEPLOYMENT_VERSION_LIST, description: '', name: 'DEPLOYMENT_VERSION')])
                            selected_traffic_weight = "NA"
                            }
                    } catch (err) {
                        currentBuild.result = 'SUCCESS'
                        throw err
                    }
                }
                sh "python /var/lib/jenkins/workspace/devops_common/scripts/validate_jira_issue.py $JIRA_CREDENTIALS_USR $JIRA_CREDENTIALS_PSW ${deployment_environment} ${JiraID_CMR} '${Component}'"
                sh "echo 'This stage will promote and deploy the code to ${deployment_environment}'"
                //sh "sh -x $JENKINS_HOME/workspace/devops_common/scripts/promotion/${deployment_environment}/promote_docker_pipeline.sh ${SERVICE_NAME} latest-${selected_deployment_version}"
                sh "sh -x $JENKINS_HOME/workspace/devops_common/scripts/promotion/${deployment_environment}/promote_docker_pipeline_skipsonar.sh ${SERVICE_NAME} latest-${selected_deployment_version}"
                sh "kubectl apply -f $JENKINS_HOME/workspace/fc-kube-config/env-${deployment_environment}/components/${PRODUCT}/  --context ${deployment_environment}"
                sh "kubectl apply -f $JENKINS_HOME/workspace/fc-kube-config/env-${deployment_environment}/components/${PRODUCT}/${SERVICE_NAME}/  --context ${deployment_environment}"
                sh "kubectl apply -f $JENKINS_HOME/workspace/fc-kube-config/env-${deployment_environment}/components/${PRODUCT}/${SERVICE_NAME}/${selected_deployment_version}  --context ${deployment_environment}"
                sh "kubectl rollout restart deployment ${SERVICE_NAME}-${selected_deployment_version} -n ${PRODUCT}  --context ${deployment_environment}"
          		sh "kubectl rollout status deployment ${SERVICE_NAME}-${selected_deployment_version} -n ${PRODUCT}  --context ${deployment_environment}"
            }
        }

        /*stage('Production Smoke') {
            when {
                anyOf {
                    environment name: 'GIT_BRANCH', value: 'origin/master'
                }
            }

            steps {
                    sh "echo 'This stage will will perform a regression suite on staging deployment'"
                // TODO: sh "curl -v --silent $PRODUCTION_HEALTH_CHECK_ENDPOINT 2>&1 | grep '$HEALTH_CHECK_RESPONSE'"
            }
        }*/
    }
    post {
        always {

            build job: 'automation_create_ad_group',
                parameters: [
                    string(name: 'Product', value: env.PRODUCT),
                    string(name: 'Component', value: env.SERVICE_NAME)
                ],
                propagate: false
        }
    }
}