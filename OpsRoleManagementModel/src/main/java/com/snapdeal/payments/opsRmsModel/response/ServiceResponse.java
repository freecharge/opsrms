package com.snapdeal.payments.opsRmsModel.response;

import java.util.Date;

import com.snapdeal.payments.opsRmsModel.exceptions.OpsRoleMgmtException;

import lombok.Data;

public @Data class ServiceResponse<T> {
	private T response;
	private OpsRoleMgmtException exception;
	private Date serverTimeStamp;

	public void setResponse(T response) {
		this.response = response;
		this.serverTimeStamp = new Date();
	}
}
