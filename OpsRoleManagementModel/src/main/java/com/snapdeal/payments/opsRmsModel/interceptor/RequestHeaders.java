package com.snapdeal.payments.opsRmsModel.interceptor;

import lombok.Getter;
import lombok.Setter;

public enum RequestHeaders {
   TOKEN("token"), APP_NAME("appName");
   @Setter
   @Getter
   private String name;

   RequestHeaders(String name) {
      this.setName(name);
   }
}
