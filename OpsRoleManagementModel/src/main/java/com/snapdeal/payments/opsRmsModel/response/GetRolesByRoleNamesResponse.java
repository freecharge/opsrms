package com.snapdeal.payments.opsRmsModel.response;

import java.util.List;

import com.snapdeal.payments.opsRmsModel.request.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author aniket
 *         11-Dec-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetRolesByRoleNamesResponse {

   private List<Role> roles;

}
