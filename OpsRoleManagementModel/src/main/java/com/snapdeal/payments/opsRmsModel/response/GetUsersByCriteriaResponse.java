package com.snapdeal.payments.opsRmsModel.response;

import java.util.List;

import com.snapdeal.payments.opsRmsModel.request.User;

import lombok.Data;

@Data
public class GetUsersByCriteriaResponse {
   private static final long serialVersionUID = -420223466518706L;
   private int totalCount;
   private List<User> userList;
}
