package com.snapdeal.payments.opsRmsModel.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

/**
 * @author aniket
 *         5-Jan-2016
 */
@Data
public class GenerateOTPRequest extends AbstractRequest {
   /**
	 * 
	 */
   private static final long serialVersionUID = 1231424L;

   @NotBlank
   private String userName;

}
