package com.snapdeal.payments.opsRmsModel.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author aniket
 * 	5-Jan-2016
 */
@NoArgsConstructor
@AllArgsConstructor
public @Data class FreezeAccountDTO {

	private String userId;
	private Date expiryTime;
	private String freezeReason;
	private String isdeleted;
}
