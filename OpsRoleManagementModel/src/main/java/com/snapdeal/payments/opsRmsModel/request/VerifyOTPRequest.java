package com.snapdeal.payments.opsRmsModel.request;

import org.hibernate.validator.constraints.NotBlank;

import com.snapdeal.payments.opsRmsModel.commons.Password;

import lombok.Data;
import lombok.ToString;

/**
 * @author aniket
 *         7-Jan-2016
 */
@Data
@ToString(exclude={"otp","newPassword"})
public class VerifyOTPRequest extends AbstractRequest {
   /**
	 * 
	 */
   private static final long serialVersionUID = 123143324L;

   @NotBlank
   private String otpId;
   @NotBlank
   private String otp;
   @Password
   private String newPassword;

}
