package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.DeleteUserRequest;

/**
 * @author aniket
 *         8-Dec-2015
 */
public interface DeleteUserService {

   public void deleteUser(DeleteUserRequest deleteUser);
   public void deleteAuditUser(DeleteUserRequest deleteUser);
}
