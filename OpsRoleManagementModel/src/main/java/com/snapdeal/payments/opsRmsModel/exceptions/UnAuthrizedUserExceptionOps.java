package com.snapdeal.payments.opsRmsModel.exceptions;

/**
 * @author shubham
 *         21-Oct-2015
 */
public class UnAuthrizedUserExceptionOps extends ValidationExceptionOps {

   private static final long serialVersionUID = -4212829204867344092L;

   public UnAuthrizedUserExceptionOps(RuntimeException e) {
      super(e);
   }

   public UnAuthrizedUserExceptionOps() {
      super();
   }

   public UnAuthrizedUserExceptionOps(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.UNAUTHRIZED_USER);
   }
}
