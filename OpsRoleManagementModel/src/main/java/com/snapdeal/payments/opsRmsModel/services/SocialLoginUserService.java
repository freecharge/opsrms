package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.SocialLoginUserRequest;
import com.snapdeal.payments.opsRmsModel.response.LoginUserResponse;

/**
 * @author aniket
 *         24-Nov-2015
 */
public interface SocialLoginUserService {

   public LoginUserResponse socialLoginUser(SocialLoginUserRequest socialLoginUserRequest);
}
