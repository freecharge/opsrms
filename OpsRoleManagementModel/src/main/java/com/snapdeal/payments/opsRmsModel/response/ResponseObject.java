package com.snapdeal.payments.opsRmsModel.response;

import lombok.Data;

@Data
public class ResponseObject {
	private String error;
	private String data;
}
