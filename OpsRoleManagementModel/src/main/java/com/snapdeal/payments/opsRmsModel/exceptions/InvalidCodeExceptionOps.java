package com.snapdeal.payments.opsRmsModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when the service request is invalid
 * 
 * @author aniket
 *         21-Jan-2016
 *
 */

@NoArgsConstructor
public class InvalidCodeExceptionOps extends OpsRoleMgmtException {

   public InvalidCodeExceptionOps(Exception e) {
      super(e);
   }

   public InvalidCodeExceptionOps(Throwable e) {
      super(e);
   }

   public InvalidCodeExceptionOps(String message) {
      super(message);
   }
   public InvalidCodeExceptionOps(String message,Throwable t) {
      super(message,t);
   }
   {
      this.setErrorCode(ExceptionErrorCode.INVALID_CODE);
   }

}
