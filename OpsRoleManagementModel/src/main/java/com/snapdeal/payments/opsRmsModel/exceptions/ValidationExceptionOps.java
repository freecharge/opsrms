package com.snapdeal.payments.opsRmsModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when the service request is invalid
 * 
 * @author shubham
 *         21-Oct-2015
 *
 */

@NoArgsConstructor
public class ValidationExceptionOps extends OpsRoleMgmtException {

   public ValidationExceptionOps(Exception e) {
      super(e);
   }

   public ValidationExceptionOps(Throwable e) {
      super(e);
   }

   public ValidationExceptionOps(String message) {
      super(message);
   }

   public ValidationExceptionOps(String message, Throwable throwable) {
      super(message, throwable);
   }

   {
      this.setErrorCode(ExceptionErrorCode.DEFAULT_VALIDATION);
   }

}
