package com.snapdeal.payments.opsRmsModel.request;

import java.util.List;

import lombok.Data;

/**
 * @author shubham
 *         19-Oct-2015
 */
@Data
public class User {
   private String id;

   private String userName;
   private String name;
   private String email;
   private String mobile;
   private boolean active;
   private List<Permission> permissions;
   private List<Role> roles;
   private String createdOn;
   private String updatedOn;
}
