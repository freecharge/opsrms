package com.snapdeal.payments.opsRmsModel.response;

import lombok.Data;
@Data
public class RequestObject {
	private String request;
}
