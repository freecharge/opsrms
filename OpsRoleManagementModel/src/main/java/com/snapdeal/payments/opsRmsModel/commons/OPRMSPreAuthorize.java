/**
 * 
 */
package com.snapdeal.payments.opsRmsModel.commons;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for specifying a method access-control expression which will be
 * evaluated to decide whether a
 * method invocation is Allowed or Not.
 * 
 * Format of input :
 * 
 * @OPRMSPreAuthorize("(hasPermission('P1') and hasPermission('P2') and
 *                                     !hasPermission('P3'))")
 * 
 *
 * @author shubham
 * 
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface OPRMSPreAuthorize {
   /**
    * @return the Spring-EL expression to be evaluated before invoking the
    *         protected method
    */
   String value() default "";
}
