package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.GetRolesByRoleNamesRequest;
import com.snapdeal.payments.opsRmsModel.response.GetRolesByRoleNamesResponse;

/**
 * @author aniket
 *         8-Dec-2015
 */
public interface GetRolesByRoleNamesService {

   public GetRolesByRoleNamesResponse getRolesByRoleNames(GetRolesByRoleNamesRequest getRoles);

}
