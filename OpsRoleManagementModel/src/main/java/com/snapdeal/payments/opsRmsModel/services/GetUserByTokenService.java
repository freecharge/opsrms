package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.GetUserByTokenRequest;
import com.snapdeal.payments.opsRmsModel.response.GetUserByTokenResponse;


public interface GetUserByTokenService {

   public GetUserByTokenResponse getUserByToken(GetUserByTokenRequest userrequest);
   public GetUserByTokenResponse getUserByTokenForMerchant(GetUserByTokenRequest userrequest);
}
