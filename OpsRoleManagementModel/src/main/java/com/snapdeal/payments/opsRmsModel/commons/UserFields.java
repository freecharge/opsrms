package com.snapdeal.payments.opsRmsModel.commons;


import lombok.Getter;

@Getter
public enum UserFields {

   USERID(EntityConstants.USERID),
   USERNAME(EntityConstants.USERNAME),
   MOBILE(EntityConstants.MOBILENUMBER),
   EMAIL(EntityConstants.EMAILID),
   PERMISSION_NAME(EntityConstants.PERMISSION_NAME),
   ROLE_NAME(EntityConstants.ROLE_NAME),
   NAME(EntityConstants.NAME),
   CREATEDON(EntityConstants.CREATED_ON),
   ACTIVE(EntityConstants.ACTIVE),
   UPDATEDON(EntityConstants.UPDATED_ON);

   private String fieldName;

   private UserFields(String field) {
      this.fieldName = field;
   }

   public static UserFields fromValue(String value) {
      UserFields type = null;
      if (null != value) {
         for (UserFields constant : values()) {
            if (constant.getFieldName().equalsIgnoreCase(value)) {
               type = constant;
            }
         }
      }
      return type;
   }
}
