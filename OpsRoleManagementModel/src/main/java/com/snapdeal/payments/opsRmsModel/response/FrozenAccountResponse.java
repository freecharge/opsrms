package com.snapdeal.payments.opsRmsModel.response;

import lombok.Data;

/**
 * @author aniket
 *         5-Jan-2016
 */
public @Data class FrozenAccountResponse {
	private boolean status;
	private String requestType;
	private long remainingMinutes ;
}
