package com.snapdeal.payments.opsRmsModel.dto;

import com.snapdeal.payments.opsRmsModel.commons.OTPState;

import lombok.Data;

public @Data class UpdateOTPStateDTO {
	private String otpId;
	private OTPState otpStateCurrent;
	private OTPState otpStateExpected;
}