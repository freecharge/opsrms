package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.GenerateOTPRequest;
import com.snapdeal.payments.opsRmsModel.response.GenerateOTPResponse;

/**
 * @author aniket
 *         5-Jan-2015
 */
public interface GenerateOTPService {

   public GenerateOTPResponse generateOTP(GenerateOTPRequest generateOtpRequest);
}
