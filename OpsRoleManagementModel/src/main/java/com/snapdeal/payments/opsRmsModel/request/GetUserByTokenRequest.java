package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;

/**
 * @author aniket
 *         29-Dec-2015
 */
@Data
public class GetUserByTokenRequest extends AbstractRequest {
   /**
    * 
    */
   private static final long serialVersionUID = 1347891L;
}
