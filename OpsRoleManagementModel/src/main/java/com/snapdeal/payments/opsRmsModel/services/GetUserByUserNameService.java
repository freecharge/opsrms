package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.GetUserByUserNameRequest;
import com.snapdeal.payments.opsRmsModel.response.GetUserByUserNameResponse;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface GetUserByUserNameService {

   public GetUserByUserNameResponse getUserByUserName(GetUserByUserNameRequest userRequest);
}
