package com.snapdeal.payments.opsRmsModel.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import lombok.ToString;

/**
 * @author aniket
 *         23-Nov-2015
 */
@Data
@ToString(exclude = {"password"})
public class LoginUserRequest extends AbstractRequest {

	private static final long serialVersionUID = -8472296562359475060L;

	@NotBlank
	private String userName;
//	@NotBlank
	private String password;
	

}
