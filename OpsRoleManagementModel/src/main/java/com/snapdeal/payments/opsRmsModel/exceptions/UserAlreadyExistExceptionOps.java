package com.snapdeal.payments.opsRmsModel.exceptions;

/**
 * @author shubham
 *         21-Oct-2015
 */
public class UserAlreadyExistExceptionOps extends ValidationExceptionOps {

   private static final long serialVersionUID = -4212829204867344092L;

   public UserAlreadyExistExceptionOps(RuntimeException e) {
      super(e);
   }

   public UserAlreadyExistExceptionOps() {
      super();
   }

   public UserAlreadyExistExceptionOps(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.USER_ALREADY_EXIST);
   }
}
