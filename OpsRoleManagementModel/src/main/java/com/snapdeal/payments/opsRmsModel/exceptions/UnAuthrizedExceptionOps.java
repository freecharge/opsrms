package com.snapdeal.payments.opsRmsModel.exceptions;

/**
 * @author shubham
 *         17-Oct-2015
 */
public class UnAuthrizedExceptionOps extends ValidationExceptionOps {

   public UnAuthrizedExceptionOps() {
      super();
   }

   public UnAuthrizedExceptionOps(String message) {
      super(message);
   }
}
