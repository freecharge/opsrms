package com.snapdeal.payments.opsRmsModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when an Expired OTP is entered
 * 
 * @author roopali 26-Oct-2018
 *
 */

@NoArgsConstructor
public class OTPIdInvalidExceptionOps extends OpsRoleMgmtException {

	private static final long serialVersionUID = 1L;

	public OTPIdInvalidExceptionOps(Exception e) {
		super(e);
	}

	public OTPIdInvalidExceptionOps(Throwable e) {
		super(e);
	}

	public OTPIdInvalidExceptionOps(String message) {
		super(message);
	}

	{
		this.setErrorCode(ExceptionErrorCode.OTP_ID_INVALID);
	}

}
