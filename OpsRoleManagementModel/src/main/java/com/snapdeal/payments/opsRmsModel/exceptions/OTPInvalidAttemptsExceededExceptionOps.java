package com.snapdeal.payments.opsRmsModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown on exceeding verification attempts
 * 
 * @author roopali 26-Oct-2018
 *
 */

@NoArgsConstructor
public class OTPInvalidAttemptsExceededExceptionOps extends OpsRoleMgmtException {

	private static final long serialVersionUID = 1L;

	public OTPInvalidAttemptsExceededExceptionOps(Exception e) {
		super(e);
	}

	public OTPInvalidAttemptsExceededExceptionOps(Throwable e) {
		super(e);
	}

	public OTPInvalidAttemptsExceededExceptionOps(String message) {
		super(message);
	}

	{
		this.setErrorCode(ExceptionErrorCode.INVALID_OTP_ATTEMPTS_EXCEEDED);
	}

}
