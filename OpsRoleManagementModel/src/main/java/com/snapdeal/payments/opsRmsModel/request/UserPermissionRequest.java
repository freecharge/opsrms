package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;

@Data
public class UserPermissionRequest extends AbstractRequest{

    private String appName;
    private String permissionName;
    private String displayText;

}
