package com.snapdeal.payments.opsRmsModel.dto;

import lombok.Data;

@Data
public class RangeFilter {
   private String fieldName;
   private String upperLimit;
   private String lowerLimit;
}
