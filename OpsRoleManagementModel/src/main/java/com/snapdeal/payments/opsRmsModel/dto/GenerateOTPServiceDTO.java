package com.snapdeal.payments.opsRmsModel.dto;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author aniket 5-Jan-2016
 */
@Data
@AllArgsConstructor
public class GenerateOTPServiceDTO{

	private static final long serialVersionUID = -1118240620874477648L;

	@NotBlank
	private String userId;

	private String mobileNumber;

	@NotBlank
	private String emailId;

}
