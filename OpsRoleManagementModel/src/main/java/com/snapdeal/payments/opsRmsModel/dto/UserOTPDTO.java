package com.snapdeal.payments.opsRmsModel.dto;

import java.io.Serializable;
import java.util.Date;

import com.snapdeal.payments.opsRmsModel.commons.OTPState;

import lombok.Data;
import lombok.ToString;


/**
 * 
 * @author aniket
 * 5-Jan-2016
 */

@ToString(exclude = {"otp"})
public @Data class UserOTPDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3644770846698623989L;
	private String userId;
	private String mobileNumber;

	private String email;

	private String otp;
	private String otpId;
	private int invalidAttempts;
	private int resendAttempts;
	private Date expiryTime;

	private OTPState otpState;

	private Date createdOn;
	private Date modifiedOn;
}
