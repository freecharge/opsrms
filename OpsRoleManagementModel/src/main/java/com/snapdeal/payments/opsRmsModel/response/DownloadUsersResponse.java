package com.snapdeal.payments.opsRmsModel.response;

import com.snapdeal.payments.opsRmsModel.dto.DownloadUser;
import lombok.Data;

import java.util.List;

@Data
public class DownloadUsersResponse {
    private List<DownloadUser> users;
}
