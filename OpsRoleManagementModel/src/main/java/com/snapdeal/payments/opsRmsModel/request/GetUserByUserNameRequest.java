package com.snapdeal.payments.opsRmsModel.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author shubham
 *         19-Oct-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserByUserNameRequest extends AbstractRequest {

   @NotBlank
   private String userName;

}
