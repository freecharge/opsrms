package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;

/**
 * @author shubham
 *         17-Oct-2015
 */
@Data
public class GetAllRolesRequest extends AbstractRequest {
   /**
    * 
    */
   private static final long serialVersionUID = 1L;
}
