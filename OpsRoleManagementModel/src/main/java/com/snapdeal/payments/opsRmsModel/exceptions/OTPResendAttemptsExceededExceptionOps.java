package com.snapdeal.payments.opsRmsModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown on exceeding OTP resend attempts
 * 
 * @author roopali 26-Oct-2018
 *
 */

@NoArgsConstructor
public class OTPResendAttemptsExceededExceptionOps extends OpsRoleMgmtException {

	private static final long serialVersionUID = 1L;

	public OTPResendAttemptsExceededExceptionOps(Exception e) {
		super(e);
	}

	public OTPResendAttemptsExceededExceptionOps(Throwable e) {
		super(e);
	}

	public OTPResendAttemptsExceededExceptionOps(String message) {
		super(message);
	}

	{
		this.setErrorCode(ExceptionErrorCode.RESEND_OTP_ATTEMPTS_EXCEEDED);
	}

}
