package com.snapdeal.payments.opsRmsModel.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.snapdeal.payments.opsRmsModel.request.User;

/**
 * @author aniket
 *         8-Dec-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUsersByIdsResponse {

   private List<User> users;

}
