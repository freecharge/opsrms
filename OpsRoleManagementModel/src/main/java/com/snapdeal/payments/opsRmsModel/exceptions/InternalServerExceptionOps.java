package com.snapdeal.payments.opsRmsModel.exceptions;

/**
 * @author shubham
 *         21-Oct-2015
 */
public class InternalServerExceptionOps extends InternalErrorExceptionOps {

   private static final long serialVersionUID = 8655878429831876879L;

   public InternalServerExceptionOps(Throwable e) {
      super(e);
   }

   public InternalServerExceptionOps() {
   }

   public InternalServerExceptionOps(String message) {
      super(message);
   }

   public InternalServerExceptionOps(String message, Throwable t) {
      super(t, message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.SERVER_INTERNAL);
   }
}
