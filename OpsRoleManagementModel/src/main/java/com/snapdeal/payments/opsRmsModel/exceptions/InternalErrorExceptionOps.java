package com.snapdeal.payments.opsRmsModel.exceptions;

/**
 * 
 * Exception occurs when there is any internal exception Error code defines the
 * exact reason for the exception
 * 
 *
 * @author shubham
 *         21-Oct-2015
 *
 */

public class InternalErrorExceptionOps extends OpsRoleMgmtException {

   private static final long serialVersionUID = 8008343838743105044L;

   public InternalErrorExceptionOps() {
   }

   public InternalErrorExceptionOps(String message) {
      super(message);
   }

   public InternalErrorExceptionOps(Throwable e) {
      super(e);
   }

   public InternalErrorExceptionOps(Throwable e, String message) {
      super(message, e);
   }

   {
      this.setErrorCode(ExceptionErrorCode.INTERNAL_ERROR);
   }

}
