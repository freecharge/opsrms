package com.snapdeal.payments.opsRmsModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when an Expired OTP is entered
 * 
 * @author roopali 26-Oct-2018
 *
 */

@NoArgsConstructor
public class OTPExpiredExceptionOps extends OpsRoleMgmtException {

	private static final long serialVersionUID = 1L;

	public OTPExpiredExceptionOps(Exception e) {
		super(e);
	}

	public OTPExpiredExceptionOps(Throwable e) {
		super(e);
	}

	public OTPExpiredExceptionOps(String message) {
		super(message);
	}

	{
		this.setErrorCode(ExceptionErrorCode.OTP_EXPIRED);
	}

}

