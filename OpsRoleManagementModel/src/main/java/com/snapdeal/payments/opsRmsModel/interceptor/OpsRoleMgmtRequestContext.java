package com.snapdeal.payments.opsRmsModel.interceptor;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;

import com.google.common.base.Optional;

public class OpsRoleMgmtRequestContext {

   private final static ThreadLocal<Map<RequestHeaders, String>> requestCache = new ThreadLocal<Map<RequestHeaders, String>>();;

   public static void set(RequestHeaders key, String value) {
      if (requestCache.get() == null) {
         requestCache.set(new HashMap<RequestHeaders, String>());
      }
      requestCache.get().put(key, value);
   }

   public static Optional<String> get(RequestHeaders key) {
      if (MapUtils.isEmpty(requestCache.get()))
         return Optional.<String> absent();
      return Optional.<String> fromNullable(requestCache.get().get(key));
   }

   public static void resetRequestMap() {
      requestCache.remove();
   }
}
