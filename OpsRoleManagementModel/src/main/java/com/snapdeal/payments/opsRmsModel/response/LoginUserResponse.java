package com.snapdeal.payments.opsRmsModel.response;

import java.util.List;

import com.snapdeal.payments.opsRmsModel.dto.Token;
import com.snapdeal.payments.opsRmsModel.request.Role;
import com.snapdeal.payments.opsRmsModel.request.User;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author aniket
 *         23-Nov-2015
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"token"})
public class LoginUserResponse {
	
	   private User user;
	   private List<Role> allRoles;
	   private Token token;
}
