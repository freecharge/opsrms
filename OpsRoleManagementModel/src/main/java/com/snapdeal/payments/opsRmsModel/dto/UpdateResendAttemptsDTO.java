package com.snapdeal.payments.opsRmsModel.dto;

import lombok.Data;

public @Data class UpdateResendAttemptsDTO {
	private String otpId;
	private String mobileNumber;
	private String emailId;
	private int resendAttempts;
}