package com.snapdeal.payments.opsRmsModel.dto;

import java.util.List;

import lombok.Data;

@Data
public class FieldFilter {
   private String fieldName;
   private List<String> values;
}
