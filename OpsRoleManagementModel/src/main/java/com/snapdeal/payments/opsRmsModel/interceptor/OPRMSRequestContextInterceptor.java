package com.snapdeal.payments.opsRmsModel.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.snapdeal.payments.opsRmsModel.commons.OPRMSPreAuthorize;

@Component
public class OPRMSRequestContextInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		HandlerMethod handlerMethod = null;
		if(handler instanceof HandlerMethod){
			handlerMethod = (HandlerMethod) handler;
			OPRMSPreAuthorize OPRMSPreAuthorize = handlerMethod.getMethod().getAnnotation(OPRMSPreAuthorize.class);
			if (OPRMSPreAuthorize != null) {
				this.setRequestDetailMap(request);
				return true;
			}

		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		OpsRoleMgmtRequestContext.resetRequestMap();
	}

	private void setRequestDetailMap(HttpServletRequest request) {
		OpsRoleMgmtRequestContext.set(RequestHeaders.APP_NAME,
				request.getHeader(RequestHeaders.APP_NAME.getName()));
		OpsRoleMgmtRequestContext.set(RequestHeaders.TOKEN,
				request.getHeader(RequestHeaders.TOKEN.getName()));

	}
}
