package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;

/**
 * @author shubham
 *         19-Oct-2015
 */
@Data
public class GetAllPermissionsRequest extends AbstractRequest {
   /**
    * 
    */
   private static final long serialVersionUID = 1L;
}
