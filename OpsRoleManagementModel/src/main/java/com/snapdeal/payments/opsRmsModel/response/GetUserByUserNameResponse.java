package com.snapdeal.payments.opsRmsModel.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.snapdeal.payments.opsRmsModel.request.User;

/**
 * @author shubham
 *         19-Oct-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserByUserNameResponse {

   private User user;

}
