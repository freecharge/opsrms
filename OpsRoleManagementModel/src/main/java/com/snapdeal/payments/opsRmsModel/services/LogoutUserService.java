package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.LogoutUserRequest;

/**
 * @author aniket
 *         16-Feb-2016
 */
public interface LogoutUserService {

   public void logoutUser(LogoutUserRequest logoutUserRequest);
}
