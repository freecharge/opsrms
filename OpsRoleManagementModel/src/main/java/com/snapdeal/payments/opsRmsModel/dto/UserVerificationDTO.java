package com.snapdeal.payments.opsRmsModel.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;


@Data
@AllArgsConstructor
@ToString(exclude = {"code"})
public class UserVerificationDTO implements Serializable {

	private static final long serialVersionUID = -4593674093085200712L;
	private String code;
	private String userId;
	private Date codeExpiryTime;
	private Date createdTime;

}