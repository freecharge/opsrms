package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.ResendOTPRequest;
import com.snapdeal.payments.opsRmsModel.response.GenerateOTPResponse;

/**
 * @author aniket
 *         6-Jan-2015
 */
public interface ResendOTPService {

   public GenerateOTPResponse resendOTP(ResendOTPRequest resendOTPRequest);
}
