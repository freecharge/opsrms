package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.CreateRoleRequest;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface CreateRoleService {

   public void createRole(CreateRoleRequest createRole);
}
