package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.ForgotPasswordNotifyRequest;

/**
 * @author aniket
 *         21-Nov-2015
 */
public interface ForgotPasswordNotifyService {

   public void forgotPasswordNotify(ForgotPasswordNotifyRequest forgotPasswordNotify);
}
