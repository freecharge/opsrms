package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author shubham
 *         19-Oct-2015
 */
@Data
public class GetUsersByRoleRequest extends AbstractRequest{

   @NotBlank
   private String roleName;

}
