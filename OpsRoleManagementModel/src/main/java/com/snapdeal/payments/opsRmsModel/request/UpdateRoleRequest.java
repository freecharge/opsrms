package com.snapdeal.payments.opsRmsModel.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * @author shubham
 *         17-Oct-2015
 */
@Data
public class UpdateRoleRequest extends AbstractRequest {
   /**
	 * 
	 */
   private static final long serialVersionUID = 1L;
   @NotNull
   private Integer id;
   @NotNull
   private List<Integer> permissionIds;
}
