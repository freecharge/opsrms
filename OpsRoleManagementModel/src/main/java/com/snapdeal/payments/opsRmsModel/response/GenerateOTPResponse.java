package com.snapdeal.payments.opsRmsModel.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author aniket
 *         5-Jan-2016
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenerateOTPResponse {
  private String otpId;

}
