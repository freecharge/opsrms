package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.GetUsersByIdsRequest;
import com.snapdeal.payments.opsRmsModel.response.GetUsersByIdsResponse;

/**
 * @author aniket
 *         8-Dec-2015
 */
public interface GetUsersByIdsService {

   public GetUsersByIdsResponse getUsersByIds(GetUsersByIdsRequest getUsers);

}
