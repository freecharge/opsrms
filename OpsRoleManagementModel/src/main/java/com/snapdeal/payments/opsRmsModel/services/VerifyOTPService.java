package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.VerifyOTPRequest;

/**
 * @author aniket
 *         6-Jan-2015
 */
public interface VerifyOTPService {

   public void verifyOTP(VerifyOTPRequest verifyOTPRequest);
}
