package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.CreateUserRequest;
import com.snapdeal.payments.opsRmsModel.response.CreateUserResponse;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface CreateUserService {
   public CreateUserResponse createUser(CreateUserRequest user);
}
