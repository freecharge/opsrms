package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;
import lombok.ToString;

/**
 * @author aniket
 *        16-Feb-2016
 */
@Data
@ToString(exclude={"token"})
public class LogoutUserRequest extends AbstractRequest {
	private static final long serialVersionUID = -8472296562359475060L;

}
