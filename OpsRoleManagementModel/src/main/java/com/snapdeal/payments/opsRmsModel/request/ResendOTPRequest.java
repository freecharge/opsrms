package com.snapdeal.payments.opsRmsModel.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

/**
 * @author aniket
 * 6-Jan-2016
 *
 */
@Data
public class ResendOTPRequest extends AbstractRequest {

	private static final long serialVersionUID = 8866300354838633736L;
	@NotBlank
	private String otpId;
}