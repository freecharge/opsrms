package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.UpdateRoleRequest;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface UpdateRoleService {

   public void updateRole(UpdateRoleRequest updaterole);
}
