package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.*;
import com.snapdeal.payments.opsRmsModel.response.GetAllAppResponse;
import com.snapdeal.payments.opsRmsModel.response.GetAllPermissionsResponse;
import com.snapdeal.payments.opsRmsModel.response.GetAllRolesResponse;
import com.snapdeal.payments.opsRmsModel.response.GetAllUsersResponse;

import java.util.List;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface UiDropDownsService {

   public GetAllRolesResponse getAllRoles(GetAllRolesRequest request);

   public GetAllPermissionsResponse getAllPermissions(GetAllPermissionsRequest request);

   public GetAllUsersResponse getAllUsers(GetAllUsersRequest request);

   GetAllAppResponse getAllAppNames(GetAllPermissionsRequest request);
   GetAllPermissionsResponse getAllPermissionsBasedOnCriteria(UserPermissionRequest request);
   void addNewPermission(AddPermissionRequest request) throws RuntimeException;

}
