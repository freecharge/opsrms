package com.snapdeal.payments.opsRmsModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when there is something wrong with OTP
 * 
 * @author aniket
 *         20-Jan-2016
 *
 */

@NoArgsConstructor
public class CipherExceptionOps extends OpsRoleMgmtException {

   public CipherExceptionOps(Exception e) {
      super(e);
   }

   public CipherExceptionOps(Throwable e) {
      super(e);
   }

   public CipherExceptionOps(String message) {
      super(message);
   }

   public CipherExceptionOps(String msg, Throwable throwable){
      super(msg,throwable);
   }

   {
      this.setErrorCode(ExceptionErrorCode.CIPHER);
   }

}
