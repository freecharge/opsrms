package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;

@Data
public class UserPermission {
    private String userId;
    private String permissionId;
}
