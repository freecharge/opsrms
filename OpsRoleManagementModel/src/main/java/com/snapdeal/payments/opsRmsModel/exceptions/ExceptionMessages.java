package com.snapdeal.payments.opsRmsModel.exceptions;

public interface ExceptionMessages {
   public static final String PERMISSION_INITIALISATION_EXCEPTION = "There Is Some Problem In Permission Handler Method Initalization";
   public static final String SYSTEM_ERROR_EXCEPTION = "System Error! Please try after some time";
   public static final String UNAUTHORIZED_USER_EXCEPTION = "User is not authorized for this action";
   public static final String PSWD_HASHING_EXCEPTION = "Problem in password hasing";
   public static final String PARAM_VALIDATION_EXCEPTION = "Param Validation Failed: ";
   public static final String PSWD_MISMATCH_EXCEPTION = "Incorrect \"current password\"";
   public static final String USER_ALREADY_EXCEPTION = "User already exist with given userName in system";
   public static final String NO_SUCH_USER_EXCEPTION = "User does not exist";
   public static final String LOGIN_FAILURE_EXCEPTION = "Wrong Username or password";
   public static final String SOCIAL_DETAILS_FETCH_EXCEPTION = "Unable to fetch details of social user";
   public static final String SOCIAL_LOGIN_EXCEPTION = "Email id did not match to social token";
   public static final String VERIFY_OTP_LIMIT_BREACHED = "Maximum limit of verification attempts reached. Please try again after {0} minutes";
   public static final String OTP_LIMIT_BREACHED = "You have exceeded the maximum limit of OTPs. Please try after {0} minutes.";
   public static final String INVALID_OTP_ID = "OTP ID is invalid";
   public static final String OTP_GENERIC_EXCEPTION = "Unable to validate the state of Otp. Please try again.";
   public static final String INVALID_OTP_ENTERED = "Please enter correct OTP, or use Resend OTP";
   public static final String PSWD_MUST_CONTAIN_SIX_LETTER = "Password length must be minimum 6 characters";
   public static final String INVALID_CHARACTER_PSWD = "Password entered is not valid, avoid using &*|;\"<>,=";
   public static final String PSWD_IS_BLANK = "Please enter your password";
   public static final String SESSION_EXPIRED = "Session Expired";
   public static final String FORGOT_PSWD_INVALID = "Please use OTP generation for resetting password";
   public static final String CIPHER_ERROR = "Cipher Initialization error.";
   public static final String ENCRYPTION_ERROR = "Encryption Error."; 
   public static final String DECRTYPTION_ERROR = "Decryption Error.";
   public static final String INCORRECT_CODE_ERROR = "Email link has expired. Please use forgot password";
   public static final String EXPIRED_OTP_ERROR = "OTP has expired, please use resend OTP";
   public static final String PSWD_EXPIRED_ERROR = "Password has expired";
   public static final String FIELD_NAME_IS_INVALID = "Field name passed is invalid";
   public static final String EMAIL_ID_IS_NOT_PRESENT = "Email Id is not registered with us ";
   public static final String MOBILE_NOT_PRESENT = "Mobile number is not registered with us ";
   public static final String EMAIL_AND_MOBILE_NOT_PRESENT ="Email Id & Mobile Number are not present!";
}