package com.snapdeal.payments.opsRmsModel.commons;

public interface EntityConstants {

	String USERID = "id";
	String USERNAME = "userName";
	String EMAILID = "email";
	String CREATED_ON = "createdOn";
	String UPDATED_ON = "updatedOn";
	String PERMISSION_NAME = "permissionName";
	String ROLE_NAME = "roleName";
	String NAME = "name";
	String MOBILENUMBER = "mobileNumber";
	String ACTIVE = "active";
}

