package com.snapdeal.payments.opsRmsModel.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = {"password"})
public class SetPasswordToDummyPasswordRequest extends AbstractRequest  {
	
	@NotBlank
	private String userName;
	@NotBlank
	private String password;
	

	
}
