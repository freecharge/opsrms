package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.UpdateUserRequest;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface UpdateUserService {

   public void updateUser(UpdateUserRequest user);
}
