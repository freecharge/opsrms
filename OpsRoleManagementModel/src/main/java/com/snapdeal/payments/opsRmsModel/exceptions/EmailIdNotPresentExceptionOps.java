package com.snapdeal.payments.opsRmsModel.exceptions;

public class EmailIdNotPresentExceptionOps extends OpsRoleMgmtException {
	
	private static final long serialVersionUID = 1L;

	public EmailIdNotPresentExceptionOps(Exception e) {
	      super(e);
	   }

	   public EmailIdNotPresentExceptionOps(Throwable e) {
	      super(e);
	   }

	   public EmailIdNotPresentExceptionOps(String message) {
	      super(message);
	   }

	   {
	      this.setErrorCode(ExceptionErrorCode.NO_EMAIL_ID_PRESENT);
	   }

}
