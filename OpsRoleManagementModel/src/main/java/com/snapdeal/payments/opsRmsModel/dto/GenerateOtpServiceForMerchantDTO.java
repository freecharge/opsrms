package com.snapdeal.payments.opsRmsModel.dto;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author roopali 10-Sep-2018
 */
@Data
@AllArgsConstructor
public class GenerateOtpServiceForMerchantDTO {
	
	private static final long serialVersionUID = -1118240620874477648L;

	@NotBlank
	private String userId;
    private String mobileNumber;
	private String emailId;


}
