package com.snapdeal.payments.opsRmsModel.exceptions;

/**
 * @author shubham
 *         21-Oct-2015
 */
public enum ExceptionErrorCode {
   CLIENT_INTERNAL("501"), INTERNAL_ERROR("502"), SERVER_INTERNAL("503"), No_SUCH_USER("504"), DEFAULT_VALIDATION(
            "505"), USER_ALREADY_EXIST("506"), ROLE_NAME_ALREADY_EXIST("507"), UNAUTHRIZED_USER(
            "508"), OTP_SERVICE("509"), CIPHER("510"), INVALID_CODE("511"), NO_EMAIL_ID_PRESENT("512"),
   NO_MOBILE_REGISTERED("513"),NO_EMAIL_AND_MOBILE_PRESENT("514"), INVALID_OTP_ENTERED ("515"),
   INVALID_OTP_ATTEMPTS_EXCEEDED ("516"),RESEND_OTP_ATTEMPTS_EXCEEDED ("517"),OTP_EXPIRED ("518"),
   OTP_ID_INVALID("519") ;

   private final String errorCode;

   private ExceptionErrorCode(String errorCode) {
      this.errorCode = errorCode;
   }

   public String getErrorCode() {
      return errorCode;
   }
}