package com.snapdeal.payments.opsRmsModel.request;

import org.hibernate.validator.constraints.NotBlank;

import com.snapdeal.payments.opsRmsModel.commons.Password;

import lombok.Data;
import lombok.ToString;

/**
 * @author aniket
 *         21-Jan-2016
 */
@Data
@ToString(exclude={"verificationCode","password"})
public class VerifyCodeRequest extends AbstractRequest {
   /**
	 * 
	 */
   private static final long serialVersionUID = 125631424L;

   @NotBlank
   private String verificationCode;
   
   @Password
   private String password;

}
