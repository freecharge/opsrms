package com.snapdeal.payments.opsRmsModel.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.snapdeal.payments.opsRmsModel.request.Role;

/**
 * @author shubham
 *         17-Oct-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetAllRolesResponse {
   private List<Role> roles;
}
