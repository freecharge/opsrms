package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.VerifyCodeRequest;

/**
 * @author aniket
 *         21-Jan-2015
 */
public interface VerifyCodeService {

   public void verifyCodeAndSetPassword(VerifyCodeRequest verifyCodeRequest);
}
