package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;

/**
 * @author shubham
 *         17-Oct-2015
 */
@Data
public class Permission {
   private Integer id;
   private String name;
   private String appName;
   private  String displayName;
}
