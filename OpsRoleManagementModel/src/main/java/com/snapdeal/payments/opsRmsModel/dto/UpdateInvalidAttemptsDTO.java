package com.snapdeal.payments.opsRmsModel.dto;

import lombok.Data;

public @Data class UpdateInvalidAttemptsDTO {
	private String otpId;
	private int invalidAttempts;
	private String clientId;
	private String reason;
}
