package com.snapdeal.payments.opsRmsModel.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import lombok.ToString;

/**
 * @author aniket
 *         24-Nov-2015
 */
@Data
@ToString(exclude={"socialToken"})
public class SocialLoginUserRequest extends AbstractRequest {
   /**
	 * 
	 */
   private static final long serialVersionUID = 11233112334L;

   @NotBlank   
   private String socialToken;
   @NotBlank
   private String emailId;
  
   private String source;

}
