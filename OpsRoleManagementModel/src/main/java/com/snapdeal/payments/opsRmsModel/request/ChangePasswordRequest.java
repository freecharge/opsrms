package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;
import lombok.ToString;

import org.hibernate.validator.constraints.NotBlank;

import com.snapdeal.payments.opsRmsModel.commons.Password;

/**
 * @author aniket
 *         21-Nov-2015
 */
@Data
@ToString(exclude={"token","oldPassword","newPassword"})
public class ChangePasswordRequest extends AbstractRequest {
   /**
	 * 
	 */
   private static final long serialVersionUID = 1L;

   @NotBlank
   private String token;
   @NotBlank
   private String oldPassword;
   @Password
   private String newPassword;

}
