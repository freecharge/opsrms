package com.snapdeal.payments.opsRmsModel.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author shubham
 *         19-Oct-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserResponse {
   /**
	 * 
	 */
   private static final long serialVersionUID = 1L;
   private String userId;
}
