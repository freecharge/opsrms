package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.LoginUserRequest;
import com.snapdeal.payments.opsRmsModel.response.LoginUserResponse;

public interface LoginUserService {

   public LoginUserResponse loginUser(LoginUserRequest loginUserRequest);
}
