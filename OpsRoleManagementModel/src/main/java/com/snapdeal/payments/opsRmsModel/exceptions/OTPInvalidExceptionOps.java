package com.snapdeal.payments.opsRmsModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown on entering an invalid OTP
 * 
 * @author roopali 26-Oct-2018
 *
 */

@NoArgsConstructor
public class OTPInvalidExceptionOps extends OpsRoleMgmtException {

	private static final long serialVersionUID = 1L;

	public OTPInvalidExceptionOps(Exception e) {
		super(e);
	}

	public OTPInvalidExceptionOps(Throwable e) {
		super(e);
	}

	public OTPInvalidExceptionOps(String message) {
		super(message);
	}

	{
		this.setErrorCode(ExceptionErrorCode.INVALID_OTP_ENTERED);
	}

}
