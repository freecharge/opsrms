package com.snapdeal.payments.opsRmsModel.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Roopali
 *         11-Sep-2018
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VerifyOtpForMobileVerificationResponse {
	
	private boolean isOtpValid;
	
}
