package com.snapdeal.payments.opsRmsModel.dto;

import lombok.Data;

@Data
public class DownloadUser {
    private String user_name;
    private String email;
    private String app_name;
    private String permission;
    private String permission_name;
    private String created_on;
    private String updated_on;
}
