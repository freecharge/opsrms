package com.snapdeal.payments.opsRmsModel.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * @author aniket 
 * 11-Dec-2015
 */
@Data
public class GetRolesByRoleNamesRequest extends AbstractRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1126348124L;
	@NotNull
	private List<String> listRoleNames;

}
