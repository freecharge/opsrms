package com.snapdeal.payments.opsRmsModel.exceptions;

public class EmailAndMobileNotPresentExceptionOps extends OpsRoleMgmtException {
	
	private static final long serialVersionUID = 1L;

	public EmailAndMobileNotPresentExceptionOps(Exception e) {
		super(e);
	}
    public EmailAndMobileNotPresentExceptionOps(Throwable e) {
		super(e);
	}
    public EmailAndMobileNotPresentExceptionOps(String message) {
		super(message);
	}
    {
    	this.setErrorCode(ExceptionErrorCode.NO_EMAIL_AND_MOBILE_PRESENT);
	}

}
