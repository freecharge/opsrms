package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.ChangePasswordRequest;

/**
 * @author aniket
 *         21-Nov-2015
 */
public interface ChangePasswordService {

   public void changePassword(ChangePasswordRequest changePassword);
}
