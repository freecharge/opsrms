package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author shubham
 *         17-Oct-2015
 */
@Data
public class GetUserByIdRequest extends AbstractRequest{
   @NotBlank
   private String userId;

}
