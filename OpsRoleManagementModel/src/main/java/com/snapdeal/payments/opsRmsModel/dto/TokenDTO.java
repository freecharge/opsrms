package com.snapdeal.payments.opsRmsModel.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author aniket
 * 	15-Feb-2016
 */
@NoArgsConstructor
@AllArgsConstructor
public @Data class TokenDTO {

	private String tokenId;
	private Date loginTime;
	private Boolean active;
	private String userName;
}
