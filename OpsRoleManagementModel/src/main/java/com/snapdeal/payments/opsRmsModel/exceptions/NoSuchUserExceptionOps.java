package com.snapdeal.payments.opsRmsModel.exceptions;

/**
 * @author shubham
 *         21-Oct-2015
 */
public class NoSuchUserExceptionOps extends ValidationExceptionOps {

   private static final long serialVersionUID = -4212829204867344092L;

   public NoSuchUserExceptionOps(RuntimeException e) {
      super(e);
   }

   public NoSuchUserExceptionOps() {
      super();
   }

   public NoSuchUserExceptionOps(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.No_SUCH_USER);
   }
}
