package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;

@Data
public class RequestObject {
	private String request;
}
