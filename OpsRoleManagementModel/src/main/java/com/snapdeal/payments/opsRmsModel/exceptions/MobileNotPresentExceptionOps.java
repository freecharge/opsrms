package com.snapdeal.payments.opsRmsModel.exceptions;


public class MobileNotPresentExceptionOps extends OpsRoleMgmtException {

	
	private static final long serialVersionUID = 1L;

	public MobileNotPresentExceptionOps(Exception e) {
		super(e);
	}
    public MobileNotPresentExceptionOps(Throwable e) {
		super(e);
	}
    public MobileNotPresentExceptionOps(String message) {
		super(message);
	}
    {
	this.setErrorCode(ExceptionErrorCode.NO_EMAIL_AND_MOBILE_PRESENT);
	}

}
