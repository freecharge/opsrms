package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.GetUsersByRoleRequest;
import com.snapdeal.payments.opsRmsModel.response.GetUsersByRoleResponse;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface GetUsersByRoleService {

   public GetUsersByRoleResponse getUsersByRole(GetUsersByRoleRequest userrequest);
}
