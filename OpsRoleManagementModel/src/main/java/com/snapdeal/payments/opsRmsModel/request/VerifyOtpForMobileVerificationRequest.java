package com.snapdeal.payments.opsRmsModel.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class VerifyOtpForMobileVerificationRequest extends AbstractRequest{

	   private static final long serialVersionUID = 123143324L;

	   @NotBlank
	   private String otpId;
	   @NotBlank
	   private String otp;
	
}
