package com.snapdeal.payments.opsRmsModel.services;

import java.util.List;

import com.snapdeal.payments.opsRmsModel.request.Role;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface GetRoleService {

   public List<Role> getAllRoles();

   public List<Role> getAllRolesByUser(String userName);
}
