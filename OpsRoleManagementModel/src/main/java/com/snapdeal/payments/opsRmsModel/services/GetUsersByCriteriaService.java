package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.GetAllUsersRequest;
import com.snapdeal.payments.opsRmsModel.request.GetUsersByCriteriaRequest;
import com.snapdeal.payments.opsRmsModel.response.DownloadUsersResponse;
import com.snapdeal.payments.opsRmsModel.response.GetUsersByCriteriaResponse;

/**
 * @author aniket
 *         26-Aug-2016
 */
public interface GetUsersByCriteriaService {

   public GetUsersByCriteriaResponse getUsersByCriteria(GetUsersByCriteriaRequest getUsersByCriteriaRequest);
   public DownloadUsersResponse downloadAllUsers(GetAllUsersRequest request) throws RuntimeException;
}
