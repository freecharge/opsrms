package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.GetUserByIdRequest;
import com.snapdeal.payments.opsRmsModel.response.GetUserByIdResponse;

/**
 * @author shubham
 *         26-Oct-2015
 */
public interface GetUserByIdService {

   public GetUserByIdResponse getUserById(GetUserByIdRequest userrequest);
}
