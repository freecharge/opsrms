package com.snapdeal.payments.opsRmsModel.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.snapdeal.payments.opsRmsModel.request.User;

/**
 * @author aniket
 *         29-Dec-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUserByTokenResponse {
   private User user;

}
