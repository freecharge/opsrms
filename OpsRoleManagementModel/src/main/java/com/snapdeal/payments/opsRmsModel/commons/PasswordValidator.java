package com.snapdeal.payments.opsRmsModel.commons;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionMessages;
import org.apache.commons.lang3.StringUtils;

public class PasswordValidator implements ConstraintValidator<Password, String> {

   private static final String PSWD_PATTERN = "^[A-Za-z0-9@%+'!~`#\\$?.\\^\\{\\}\\[\\]\\(\\)_\\-:, \\/\\\\]*$";

   private Password pswd;
   @Override
   public void initialize(Password constraintAnnotation) {
	   this.pswd = constraintAnnotation;
   }

   @Override
   public boolean isValid(String password, ConstraintValidatorContext context) {

      if (!StringUtils.isBlank(password)) {
         String passwordTrimed = password.trim();
         if (passwordTrimed.length() < 6 || passwordTrimed.length() > 127) {
            addConstraintViolation(context,
                     ExceptionMessages.PSWD_MUST_CONTAIN_SIX_LETTER);
            return false;
         }
         if (!passwordTrimed.matches(PSWD_PATTERN)) {
            addConstraintViolation(context, ExceptionMessages.INVALID_CHARACTER_PSWD);
            return false;
         }
      } else if(this.pswd.mandatory()){
         addConstraintViolation(context, ExceptionMessages.PSWD_IS_BLANK);
         return false;
      }
      return true;
   }
  
   private void addConstraintViolation(ConstraintValidatorContext context,
            String errorMessage) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(errorMessage)
               .addConstraintViolation();
   }
}
