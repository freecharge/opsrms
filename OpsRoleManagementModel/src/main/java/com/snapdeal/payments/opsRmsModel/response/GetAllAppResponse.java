package com.snapdeal.payments.opsRmsModel.response;

/*
@Author SmrutiRanjan
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetAllAppResponse {
    List<String> appNameList;
}
