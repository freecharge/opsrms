package com.snapdeal.payments.opsRmsModel.exceptions;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Exception is thrown when the service request is invalid
 * 
 * @author shubham
 *         21-Oct-2015
 */

@NoArgsConstructor
public class OpsRoleMgmtException extends RuntimeException {

   private ExceptionErrorCode errorCode;
   private Class<? extends OpsRoleMgmtException> exceptionCause;


   public OpsRoleMgmtException withErrorCode(ExceptionErrorCode errorCode) {
      this.errorCode = errorCode;
      return this;

   }

   public OpsRoleMgmtException(Throwable e) {
      super(e);
   }

   public OpsRoleMgmtException(String message) {
      super(message);
   }

   public OpsRoleMgmtException(String message, Throwable e) {
      super(message, e);
   }

   public ExceptionErrorCode getErrorCode() {
      return errorCode;
   }

   public void setErrorCode(ExceptionErrorCode errorCode) {
      this.errorCode = errorCode;
   }

   public Class<?> getExceptionCause() {
      return exceptionCause;
   }

   public void setExceptionCause(Class<? extends OpsRoleMgmtException> exceptionCause) {
      this.exceptionCause = exceptionCause;
   }

   {
      this.setExceptionCause(this.getClass());
   }

   private static final long serialVersionUID = 1L;

}
