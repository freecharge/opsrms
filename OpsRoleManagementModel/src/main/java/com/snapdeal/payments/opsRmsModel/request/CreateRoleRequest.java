package com.snapdeal.payments.opsRmsModel.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author shubham 17-Oct-2015
 */
@Data
public class CreateRoleRequest extends AbstractRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotBlank
	private String roleName;
	@NotNull
	private List<Integer> permissionIds;
}
