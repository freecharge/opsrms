package com.snapdeal.payments.opsRmsModel.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.snapdeal.payments.opsRmsModel.request.User;

/**
 * @author shubham
 *         19-Oct-2015
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUsersByRoleResponse {

   private List<User> users;

}
