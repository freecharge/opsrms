package com.snapdeal.payments.opsRmsModel.commons;

public interface CommunicationConstants {

	public static final String RMS_GENERATE_OTP = "RMS_GENERATE_OTP";
	public static final String OTP_GENERATED = "OTP_GENERATED";
	public static final String RMS_DEBUGGER_DASHBOARD_SETPSWD = "RMS_DEBUGGER_DASHBOARD_SETPASSWORD";
	public static final String SUCCESS = "SUCCESS";
	public static final String RMS_OP_PANEL_SETPSWD = "RMS_OP_PANEL_SETPASSWORD";
	public static final String RMS_MERCHANT_PANEL_SETPSWD = "RMS_MERCHANT_PANEL_SETPASSWORD";
	public static final String MERCHANT_CREATION_OTP = "MERCHANT_CREATION_OTP";

}