package com.snapdeal.payments.opsRmsModel.exceptions;

/**
 * 
 * Exception occurs when there is any internal exception in client code Error
 * code defines the exact reason for the exception e.g. while calling restAPI
 *
 * @author shubham
 *         21-Oct-2015
 *
 */

public class InternalClientExceptionOps extends InternalErrorExceptionOps {

   private static final long serialVersionUID = -4788605715033323479L;

   public InternalClientExceptionOps() {
   }

   public InternalClientExceptionOps(String message) {
      super(message);
   }

   public InternalClientExceptionOps(String message, Throwable e) {
      super(e, message);
   }

   public InternalClientExceptionOps(Throwable e) {
      super(e);
   }

   {
      setErrorCode(ExceptionErrorCode.CLIENT_INTERNAL);
   }

}
