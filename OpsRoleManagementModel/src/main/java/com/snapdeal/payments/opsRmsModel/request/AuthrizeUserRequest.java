package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;
import lombok.ToString;

/**
 * @author shubham
 *         17-Oct-2015
 */
@Data
@ToString(exclude={"preAuthrizeString"})
public class AuthrizeUserRequest extends AbstractRequest {
   /**
	 * 
	 */
   private static final long serialVersionUID = 1L;

   private String preAuthrizeString;

}
