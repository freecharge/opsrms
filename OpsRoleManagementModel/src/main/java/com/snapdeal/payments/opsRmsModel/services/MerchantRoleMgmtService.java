package com.snapdeal.payments.opsRmsModel.services;

import com.snapdeal.payments.opsRmsModel.request.GenerateOTPRequest;
import com.snapdeal.payments.opsRmsModel.request.ResendOTPRequest;
import com.snapdeal.payments.opsRmsModel.request.SendEmailAndSmsOnMerchantCreationRequest;
import com.snapdeal.payments.opsRmsModel.request.SetPasswordToDummyPasswordRequest;
import com.snapdeal.payments.opsRmsModel.request.VerifyOtpForMobileVerificationRequest;
import com.snapdeal.payments.opsRmsModel.response.GenerateOTPResponse;
import com.snapdeal.payments.opsRmsModel.response.SendEmailAndSmsOnMerchantCreationResponse;
import com.snapdeal.payments.opsRmsModel.response.VerifyOtpForMobileVerificationResponse;

/**  
 * @author roopali 11-Sep-2018
 * APIs written for TSM-583
 */

public interface MerchantRoleMgmtService {

	   SendEmailAndSmsOnMerchantCreationResponse sendEmailAndSmsOnMerchantCreation(SendEmailAndSmsOnMerchantCreationRequest sendEmailOnMerchantCreationRequest);
	   GenerateOTPResponse sendOtpForMobileVerification(GenerateOTPRequest generateOtpRequest);
	   GenerateOTPResponse sendDummyOtpForMobileVerification(GenerateOTPRequest generateOtpRequest);
	   GenerateOTPResponse resendOtpForMobileVerification(ResendOTPRequest resendOTPRequest);
	   VerifyOtpForMobileVerificationResponse verifyOtpForMobileVerification(VerifyOtpForMobileVerificationRequest verifyOtpForMobileVerificationRequest);
	   Boolean resetPasswordToDummyPasswordForStaging(SetPasswordToDummyPasswordRequest request);
}
