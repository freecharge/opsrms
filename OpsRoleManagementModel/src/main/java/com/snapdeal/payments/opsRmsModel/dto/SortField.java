package com.snapdeal.payments.opsRmsModel.dto;

import lombok.Data;

@Data
public class SortField {
   private String fieldName;
   private boolean order;
}
