package com.snapdeal.payments.opsRmsModel.commons;

public interface OTPConstants {
	public static final String FROZEN_REASON_INVALID_ATTEMPTS = "Invalid_Attempts";
	public static final String FROZEN_REASON_RESEND_ATTEMPTS = "Resend_Attempts";
	public static final int STATUS=200;
  }
