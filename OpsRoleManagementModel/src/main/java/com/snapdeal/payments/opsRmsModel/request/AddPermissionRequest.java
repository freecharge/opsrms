package com.snapdeal.payments.opsRmsModel.request;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
public class AddPermissionRequest extends AbstractRequest{

    @NotBlank
    private String userId;
    private String permissionName;
    private String appName;
    private String displayName;
}
