package com.snapdeal.payments.opsRmsModel.exceptions;

/**
 * @author shubham
 *         21-Oct-2015
 */
public class OpsRoleNameAlreadyExistException extends ValidationExceptionOps {

   private static final long serialVersionUID = -4212829204867344092L;

   public OpsRoleNameAlreadyExistException(RuntimeException e) {
      super(e);
   }

   public OpsRoleNameAlreadyExistException() {
      super();
   }

   public OpsRoleNameAlreadyExistException(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.ROLE_NAME_ALREADY_EXIST);
   }
}
