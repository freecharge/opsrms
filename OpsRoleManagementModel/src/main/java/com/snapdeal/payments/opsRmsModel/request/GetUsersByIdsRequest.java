package com.snapdeal.payments.opsRmsModel.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

/**
 * @author aniket 
 * 8-Dec-2015
 */
@Data
public class GetUsersByIdsRequest extends AbstractRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1126348124L;
	@NotNull
	@NotEmpty
	private List<String> listUserIds;

}
