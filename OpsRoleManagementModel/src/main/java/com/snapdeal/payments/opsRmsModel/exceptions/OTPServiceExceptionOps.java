package com.snapdeal.payments.opsRmsModel.exceptions;

import lombok.NoArgsConstructor;

/**
 * Exception is thrown when there is something wrong with OTP
 * 
 * @author aniket
 *         20-Jan-2016
 *
 */

@NoArgsConstructor
public class OTPServiceExceptionOps extends OpsRoleMgmtException {

   public OTPServiceExceptionOps(Exception e) {
      super(e);
   }

   public OTPServiceExceptionOps(Throwable e) {
      super(e);
   }

   public OTPServiceExceptionOps(String message) {
      super(message);
   }

   {
      this.setErrorCode(ExceptionErrorCode.OTP_SERVICE);
   }

}
