package com.snapdeal.payments.opsRmsClient.utils;

import com.snapdeal.payments.opsRmsClient.exceptions.HttpTransportException;
import com.snapdeal.payments.opsRmsModel.dto.ClientConfig;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class provides Implementation of Http Methods namely GET, POST, PUT,
 * DELETE
 */
@Slf4j
public class HttpSender {

    private static final String CONTENT_ENCODING_UTF_8 = "UTF-8";

    private final HttpClientConfig httpConfig = HttpClientConfig.getInstance();

    private volatile HttpClient httpClient;

    private static HttpSender instance = new HttpSender();

    private HttpSender() {
    }

    public static HttpSender getInstance() {
        return instance;
    }

    public HttpResponse executeGet(String url, Map<String, String> params,
                                   Map<String, String> headers, ClientConfig config) throws HttpTransportException {

        HttpGet httpGet = new HttpGet(createURL(url, params));
        setHeaders(httpGet, headers);

        try {

            HttpClient httpClient = getHttpClient(getApiTimeoutFromRequestConfig(config));

            HttpResponse response = httpClient.execute(httpGet);
            return response;
        } catch (Exception e) {
            throw new HttpTransportException("Unable to execute http get"+ExceptionErrorCode.CLIENT_INTERNAL.getErrorCode(), e);
        }
    }

    public HttpResponse executePost(String url, Map<String, String> params,
                                    Map<String, String> headers, ClientConfig config) throws HttpTransportException {

        HttpPost httpPost = new HttpPost(url);
        setHeaders(httpPost, headers);
        try {
            if (params != null) {
                httpPost.setEntity(createStringEntity(params));
            }
            int apiTimeout= getApiTimeoutFromRequestConfig(config);
            log.info("apiTimeout: "+ apiTimeout);
            HttpClient httpClient = getHttpClient(apiTimeout);
            log.info("httpClient for OPRMS: " + httpClient);
            HttpResponse response = httpClient.execute(httpPost);
            return response;
        } catch (Exception e) {
            throw new HttpTransportException("Unable to execute http post "+ExceptionErrorCode.CLIENT_INTERNAL.getErrorCode(),e);
        }
    }

    public HttpResponse executePut(String url, Map<String, String> params,
                                   Map<String, String> headers, ClientConfig config) throws HttpTransportException {

        HttpPut httpPut = new HttpPut(url);
        setHeaders(httpPut, headers);

        try {
            if (params != null) {
                httpPut.setEntity(createStringEntity(params));
            }
            HttpClient httpClient = getHttpClient(getApiTimeoutFromRequestConfig(config));
            HttpResponse response = httpClient.execute(httpPut);
            return response;
        } catch (Exception e) {
            throw new HttpTransportException("Unable to execute http put" + ExceptionErrorCode.CLIENT_INTERNAL.getErrorCode(), e);
        }
    }

    public HttpResponse executeDelete(String url, Map<String, String> params,
                                      Map<String, String> headers, ClientConfig config) throws HttpTransportException {

        HttpDelete httpDelete = new HttpDelete(createURL(url, params));
        setHeaders(httpDelete, headers);

        try {
            HttpClient httpClient = getHttpClient(getApiTimeoutFromRequestConfig(config));
            HttpResponse response = httpClient.execute(httpDelete);
            return response;
        } catch (Exception e) {
            log.error("Error in executing http delete: {} " , e.getMessage());
            throw new HttpTransportException("Unable to execute http delete"
                    + ExceptionErrorCode.CLIENT_INTERNAL.getErrorCode(), e);
        }
    }

    private HttpEntity createStringEntity(Map<String, String> params) throws JSONException,
            UnsupportedEncodingException {

        JSONObject keyArg = new JSONObject();
        for (Map.Entry<String, String> pEntry : params.entrySet()) {
            keyArg.put(pEntry.getKey(), pEntry.getValue());
        }
        StringEntity input = null;
        input = new StringEntity(keyArg.toString());
        return input;
    }

    private String createURL(String url, Map<String, String> params) {

        if (params == null) {
            return url;
        }

        StringBuilder builder = new StringBuilder();
        builder.append(url);

        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        for (Map.Entry<String, String> pEntry : params.entrySet()) {
            nvps.add(new BasicNameValuePair(pEntry.getKey(), pEntry.getValue()));
        }

        builder.append('?').append(URLEncodedUtils.format(nvps, CONTENT_ENCODING_UTF_8));
        return builder.toString();
    }

    private void setHeaders(HttpRequestBase httpObject, Map<String, String> headers) {
        if (headers != null) {
            for (Map.Entry<String, String> hEntry : headers.entrySet()) {
                httpObject.addHeader(hEntry.getKey(), hEntry.getValue());
            }
        }
    }

    /**
     * Set the SSLContext on the basis of mutualAuthentication.If mutual
     * authentication is required,it will set the respective client keystore and
     * trust store to be sent to server during SSL Handshaking.
     */
    public SSLConnectionSocketFactory getSSLConnectionSocketFactory() throws KeyStoreException,
            NoSuchAlgorithmException, CertificateException, IOException,
            UnrecoverableKeyException, KeyManagementException {

        final SSLContextBuilder builder = new SSLContextBuilder();
        if (httpConfig.getMutualAuthenticationRequired()) {
            KeyStore clientKeyStore = KeyStore.getInstance(httpConfig.getKeyStoreType());
            clientKeyStore.load(new FileInputStream(httpConfig.getClientKeystoreFile()), httpConfig
                    .getClientKeystorePswd().toCharArray());
            builder.loadKeyMaterial(clientKeyStore, httpConfig.getClientKeystorePswd()
                    .toCharArray());
        }
        SSLContext sslContext = builder.build();
        // Install the all-trusting trust manager
        sslContext.init(null, getTrustAllCertsManager(), new java.security.SecureRandom());
        final SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
                SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        sslsf.createSocket(null);
        return sslsf;
    }

    private TrustManager[] getTrustAllCertsManager() {
        // creating trust all Certificate TM
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            }
        }};
        return trustAllCerts;
    }

    /**
     * Creates a Pooled connection Manager to be used by HTTP Client during
     * connection establishment.You can also set the number of max connection
     * per route here though connPoolControl.
     */
    public PoolingHttpClientConnectionManager getConnectionManager(SSLConnectionSocketFactory sslsf) {

        final PoolingHttpClientConnectionManager poolingmgr = new PoolingHttpClientConnectionManager(
                RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.getSocketFactory())
                        .register("https", sslsf).build());

        poolingmgr.setMaxTotal(httpConfig.getMaxConnTotal());
        poolingmgr.setDefaultMaxPerRoute(httpConfig.getMaxConnPerRoute());

        return poolingmgr;
    }

    /**
     * This will create a keep alive strategy used to persist a established
     * connection. Default : 5 second
     */
    public ConnectionKeepAliveStrategy getKeepAliveStartegy() {

        ConnectionKeepAliveStrategy myStrategy = new ConnectionKeepAliveStrategy() {
            @Override
            public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
                return httpConfig.getKeepAlive();
            }
        };
        return myStrategy;
    }

    private HttpClient createHttpClient(int apiTimeout) throws UnrecoverableKeyException,
            KeyManagementException, KeyStoreException, NoSuchAlgorithmException,
            CertificateException, IOException {

        HttpClientBuilder builder;
        RequestConfig config = RequestConfig.custom().setConnectTimeout(apiTimeout)
                .setConnectionRequestTimeout(apiTimeout).setSocketTimeout(apiTimeout).build();
        if (ClientDetails.getInstance().getUrl().startsWith(ClientConstants.HTTPS)) {
            builder = HttpClientBuilder.create().useSystemProperties()
                    .setKeepAliveStrategy(getKeepAliveStartegy())
                    .setConnectionManager(getConnectionManager(getSSLConnectionSocketFactory()));
        } else {
            builder = HttpClientBuilder.create();
        }
        return builder.setDefaultRequestConfig(config).build();
    }

    public HttpClient getHttpClient(int apiTimeout) throws UnrecoverableKeyException,
            KeyManagementException, KeyStoreException, NoSuchAlgorithmException,
            CertificateException, IOException {
        if (httpClient == null) {
            synchronized (this) {
                if (httpClient == null)
                    httpClient = createHttpClient(apiTimeout);
            }
        }
        return httpClient;
    }

    private int getApiTimeoutFromRequestConfig(ClientConfig config) {

        if (config == null || config.getApiTimeOut() == 0) {
            return ClientDetails.getInstance().getApiTimeOut();
        }
        return config.getApiTimeOut();
    }
}
