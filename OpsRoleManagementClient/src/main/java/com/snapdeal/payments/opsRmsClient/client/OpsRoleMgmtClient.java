package com.snapdeal.payments.opsRmsClient.client;

import com.snapdeal.payments.opsRmsModel.request.*;
import com.snapdeal.payments.opsRmsModel.response.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.mina.http.api.HttpMethod;

import com.fasterxml.jackson.core.type.TypeReference;
import com.snapdeal.payments.opsRmsClient.exceptions.ServiceException;
import com.snapdeal.payments.opsRmsClient.utils.ClientDetails;
import com.snapdeal.payments.opsRmsClient.utils.HttpUtil;
import com.snapdeal.payments.opsRmsModel.commons.RestURIConstants;
import com.snapdeal.payments.opsRmsModel.exceptions.ExceptionErrorCode;
import com.snapdeal.payments.opsRmsModel.exceptions.NoSuchUserExceptionOps;
import com.snapdeal.payments.opsRmsModel.exceptions.OpsRoleMgmtException;
import com.snapdeal.payments.opsRmsModel.services.OpsRoleMgmtService;

public class OpsRoleMgmtClient implements OpsRoleMgmtService {
	protected <T extends AbstractRequest, R> R prepareResponse(T request,
			TypeReference<ServiceResponse<R>> typeReference, HttpMethod httpMthod, String uri)
					throws ServiceException, OpsRoleMgmtException {

		final String completeURL = HttpUtil.getInstance().getCompleteUrl(uri);
		ServiceResponse<R> obj = (ServiceResponse<R>) HttpUtil.getInstance().processHttpRequest(completeURL,
				typeReference, request, httpMthod);
		if (obj.getException() != null) {
			OpsRoleMgmtException exception = new OpsRoleMgmtException(obj.getException().getMessage());
			exception.setErrorCode(obj.getException().getErrorCode());
			if (exception.getErrorCode().getErrorCode().equals(ExceptionErrorCode.No_SUCH_USER.getErrorCode())) {
				NoSuchUserExceptionOps noSuchUserException = new NoSuchUserExceptionOps(exception.getMessage());
				noSuchUserException.setErrorCode(exception.getErrorCode());
				throw noSuchUserException;
			}
			throw exception;
		}
		return obj.getResponse();
	}
	public OpsRoleMgmtClient() {
	}

	public OpsRoleMgmtClient(String uri, String port, int timeOut) throws Exception {
		ClientDetails.init(uri, port, timeOut);
	}

	@Override
	public void createRole(CreateRoleRequest createRole) throws ServiceException {
		prepareResponse(createRole, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.CREATE_ROLE);
	}

	@Override
	public void updateRole(UpdateRoleRequest updaterole) throws ServiceException {
		prepareResponse(updaterole, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.UPDATE_ROLE);
	}

	@Override
	public CreateUserResponse createUser(CreateUserRequest user) throws ServiceException {
		return prepareResponse(user, new TypeReference<ServiceResponse<CreateUserResponse>>() {
		}, HttpMethod.POST, RestURIConstants.CREATE_USER_WITH_LINK);
	}

	@Override
	public void updateUser(UpdateUserRequest user) throws ServiceException {
		prepareResponse(user, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.UPDATE_USER);

	}

	@Override
	public GetUserByIdResponse getUserById(GetUserByIdRequest userrequest)
			throws ServiceException {
		return prepareResponse(userrequest, new TypeReference<ServiceResponse<GetUserByIdResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_USER_BY_ID);
	}

	@Override
	public GetUserByUserNameResponse getUserByUserName(GetUserByUserNameRequest userRequest)
			throws ServiceException {
		return prepareResponse(userRequest, new TypeReference<ServiceResponse<GetUserByUserNameResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_USER_BY_USER_NAME);
	}

	@Override
	public GetUsersByRoleResponse getUsersByRole(GetUsersByRoleRequest userrequest)
			throws ServiceException {
		return prepareResponse(userrequest, new TypeReference<ServiceResponse<GetUsersByRoleResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_USERS_BY_ROLE);
	}

	@Override
	public GetAllRolesResponse getAllRoles(GetAllRolesRequest request) {
		return prepareResponse(request, new TypeReference<ServiceResponse<GetAllRolesResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_ALL_ROLES);
	}

	@Override
	public GetAllPermissionsResponse getAllPermissions(GetAllPermissionsRequest request) {
		return prepareResponse(request, new TypeReference<ServiceResponse<GetAllPermissionsResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_ALL_PERMISSIONS);
	}
	@Override
	public void changePassword(ChangePasswordRequest changePassword) {
		prepareResponse(changePassword, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.CHANGE_PSWD);

	}

	@Override
	public void forgotPasswordNotify(ForgotPasswordNotifyRequest forgotPasswordNotify) {
		prepareResponse(forgotPasswordNotify, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.FORGOT_PSWD);

	}

	@Override
	public LoginUserResponse loginUser(LoginUserRequest loginUserRequest) {
		return prepareResponse(loginUserRequest, new TypeReference<ServiceResponse<LoginUserResponse>>() {
		}, HttpMethod.POST, RestURIConstants.LOGIN_USER);
	}


	@Override
	public void logoutUser(LogoutUserRequest logoutUserRequest) {
		prepareResponse(logoutUserRequest, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.LOGOUT_USER);
	}

	@Override
	public GetAllUsersResponse getAllUsers(GetAllUsersRequest request) {
		return prepareResponse(request, new TypeReference<ServiceResponse<GetAllUsersResponse>>() {
		}, HttpMethod.GET, RestURIConstants.GET_ALL_USER);
	}

	@Override
	public GetAllAppResponse getAllAppNames(GetAllPermissionsRequest request) throws ServiceException{
		return prepareResponse(request, new TypeReference<ServiceResponse<GetAllAppResponse>>() {},
				HttpMethod.POST, RestURIConstants.GET_ALL_APP_NAMES);
	}

	@Override
	public GetAllPermissionsResponse getAllPermissionsBasedOnCriteria(UserPermissionRequest request) throws ServiceException{
		return prepareResponse(request, new TypeReference<ServiceResponse<GetAllPermissionsResponse>>() {},
				HttpMethod.POST, RestURIConstants.GET_ALL_PERMISSIONS_BY_CRITERIA);
	}

	@Override
	public void addNewPermission(AddPermissionRequest request) throws RuntimeException {
		prepareResponse(request, new TypeReference<ServiceResponse<Void>>() {}, HttpMethod.POST, RestURIConstants.ADD_NEW_PERMISSION);
	}

	@Override
	public LoginUserResponse socialLoginUser(SocialLoginUserRequest socialLoginUserRequest) {
		return prepareResponse(socialLoginUserRequest, new TypeReference<ServiceResponse<LoginUserResponse>>() {
		}, HttpMethod.POST, RestURIConstants.SOCIAL_LOGIN_USER);
	}

	@Override
	public void deleteUser(DeleteUserRequest deleteUser) {
		prepareResponse(deleteUser, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.DELETE_USER);

	}

	@Override
	public GetUsersByIdsResponse getUsersByIds(GetUsersByIdsRequest getUsers) {
		return prepareResponse(getUsers, new TypeReference<ServiceResponse<GetUsersByIdsResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GET_ALL_USERS_BY_IDS);
	}

	@Override
	public GetRolesByRoleNamesResponse getRolesByRoleNames(GetRolesByRoleNamesRequest getRoles) {
		return prepareResponse(getRoles, new TypeReference<ServiceResponse<GetRolesByRoleNamesResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GET_ALL_ROLES_BY_ROLENAMES);
	}

	@Override
	public GetUserByTokenResponse getUserByToken(GetUserByTokenRequest userrequest) {
		return prepareResponse(userrequest, new TypeReference<ServiceResponse<GetUserByTokenResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GET_USER_DETAILS_FROM_TOKEN);
	}

	@Override
	public GenerateOTPResponse generateOTP(GenerateOTPRequest generateOtpRequest) {
		return prepareResponse(generateOtpRequest, new TypeReference<ServiceResponse<GenerateOTPResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GENERATE_OTP);
	}

	@Override
	public GenerateOTPResponse resendOTP(ResendOTPRequest resendOTPRequest) {
		return prepareResponse(resendOTPRequest, new TypeReference<ServiceResponse<GenerateOTPResponse>>() {
		}, HttpMethod.POST, RestURIConstants.RESEND_OTP);
	}

	@Override
	public void verifyOTP(VerifyOTPRequest verifyOTPRequest) {
		prepareResponse(verifyOTPRequest, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.VALIDATE_OTP);

	}

	public void authorizeUser(AuthrizeUserRequest request) {
		String permissions = request.getPreAuthrizeString();
		permissions = permissions.replaceAll("\\s+","");
		if (!permissions.isEmpty()) {
			final String BASE_PERMISSION_STRING = "(hasPermission('";
			String[] parts = permissions.split(",");
			int numberOfPermissions = parts.length;
			StringBuilder preAuthrizeString = new StringBuilder();
			preAuthrizeString.append(BASE_PERMISSION_STRING) ;
			preAuthrizeString.append(parts[0]);
			preAuthrizeString.append("')");
			while (numberOfPermissions > 1) {
				numberOfPermissions--;
				preAuthrizeString.append("and hasPermission('");
				preAuthrizeString.append(parts[numberOfPermissions]);preAuthrizeString.append("')");
			}
			preAuthrizeString.append(")");
			request.setPreAuthrizeString(preAuthrizeString.toString());
		}
		prepareResponse(request, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.GET, RestURIConstants.PREAUTHRIZE_TO_USER);
	}

	@Override
	public void verifyCodeAndSetPassword(VerifyCodeRequest verifyCodeRequest) {
		prepareResponse(verifyCodeRequest, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.VALIDATE_CODE);
	}
	
	@Override
	public GetUsersByCriteriaResponse getUsersByCriteria(GetUsersByCriteriaRequest getUsersByCriteriaRequest) {
		return prepareResponse(getUsersByCriteriaRequest, new TypeReference<ServiceResponse<GetUsersByCriteriaResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GET_USER_BY_CRITERIA);
	}


	public SendEmailAndSmsOnMerchantCreationResponse sendEmailAndSmsOnMerchantCreation(SendEmailAndSmsOnMerchantCreationRequest user) {
		return prepareResponse(user, new TypeReference<ServiceResponse<SendEmailAndSmsOnMerchantCreationResponse>>() {
		}, HttpMethod.POST, RestURIConstants.SEND_EMAIL_AND_SMS_ON_MERCHANT_CREATION);
	}
	
	
	public GenerateOTPResponse sendOtpForMobileVerification(GenerateOTPRequest user) {
		return prepareResponse(user, new TypeReference<ServiceResponse<GenerateOTPResponse>>() {
		}, HttpMethod.POST, RestURIConstants.SEND_OTP_FOR_MOBILE_VERIFICATION);
	}
	
	
	public GenerateOTPResponse sendDummyOtpForMobileVerification(GenerateOTPRequest user) {
		return prepareResponse(user, new TypeReference<ServiceResponse<GenerateOTPResponse>>() {
		}, HttpMethod.POST, RestURIConstants.SEND_DUMMY_OTP_FOR_MOBILE_VERIFICATION);
	}
	
	
	public GenerateOTPResponse resendOtpForMobileVerification(ResendOTPRequest resendOTPRequest) {
		return prepareResponse(resendOTPRequest, new TypeReference<ServiceResponse<GenerateOTPResponse>>() {
		}, HttpMethod.POST, RestURIConstants.RESEND_OTP_FOR_MOBILE_VERIFICATION);
	}
	

	public VerifyOtpForMobileVerificationResponse verifyOtpForMobileVerification(VerifyOtpForMobileVerificationRequest resendOTPRequest) {
		return prepareResponse(resendOTPRequest, new TypeReference<ServiceResponse<VerifyOtpForMobileVerificationResponse>>() {
		}, HttpMethod.POST, RestURIConstants.VERIFY_OTP_FOR_MOBILE_VERIFICATION);
	}
	
	
	public Boolean resetPasswordToDummyPasswordForStaging(SetPasswordToDummyPasswordRequest resendOTPRequest) {
		return prepareResponse(resendOTPRequest, new TypeReference<ServiceResponse<Boolean>>() {
		}, HttpMethod.POST, RestURIConstants.RESET_PSWD_TO_DUMMY_PSWD);
	}
	
	
	public GetUserByTokenResponse getUserByTokenForMerchant(GetUserByTokenRequest userrequest) {
		return prepareResponse(userrequest, new TypeReference<ServiceResponse<GetUserByTokenResponse>>() {
		}, HttpMethod.POST, RestURIConstants.GET_USER_DETAILS_FROM_TOKEN_FOR_MERCHANT);
	}
	@Override
	public void deleteAuditUser(DeleteUserRequest deleteUser) {
		prepareResponse(deleteUser, new TypeReference<ServiceResponse<Void>>() {
		}, HttpMethod.POST, RestURIConstants.DELETE_AUDIT_USER);
	}

	@Override
	public DownloadUsersResponse downloadAllUsers(GetAllUsersRequest request) {
		return prepareResponse(request, new TypeReference<ServiceResponse<DownloadUsersResponse>>() {
		}, HttpMethod.GET, RestURIConstants.DOWNLOAD_USERS);
	}
}