package com.snapdeal.payments.opsRmsClient.commons.enums;

public enum EnvironmentEnum {

   TESTING, DEVELOPMENT, PRODUCTION
}
