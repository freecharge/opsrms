package com.snapdeal.payments.opsRmsClient.utils;

import org.apache.mina.http.api.HttpMethod;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.snapdeal.payments.opsRmsClient.exceptions.HttpTransportException;
import com.snapdeal.payments.opsRmsClient.exceptions.ServiceException;
import com.snapdeal.payments.opsRmsModel.commons.OPRMSPreAuthorize;
import com.snapdeal.payments.opsRmsModel.commons.RestURIConstants;
import com.snapdeal.payments.opsRmsModel.exceptions.OpsRoleMgmtException;
import com.snapdeal.payments.opsRmsModel.request.AbstractRequest;
import com.snapdeal.payments.opsRmsModel.request.AuthrizeUserRequest;
import com.snapdeal.payments.opsRmsModel.response.ServiceResponse;

@Aspect
@Component
public class OPRMSPreAuthorizeHandler {

   protected <T extends AbstractRequest, R> R prepareResponse(T request,
            TypeReference<ServiceResponse<R>> typeReference, HttpMethod httpMthod, String uri)
            throws ServiceException, OpsRoleMgmtException {

      final String completeURL = HttpUtil.getInstance().getCompleteUrl(uri);

      ServiceResponse<R> obj = (ServiceResponse<R>) HttpUtil.getInstance().processHttpRequest(
               completeURL, typeReference, request, httpMthod);
      OpsRoleMgmtException exception = obj.getException();
      if (exception != null) {
         throw exception;
      }
      return obj.getResponse();
   }

   @Before("within(com.snapdeal..*) && @annotation(authorize)")
   public void handleAuthrization(OPRMSPreAuthorize authorize) throws Exception {

      AuthrizeUserRequest request = new AuthrizeUserRequest();
      request.setPreAuthrizeString(authorize.value());

      prepareResponse(request, new TypeReference<ServiceResponse<Void>>() {
      }, HttpMethod.GET, RestURIConstants.PREAUTHRIZE_TO_USER);

   }
}
