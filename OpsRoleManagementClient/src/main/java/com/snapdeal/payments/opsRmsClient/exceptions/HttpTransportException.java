package com.snapdeal.payments.opsRmsClient.exceptions;


public class HttpTransportException extends ServiceException {

	private static final long serialVersionUID = 1L;

	public HttpTransportException(String message, String code) {
		super(message, code);
	}

	public HttpTransportException(String message, Throwable throwable) {
		super(message, throwable);
	}

}