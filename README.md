# OPS Panel Role Management Service

It powers the authentication and authorization for Ops Panel.

## Getting Started

### Prerequisites
* JDK 8+
* Maven
* Tomcat 8
* MySQL

### Building and Running

Clone the repository in your local machine, `master` branch is the last stable branch.

```
    git clone git@bitbucket.org:freecharge/opsrms.git
    cd opsrms/
    mvn clean install -U
```

## Environments

### Staging
* Host IP: "10.220.21.18", Hostname: oprms-service-staging-01.fcinternal.in, port - 8080
* Mysql db host: "jdbc:mysql://stage1_db.freecharge.in/ops_panel_role_mgmt"

### PRODUCTION
* ELB: "ops-rolemanagement-internal-elb.fcinternal.net:80"
* OLD mysql db host - "jdbc:mysql://merchantcommon-db-prod-01.fcinternal.net/ops_panel_role_mgmt"
* mysql db host - "jdbc:mysql://oprms-settlement-report-master-db-prod.freecharge.in:3307/ops_panel_role_mgmt
 

## Deployment

To check logs on Staging :-

* `ssh <username>@10.220.21.18`
* cd /var/log/opsrolemanagement/applog
* Log FileName :- application.log

To check logs on Production Server 1:-

* ssh <username>@10.240.21.90
* cd /var/log/opsrolemanagement/applog
* Log FileName :- application.log

To check logs on Production Server 2:-

* ssh <username>10.240.20.102
* cd /var/log/opsrolemanagement/applog
* Log FileName :- application.log


### DEPLOYMENT

* Login to jenkins using ldap credentials 
* Build the branch on https://jenkins.freecharge.in/jenkins/view/customer_support/view/OPRms/view/oprms_build/job/oprms_build/
* Then `Build with Parameters`.
* Mention branch name and click on `Build`.
* Post build, go to last build and click `Promotion Status`.
* Go to Production or Staging section and click `Promote Build`.
* Now go back to oprms_build and press `oprms_deployment` (https://jenkins.freecharge.in/jenkins/view/customer_support/view/OPRms/view/oprms_build/job/oprms_deployment/)
* Click on "Build with parameters" ,select environment, choose hostname, mention CMR and press `Build`.
* That's it, the build will get deployed to the mentioned environment.


## Contributing
As of now, the development of this microservice is done entirely within CS team. 
However, if you have any refactoring improvements or new feature code to contribute, please raise a PR and reach out to `cs-oncall@freecharge.com`.

For the usual development effort,

* Cut a branch from provided JIRA from master branch and do the development.
* Raise a PR, address comments, get it approved
* commit your changes where each commit must mention JIRA id: ` git commit -m "CS-1259 <<Commit message>>"`


## Contact Us
For any questions regarding the workflows, APIs etc. for this microservice, please reach out to `cs-oncall@freecharge.com`.
For any on-call issues, please create a OP JIRA on-call ticket and if needed please reach out to `cs-oncall@freecharge.com`