#common to all
#Version 0.0.1

FROM harbor.fcinternal.in/infra/spring-boot_eks_base_image:latest

ARG RELEASE_VERSION
ARG COMP_NAME
ARG JAR_NAME
ARG SERVICE_NAME

RUN mkdir -p /apps/"$SERVICE_NAME"/release/"$RELEASE_VERSION"/conf && mkdir -p /apps/"$SERVICE_NAME"/release/"$RELEASE_VERSION"/lib
COPY ./"$COMP_NAME"/target/"$JAR_NAME" /apps/"$SERVICE_NAME"/release/"$RELEASE_VERSION"/lib
RUN ln -fs /apps/"$SERVICE_NAME"/release/"$RELEASE_VERSION" /apps/"$SERVICE_NAME"/current

RUN echo "Asia/Kolkata" > /etc/timezone