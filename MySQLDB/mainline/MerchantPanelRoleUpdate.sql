use role_mgmt;
insert into role values(1,'Admin',current_timestamp,current_timestamp);
insert into role_permission_map values(1,1001);
insert into user_role_mapping values(1,1001);
update permissions set display_name="Add User";
