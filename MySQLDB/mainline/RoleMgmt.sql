create database role_mgmt;
use role_mgmt;

create table permissions(
id  int(16) auto_increment,
app_name varchar(128),
permission_name varchar(128),
created_on DATETIME(3) not null DEFAULT now(3),
updated_on DATETIME(3) not null DEFAULT now(3) on update now(3),
primary key(id),
UNIQUE KEY(app_name,permission_name)
)ROW_FORMAT=DYNAMIC; 

ALTER TABLE permissions AUTO_INCREMENT=1001;


create table role(

id  int(16) auto_increment,
name varchar(128),
created_on DATETIME(3) not null DEFAULT now(3),
updated_on DATETIME(3) not null DEFAULT now(3) on update now(3),
primary key(id)
) ROW_FORMAT=DYNAMIC;

ALTER TABLE role AUTO_INCREMENT=1001;


create table role_permission_map(
role_id int(16),
permission_id int(16),
UNIQUE KEY (role_id, permission_id)
) ROW_FORMAT=DYNAMIC;

create table user(
id varchar(255),
name varchar(128),
user_name varchar(255),
password varchar(255),
email varchar(255),
mobile_no int(16),
active TINYINT NOT NULL DEFAULT 1,
is_social_user TINYINT NOT NULL DEFAULT 0,
UNIQUE KEY(user_name),
created_on DATETIME(3) not null DEFAULT now(3),
updated_on DATETIME(3) not null DEFAULT now(3) on update now(3),
primary key(id)
) ROW_FORMAT=DYNAMIC;



create table user_role_mapping(
role_id int(16),
user_id varchar(255),
UNIQUE KEY (role_id, user_id)
)ROW_FORMAT=DYNAMIC;


create table user_permission_mapping(
permission_id  int(16),
user_id varchar(255),
UNIQUE KEY (permission_id, user_id)
) ROW_FORMAT=DYNAMIC;



-- Script for super user

insert into permissions(id,app_name,permission_name) values(1001,"ROLEMGMT","SUPER_CREATE");
-- default password for super user password@123
insert into user(id,name,user_name,password,email) values("1001","shubham","shubham.bansal@snapdeal.com","bf03d11c9b14b8e84daceade45ccbe2de3856b2e","shubham.bansal@snapdeal.com");
insert into role(id,name) values(1001,"SUPER_USER");
insert into role_permission_map values(1001,1001);
insert into user_role_mapping values(1001,1001);
insert into user_permission_mapping values(1001,1001);


