use role_mgmt;
CREATE TABLE verification_user (
  id int(16) auto_increment,
  code varchar(255) NOT NULL,
  user_id varchar(255) NOT NULL,
  code_expiry_time datetime NOT NULL,
  created_time datetime NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE verification_user AUTO_INCREMENT=1001;
